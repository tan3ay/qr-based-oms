"use strict";
// import { IChipProp } from "../components/chip-bar/chip-bar";
exports.__esModule = true;
exports.SELECT_TYPE = void 0;
var SELECT_TYPE;
(function (SELECT_TYPE) {
    SELECT_TYPE["SINGLE_SELECT"] = "SINGLE_SELECT";
    SELECT_TYPE["MULTI_SELECT"] = "MULTI_SELECT";
})(SELECT_TYPE = exports.SELECT_TYPE || (exports.SELECT_TYPE = {}));
// export const menuFilters:IChipProp[] =[
//     {label:'Chef special',value:'isChefSpecial',selected:false},
//     {label:'Vegan',value:'isVegan',selected:false},
//     {label:'Vegetarian',value:'isVeg',selected:false},
//     {label:'Drinks',value:'isDrink',selected:false}
// ]
