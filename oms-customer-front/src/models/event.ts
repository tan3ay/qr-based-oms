import { OrderDto } from "./Order";

export interface EventData {
  type: string;
  target: string;
  targetId: string;
  order: OrderDto;
}

export enum EVENT_TYPE {
  ORDER_CREATED = "ORDER_CREATED",
  ORDER_STATUS_UPDATED = "ORDER_STATUS_UPDATED",
  ORDER_ITEM_UPDATE = "ORDER_ITEM_UPDATE",
  SUCCESS_ACK = "SUCCESS_ACK",
  FAILURE_ACK = "FAILURE_ACK",
}

export interface WebSocketEvent {
  restaurantId: string;
  type: string;
  data: string;
}
