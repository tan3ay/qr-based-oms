import { IChipProp } from "../components/chip-bar/chip-bar";
import { MenuDto, MenuItemDto, MenuItemOption } from "./menu";
import * as _ from "lodash";
import {
  ICheckList,
  ICheckListItem,
  ICheckListItemProp,
  ICheckListProp,
} from "../components/checked-list/check-list";
import { last } from "lodash";
import { KitchenStatus, OrderDto, OrderItemDto, ORDER_STATUS } from "./Order";
import { ORDER_ACTIONS } from "../state/action";

export enum SELECT_TYPE {
  SINGLE_SELECT = "SINGLE_SELECT",
  MULTI_SELECT = "MULTI_SELECT",
}

export interface OrderItemVo extends OrderItemDto {}

export interface OrderVo extends OrderDto {
  itemsVo: OrderItemVo[];
}

export const cloneOrderDtoToVo = (orderDto: OrderDto | undefined): OrderVo => {
  if (!orderDto) return <OrderVo>{};
  let orderVo: OrderVo = { ..._.cloneDeep(orderDto), itemsVo: [] };
  let itemVo: OrderItemVo[] = [];
  orderVo.items.forEach((item) => {
    itemVo.push(_.cloneDeep(item));
  });
  return { ...orderVo, itemsVo: itemVo };
};

export const cloneOrderVoToDto = (orderVo: OrderVo): OrderDto => {
  let orderDto: OrderDto = _.cloneDeep(_.omit(orderVo, "itemsVo"));
  orderDto.items = orderVo.itemsVo;

  let kitchens = new Set<string>();
  orderDto.items.forEach((item) => kitchens.add(item.kitchenId));

  orderDto.kitchenStatus = Array.from(kitchens).map((kitchen: string) => {
    return { kitchenId: kitchen, status: ORDER_STATUS.PLACED };
  });

  return orderDto;
};
