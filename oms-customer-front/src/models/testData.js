"use strict";
exports.__esModule = true;
exports.menuData = exports.menuDtoMock = void 0;
var SELECT_TYPE;
(function (SELECT_TYPE) {
    SELECT_TYPE[SELECT_TYPE["SINGLE_SELECT"] = 0] = "SINGLE_SELECT";
    SELECT_TYPE[SELECT_TYPE["MULTI_SELECT"] = 1] = "MULTI_SELECT";
})(SELECT_TYPE || (SELECT_TYPE = {}));
var riceOption = [
    {
        optionId: "1",
        optionName: "Biryani",
        optionCost: 0
    },
    {
        optionId: "2",
        optionName: "Plain rice",
        optionCost: 0
    },
    {
        optionId: "3",
        optionName: "Curd Rice",
        optionCost: 0
    }
];
var curryOptions = [
    {
        optionId: "1",
        optionName: "Chicken curry",
        optionCost: 0
    },
    {
        optionId: "2",
        optionName: "Daal",
        optionCost: 0
    },
    {
        optionId: "3",
        optionName: "Veg Curry",
        optionCost: 0
    }
];
var meatOptions = [
    {
        optionId: "1",
        optionName: "Chicken",
        optionCost: 0
    },
    {
        optionId: "2",
        optionName: "Mutton",
        optionCost: 0
    }
];
var sweetOptions = [
    {
        optionId: "1",
        optionName: "Ladoo",
        optionCost: 0
    },
    {
        optionId: "2",
        optionName: "Chocolate Lava",
        optionCost: 0
    }
];
var riceItem = {
    itemId: "1",
    itemName: "Rice",
    itemOptions: riceOption,
    itemType: SELECT_TYPE.SINGLE_SELECT,
    maxSelectOptions: 2
};
var curryItem = {
    itemId: "2",
    itemName: "Curry",
    itemOptions: curryOptions,
    itemType: SELECT_TYPE.MULTI_SELECT,
    maxSelectOptions: 2
};
var meatItem = {
    itemId: "3",
    itemName: "Meat",
    itemOptions: meatOptions,
    itemType: SELECT_TYPE.MULTI_SELECT,
    maxSelectOptions: 1
};
var sweetItem = {
    itemId: "4",
    itemName: "Sweets",
    itemOptions: sweetOptions,
    itemType: SELECT_TYPE.MULTI_SELECT,
    maxSelectOptions: 1
};
exports.menuDtoMock = {
    "restId": "1",
    "menuId": "3",
    "menuKey": "1",
    "menuOrder": "2#1",
    "name": "Veg Platter",
    "desc": "Veg platter comes with 1 rice 2 rotis and your choice of drinks",
    "imgUrl": "https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg",
    "category": "Specialite Maison",
    "cost": 4,
    "quantity": 0,
    "isCustomizable": 1,
    "customizeSettingType": 0,
    "customizeSettingData": {},
    "characteristics": { "isVegan": 1, "isChefSpecial": 1, "isDrink": 1, "isVeg": 1 },
    "items": [riceItem, curryItem, meatItem, sweetItem],
    "kitchenId": '0'
};
console.log(JSON.stringify(exports.menuDtoMock));
var simpleMenu = '[{"restId":"1","menuKey":"menu#1629192300860","menuId":"1629192300860","kitchenId":"1","name":"LASSI","desc":"33 CL - Fait Maison - Boisson de yaourt sale ou sucre ou nature","imgUrl":"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg","category":"Specialite Maison","menuOrder":"2#1","cost":4,"characteristics":{"isDrink":"1","isVeg":"1","isChefSpecial":"1","isVegan":"1"},"quantity":0,"isCustomizable":0,"customizeSettingType":"0","customizeSettingData":{},"items":[]},{"restId":"1","menuKey":"menu#1629192300989","menuId":"1629192300989","kitchenId":"0","name":"Veg Platter","desc":"Veg platter comes with 1 rice 2 rotis and your choice of drinks","imgUrl":"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg","category":"Thallis","menuOrder":"3#1","cost":4,"characteristics":{"isDrink":"1","isVeg":"1","isChefSpecial":"1","isVegan":"1"},"quantity":0,"isCustomizable":1,"customizeSettingType":"0","customizeSettingData":{},"items":[{"itemId":"1","itemName":"Rice","itemOptions":[{"optionId":"1","optionName":"Biryani"},{"optionId":"2","optionName":"Plain rice"},{"optionId":"3","optionName":"Curd Rice"}],"itemType":0,"maxSelectOptions":2},{"itemId":"2","itemName":"Curry","itemOptions":[{"optionId":"1","optionName":"Chicken curry"},{"optionId":"2","optionName":"Daal"},{"optionId":"3","optionName":"Veg Curry"}],"itemType":1,"maxSelectOptions":2},{"itemId":"3","itemName":"Meat","itemOptions":[{"optionId":"1","optionName":"Chicken curry"},{"optionId":"2","optionName":"Daal"},{"optionId":"3","optionName":"Veg Curry"}],"itemType":1,"maxSelectOptions":1},{"itemId":"4","itemName":"Sweets","itemOptions":[{"optionId":"1","optionName":"Chicken curry"},{"optionId":"2","optionName":"Daal"},{"optionId":"3","optionName":"Veg Curry"}],"itemType":1,"maxSelectOptions":1}]},{"restId":"1","menuKey":"menu#1629192301005","menuId":"1629192301005","kitchenId":"1","name":"LASSI MANGUE","desc":"33 CL - Fait Maison - Boisson de yaourt sale ou sucre a la mangue","imgUrl":"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg","category":"Specialite Maison","menuOrder":"2#2","cost":5,"characteristics":{"isDrink":"1","isVeg":"1","isChefSpecial":"1","isVegan":"1"},"quantity":0,"isCustomizable":0,"customizeSettingType":"0","customizeSettingData":{},"items":[]}]';
exports.menuData = JSON.parse(simpleMenu);
