import { ERROR_TYPE } from "../../components/error-fallback/error-fallback";
import {
  PageLoadingDispatchType,
  LOAD_TYPE,
  PAGE_LOADING_ACTIONS,
  PAGE_ERROR_ACTIONS,
  PageErrorDispatchType,
  ERROR_PAGE,
} from "../action";

export interface PageErrorStateI {
  allPage: boolean;
  error: ERROR_TYPE;
}

const defaultState: PageErrorStateI = {
  allPage: false,
  error: ERROR_TYPE.NONE,
};

export const pageErrorReducer = (
  state: PageErrorStateI = defaultState,
  action: PageErrorDispatchType
): PageErrorStateI => {
  switch (action.type) {
    case PAGE_ERROR_ACTIONS.SET_PAGE_ERROR:
      {
        let newState = { ...state };
        switch (action.payload.page) {
          case ERROR_PAGE.ALL_PAGE:
            newState.allPage = true;
            newState.error = action.payload.error;
            break;
        }
        return newState;
      }
      break;

    default:
      return state;
  }
};
