import {
  PageLoadingDispatchType,
  LOAD_TYPE,
  PAGE_LOADING_ACTIONS,
} from "../action";

export interface PageLoadingStateI {
  allPage: boolean;
}

const defaultState: PageLoadingStateI = {
  allPage: false,
};

export const pageLoadingReducer = (
  state: PageLoadingStateI = defaultState,
  action: PageLoadingDispatchType
): PageLoadingStateI => {
  switch (action.type) {
    case PAGE_LOADING_ACTIONS.SET_PAGE_LOADING:
      {
        const newState = { ...state };
        switch (action.payload) {
          case LOAD_TYPE.ALL_PAGE:
            newState.allPage = true;
            break;
        }
        return newState;
      }
      break;

    case PAGE_LOADING_ACTIONS.SET_PAGE_LOADED:
      {
        const newState = { ...state };
        switch (action.payload) {
          case LOAD_TYPE.ALL_PAGE:
            newState.allPage = false;
            break;
        }
        return newState;
      }
      break;
    default:
      return state;
  }
};
