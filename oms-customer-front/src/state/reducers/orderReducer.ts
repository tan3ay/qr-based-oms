import { BrowserCacheUtil, CACH_KEYS } from "../../helper/BrowserCacheUtil";
import { OrderUtil } from "../../helper/OrderUtil";
import { OrderDto } from "../../models/Order";
import { OrderVo } from "../../models/orderVo";
import { OrderDispatchType, ORDER_ACTIONS } from "../action";
const getUuid = require("uuid-by-string");

export interface OrderStateI {
  editOrder: OrderVo;
  viewOrder: OrderVo;
}

const defaultState: OrderStateI = {
  editOrder: OrderUtil.getInitOrder(CACH_KEYS.EDIT_ORDER),
  viewOrder: OrderUtil.getInitOrder(CACH_KEYS.VIEW_ORDER),
};

export const orderReducer = (
  state: OrderStateI = defaultState,
  action: OrderDispatchType
): OrderStateI => {
  switch (action.type) {
    case ORDER_ACTIONS.SET_EDIT_ORDER:
      BrowserCacheUtil.put(
        JSON.stringify(action.payload),
        CACH_KEYS.EDIT_ORDER,
        60
      );
      // BrowserCacheUtil.putOrder(JSON.stringify(action.payload));
      return {
        ...state,
        editOrder: action.payload,
      };
      break;
    case ORDER_ACTIONS.SET_VIEW_ORDER:
      BrowserCacheUtil.put(
        JSON.stringify(action.payload),
        CACH_KEYS.VIEW_ORDER,
        60
      );
      // BrowserCacheUtil.putOrder(JSON.stringify(action.payload));
      return {
        ...state,
        viewOrder: action.payload,
      };
      break;
    case ORDER_ACTIONS.SET_ORDER_ITEM:
      {
        const newOrder = { ...state.editOrder };

        const targetIdx = newOrder.itemsVo.findIndex(
          ({ itemId }) => itemId === action.payload.itemId
        );

        if (targetIdx === -1) newOrder.itemsVo.push(action.payload);
        else {
          newOrder.itemsVo[targetIdx] = action.payload;
        }
        // filter Items
        // Remove items with 0 quantity
        OrderUtil.filterItem(newOrder);

        // Update total cost
        newOrder.cost = OrderUtil.getCost(newOrder);
        BrowserCacheUtil.put(
          JSON.stringify(newOrder),
          CACH_KEYS.EDIT_ORDER,
          60
        );

        return {
          ...state,
          editOrder: newOrder,
        };
      }
      break;

    case ORDER_ACTIONS.SET_CUSTOM_ORDER_ITEM:
      {
        const newOrder = { ...state.editOrder };

        const uuid = getUuid(
          action.payload.itemName + action.payload.itemDetail,
          5
        );
        action.payload.itemId = action.payload.itemId + "_" + uuid;

        if (newOrder.itemsVo) {
          const targetIdx = newOrder.itemsVo.findIndex(
            ({ itemId }) => itemId === action.payload.itemId
          );

          if (targetIdx === -1) {
            newOrder.itemsVo.push(action.payload);
          } else {
            const newQuantity =
              newOrder.itemsVo[targetIdx].itemQuantity +
              action.payload.itemQuantity;
            newOrder.itemsVo[targetIdx] = {
              ...action.payload,
              itemQuantity: newQuantity,
            };
          }
          // filter Items
          // Remove items with 0 quantity
          OrderUtil.filterItem(newOrder);

          // Update total cost
          newOrder.cost = OrderUtil.getCost(newOrder);
          BrowserCacheUtil.put(
            JSON.stringify(newOrder),
            CACH_KEYS.EDIT_ORDER,
            60
          );
        }

        return {
          ...state,
          editOrder: newOrder,
        };
      }
      break;

    default:
      return state;
  }
};
