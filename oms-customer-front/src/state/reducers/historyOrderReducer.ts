import { BrowserCacheUtil, CACH_KEYS } from "../../helper/BrowserCacheUtil";
import { OrderDto } from "../../models/Order";
import { HistoryOrderDispatchTypes, HISTORY_ORDER_ACTIONS } from "../action";

export interface OrderStateI {
  updateTime: number;
  orders: OrderDto[];
}

const defaultState: OrderStateI = {
  updateTime: new Date().valueOf(),
  orders: BrowserCacheUtil.get(CACH_KEYS.HISTORY_ORDERS)
    ? BrowserCacheUtil.get(CACH_KEYS.HISTORY_ORDERS)
    : [],
};

export const historyOrderReducer = (
  state: OrderStateI = defaultState,
  action: HistoryOrderDispatchTypes
): OrderStateI => {
  switch (action.type) {
    case HISTORY_ORDER_ACTIONS.LOAD_ORDER_SUCCESS:
      const newOrders = [...state.orders, action.payload];

      const newState: OrderStateI = {
        updateTime: new Date().valueOf(),
        orders: action.payload,
      };
      return newState;
      break;
    case HISTORY_ORDER_ACTIONS.ORDER_ADDED:
      {
        const newOrders = [...state.orders, action.payload];
        BrowserCacheUtil.put(
          JSON.stringify(newOrders),
          CACH_KEYS.HISTORY_ORDERS,
          3 * 60
        );
        return { ...state, orders: newOrders };
      }
      break;
    case HISTORY_ORDER_ACTIONS.ORDER_REMOVED:
      {
        const newOrders = [...state.orders];
        const filteredOrders = newOrders.filter(
          (order) => order.orderId === action.payload.orderId
        );
        BrowserCacheUtil.put(
          JSON.stringify(filteredOrders),
          CACH_KEYS.HISTORY_ORDERS,
          3 * 60
        );

        return { ...state, orders: filteredOrders };
      }
      break;
    default:
      return state;
  }
};
