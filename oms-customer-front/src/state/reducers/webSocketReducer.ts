import { WebSocketEvent } from "../../models/event";
import {
  WebSocketDispatchType,
  WEBSOCKET_ACTIONS,
  WEBSOCKET_STATE,
} from "../action";

export const WS_BASE =
  "wss://8irvmkg0qk.execute-api.us-east-2.amazonaws.com/dev?restaurantId=1629791235991";
export interface WebSocketStateI {
  webSocketState: number;
  gsw: WebSocket;
  pendingNotif: WebSocketEvent[];
}

const defaultState: WebSocketStateI = {
  webSocketState: WEBSOCKET_STATE.CONNECTING,
  gsw: new WebSocket(WS_BASE),
  pendingNotif: [],
};

export const webSocketReducer = (
  state: WebSocketStateI = defaultState,
  action: WebSocketDispatchType
): WebSocketStateI => {
  switch (action.type) {
    case WEBSOCKET_ACTIONS.UPDATE_WEBSOCKET_STATE:
      return {
        ...state,
        webSocketState: action.payload,
      };
      break;
    case WEBSOCKET_ACTIONS.SET_WEBSOCKET:
      return {
        ...state,
        gsw: action.payload,
      };
      break;
    case WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATION:
      return {
        ...state,
        pendingNotif: [...state.pendingNotif, action.payload],
      };
      break;
    case WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATIONS:
      const notification = [...action.payload];
      return {
        ...state,
        pendingNotif: notification,
      };
      break;
    default:
      return state;
  }
};
