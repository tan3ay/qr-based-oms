import { BrowserCacheUtil } from "../../helper/BrowserCacheUtil";
import { OrderUtil } from "../../helper/OrderUtil";
import { MenuDto } from "../../models/menu";
import { MenuDispatchType, MENU_ACTIONS } from "../action";

export interface MenuStateI {
  menuItems: MenuDto[];
}

const defaultState: MenuStateI = {
  menuItems: OrderUtil.getInitMenu(),
};

export const menuReducer = (
  state: MenuStateI = defaultState,
  action: MenuDispatchType
): MenuStateI => {
  switch (action.type) {
    case MENU_ACTIONS.SET_MENU:
      BrowserCacheUtil.putMenu(JSON.stringify(action.payload));
      return {
        ...state,
        menuItems: action.payload,
      };
      break;
    case MENU_ACTIONS.SET_MENU_ITEM:
      if (state.menuItems) {
        const targetIdx = state.menuItems.findIndex(
          ({ menuId }) => menuId === action.payload.menuId
        );
        const update = [...state.menuItems];
        update[targetIdx] = action.payload;
        return { ...state, menuItems: update };
      } else return state;

      break;
    default:
      return state;
  }
};
