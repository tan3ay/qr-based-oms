import { combineReducers } from "redux";
import { historyOrderReducer } from "./historyOrderReducer";
import { menuReducer } from "./menuReducer";
import { orderReducer } from "./orderReducer";
import { pageErrorReducer } from "./pageErrorReducer";
import { pageLoadingReducer } from "./pageLoadingReducer";
import { restaurantReducer } from "./restaurantReducer";
import { webSocketReducer } from "./webSocketReducer";

export const reducers = combineReducers({
  menu: menuReducer,
  order: orderReducer,
  restaurant: restaurantReducer,
  webSocket: webSocketReducer,
  pageLoadingState: pageLoadingReducer,
  pageError: pageErrorReducer,
  historyOrders: historyOrderReducer,
});

//export default reducers;
