import { RestaurantService } from "../../api/restaurant-service";
import { BrowserCacheUtil } from "../../helper/BrowserCacheUtil";
import { RestaurantDto } from "../../models/restaurantMeta";

import { RestaurantDispatchType, RESTAURANT_ACTIONS } from "../action";

export interface RestaurantStateI {
  restaurantMeta: RestaurantDto | null;
}

const defaultState: RestaurantStateI = {
  restaurantMeta: BrowserCacheUtil.getRestaurant(),
};

export const restaurantReducer = (
  state: RestaurantStateI = defaultState,
  action: RestaurantDispatchType
): RestaurantStateI => {
  switch (action.type) {
    case RESTAURANT_ACTIONS.SET_RESTAURANT_META:
      return {
        ...state,
        restaurantMeta: action.payload,
      };
      break;
    default:
      return state;
  }
};
