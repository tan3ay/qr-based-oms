import { ERROR_TYPE } from "../../components/error-fallback/error-fallback";
import { WebSocketEvent } from "../../models/event";
import { MenuDto } from "../../models/menu";
import { OrderDto, OrderItemDto, ORDER_ITEM_STATUS } from "../../models/Order";
import { OrderItemVo, OrderVo } from "../../models/orderVo";
import { RestaurantDto } from "../../models/restaurantMeta";

export enum MENU_ACTIONS {
  SET_MENU = "SET_MENU",
  SET_MENU_ITEM = "SET_MENU_ITEM",
}

interface setMenu {
  type: MENU_ACTIONS.SET_MENU;
  payload: MenuDto[];
}

interface setMenuItem {
  type: MENU_ACTIONS.SET_MENU_ITEM;
  payload: MenuDto;
}
export type MenuDispatchType = setMenu | setMenuItem;

export enum ORDER_ACTIONS {
  SET_EDIT_ORDER = "SET_EDIT_ORDER",
  SET_VIEW_ORDER = "SET_VIEW_ORDER",
  SET_ORDER_ITEM = "SET_ORDER_ITEM",
  SET_CUSTOM_ORDER_ITEM = "SET_CUSTOM_ORDER_ITEM",
}

interface setEditOrder {
  type: ORDER_ACTIONS.SET_EDIT_ORDER;
  payload: OrderVo;
}

interface setViewOrder {
  type: ORDER_ACTIONS.SET_VIEW_ORDER;
  payload: OrderVo;
}

interface setOrderItem {
  type: ORDER_ACTIONS.SET_ORDER_ITEM;
  payload: OrderItemVo;
}

interface setCustomOrderItem {
  type: ORDER_ACTIONS.SET_CUSTOM_ORDER_ITEM;
  payload: OrderItemVo;
}
export type OrderDispatchType =
  | setEditOrder
  | setViewOrder
  | setOrderItem
  | setCustomOrderItem;

// Restaurant Meta Information
export enum RESTAURANT_ACTIONS {
  SET_RESTAURANT_META = "SET_RESTAURANT_META",
}

interface setRestaurantMeta {
  type: RESTAURANT_ACTIONS.SET_RESTAURANT_META;
  payload: RestaurantDto;
}
export type RestaurantDispatchType = setRestaurantMeta;

// Websocket Meta Information
// 0 closed
// 1 Connecting
// 2 Connected
export enum WEBSOCKET_STATE {
  DISCONNECTED = 0,
  CONNECTING = 1,
  CONNECTED = 2,
}
export enum WEBSOCKET_ACTIONS {
  UPDATE_WEBSOCKET_STATE = "UPDATE_WEBSOCKET_STATE",
  SET_WEBSOCKET = "SET_WEBSOCKET",
  SET_PENDING_NOTIFICATION = "SET_PENDING_NOTIFICATION",
  SET_PENDING_NOTIFICATIONS = "SET_PENDING_NOTIFICATIONS",
}

interface setWebSocketState {
  type: WEBSOCKET_ACTIONS.UPDATE_WEBSOCKET_STATE;
  payload: number;
}
interface setWebSocket {
  type: WEBSOCKET_ACTIONS.SET_WEBSOCKET;
  payload: WebSocket;
}
interface setPendingNotification {
  type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATION;
  payload: WebSocketEvent;
}
interface setPendingNotifications {
  type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATIONS;
  payload: WebSocketEvent[];
}
export type WebSocketDispatchType =
  | setWebSocketState
  | setWebSocket
  | setPendingNotification
  | setPendingNotifications
  | setPendingNotifications;

export enum LOAD_TYPE {
  ALL_PAGE = "ALL_PAGE",
}

export enum PAGE_LOADING_ACTIONS {
  SET_PAGE_LOADING = "SET_PAGE_LOADING",
  SET_PAGE_LOADED = "SET_PAGE_LOADED",
}
interface setPageLoadingState {
  type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADING;
  payload: LOAD_TYPE;
}
interface setPageLoadedState {
  type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADED;
  payload: LOAD_TYPE;
}

export type PageLoadingDispatchType = setPageLoadingState | setPageLoadedState;

export enum ERROR_PAGE {
  ALL_PAGE = "ALL_PAGE",
}
export enum PAGE_ERROR_ACTIONS {
  SET_PAGE_ERROR = "SET_PAGE_ERROR",
  SET_PAGE_LOADED = "SET_PAGE_LOADED",
}
interface setPageErrorState {
  type: PAGE_ERROR_ACTIONS.SET_PAGE_ERROR;
  payload: { page: ERROR_PAGE; error: ERROR_TYPE };
}

export type PageErrorDispatchType = setPageErrorState;

/*
 TODO MERGE REMOVE UPDATE_ORDER & ORDER_FINISH & change it to ORDER_STATUS_UPDATE
 */
export enum HISTORY_ORDER_ACTIONS {
  LOAD_ORDER_SUCCESS = "LOAD_HISTORY_ORDER_SUCCESS",
  ORDER_ADDED = "ORDER_ADDED",
  ORDER_REMOVED = "ORDER_REMOVED",
}

interface orderLoaded {
  type: HISTORY_ORDER_ACTIONS.LOAD_ORDER_SUCCESS;
  payload: OrderDto[];
}

interface addOrder {
  type: HISTORY_ORDER_ACTIONS.ORDER_ADDED;
  payload: OrderDto;
}

interface removeOrder {
  type: HISTORY_ORDER_ACTIONS.ORDER_REMOVED;
  payload: OrderDto;
}

export type HistoryOrderDispatchTypes = orderLoaded | addOrder | removeOrder;
