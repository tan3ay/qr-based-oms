import { Dispatch } from "react";
import { ERROR_TYPE } from "../../components/error-fallback/error-fallback";
import { WebSocketEvent } from "../../models/event";
import { MenuDto } from "../../models/menu";
import { OrderDto } from "../../models/Order";
import { OrderItemVo, OrderVo } from "../../models/orderVo";
import { RestaurantDto } from "../../models/restaurantMeta";
import {
  ERROR_PAGE,
  HistoryOrderDispatchTypes,
  HISTORY_ORDER_ACTIONS,
  LOAD_TYPE,
  MenuDispatchType,
  MENU_ACTIONS,
  OrderDispatchType,
  ORDER_ACTIONS,
  PageErrorDispatchType,
  PageLoadingDispatchType,
  PAGE_ERROR_ACTIONS,
  PAGE_LOADING_ACTIONS,
  RestaurantDispatchType,
  RESTAURANT_ACTIONS,
  WebSocketDispatchType,
  WEBSOCKET_ACTIONS,
} from "../action";

// Order Item Actions

export const increaseQuantity = (menuItem: MenuDto) => {
  return (dispatch: Dispatch<MenuDispatchType>) => {
    menuItem.quantity = menuItem.quantity + 1;
    dispatch({
      type: MENU_ACTIONS.SET_MENU_ITEM,
      payload: { ...menuItem, quantity: menuItem.quantity },
    });
  };
};

export const decreaseQuantity = (menuItem: MenuDto) => {
  return (dispatch: Dispatch<MenuDispatchType>) => {
    if (menuItem.quantity == 0) return;
    menuItem.quantity = menuItem.quantity - 1;
    dispatch({
      type: MENU_ACTIONS.SET_MENU_ITEM,
      payload: menuItem,
    });
  };
};

export const setMenu = (menus: MenuDto[]) => {
  return (dispatch: Dispatch<MenuDispatchType>) => {
    dispatch({
      type: MENU_ACTIONS.SET_MENU,
      payload: menus,
    });
  };
};

// Order Actions
export const setEditOrder = (order: OrderVo) => {
  return (dispatch: Dispatch<OrderDispatchType>) => {
    dispatch({
      type: ORDER_ACTIONS.SET_EDIT_ORDER,
      payload: order,
    });
  };
};

export const setViewOrder = (order: OrderVo) => {
  return (dispatch: Dispatch<OrderDispatchType>) => {
    dispatch({
      type: ORDER_ACTIONS.SET_VIEW_ORDER,
      payload: order,
    });
  };
};

export const setOrderItem = (orderItem: OrderItemVo) => {
  return (dispatch: Dispatch<OrderDispatchType>) => {
    dispatch({
      type: ORDER_ACTIONS.SET_ORDER_ITEM,
      payload: orderItem,
    });
  };
};

export const setCustomOrderItem = (orderItem: OrderItemVo) => {
  return (dispatch: Dispatch<OrderDispatchType>) => {
    dispatch({
      type: ORDER_ACTIONS.SET_CUSTOM_ORDER_ITEM,
      payload: orderItem,
    });
  };
};

// set Restaurant

export const setRestaurant = (restaurant: RestaurantDto) => {
  return (dispatch: Dispatch<RestaurantDispatchType>) => {
    dispatch({
      type: RESTAURANT_ACTIONS.SET_RESTAURANT_META,
      payload: restaurant,
    });
  };
};

// Websocket
export const setWebSocketState = (state: number) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.UPDATE_WEBSOCKET_STATE,
      payload: state,
    });
  };
};

export const setWebSocket = (ws: WebSocket) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.SET_WEBSOCKET,
      payload: ws,
    });
  };
};

export const setPendingNotification = (notification: WebSocketEvent) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATION,
      payload: notification,
    });
  };
};
export const setPendingNotifications = (notification: WebSocketEvent[]) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATIONS,
      payload: notification,
    });
  };
};

// PageLoading

export const setDataLoading = (loading: boolean) => {
  if (loading)
    return (dispatch: Dispatch<PageLoadingDispatchType>) => {
      dispatch({
        type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADING,
        payload: LOAD_TYPE.ALL_PAGE,
      });
    };
  else
    return (dispatch: Dispatch<PageLoadingDispatchType>) => {
      dispatch({
        type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADED,
        payload: LOAD_TYPE.ALL_PAGE,
      });
    };
};

// PageError

export const setPageError = (errorState: boolean, error: ERROR_TYPE) => {
  const page = ERROR_PAGE.ALL_PAGE;
  return (dispatch: Dispatch<PageErrorDispatchType>) => {
    dispatch({
      type: PAGE_ERROR_ACTIONS.SET_PAGE_ERROR,
      payload: { page, error },
    });
  };
};

// HIstory Order Actions
export const setHistoryOrders = (orders: OrderDto[]) => {
  return (dispatch: Dispatch<HistoryOrderDispatchTypes>) => {
    dispatch({
      type: HISTORY_ORDER_ACTIONS.LOAD_ORDER_SUCCESS,
      payload: orders,
    });
  };
};

export const addHistoryOrder = (order: OrderDto) => {
  return (dispatch: Dispatch<HistoryOrderDispatchTypes>) => {
    dispatch({
      type: HISTORY_ORDER_ACTIONS.ORDER_ADDED,
      payload: order,
    });
  };
};

export const removeHistoryOrder = (order: OrderDto) => {
  return (dispatch: Dispatch<HistoryOrderDispatchTypes>) => {
    dispatch({
      type: HISTORY_ORDER_ACTIONS.ORDER_ADDED,
      payload: order,
    });
  };
};
