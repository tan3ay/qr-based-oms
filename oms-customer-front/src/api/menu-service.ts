import { MenuDto } from "../models/menu";
import { API } from "./base-api";

const MENU_ENDPOINT = "/menu";
const MENUS_ENDPOINT = "/menus";

export const MenuService = {
  async getMenus(restaurantId: string) {
    const param = {
      restaurantId: restaurantId,
    };
    return await API.makeGetRequest(MENUS_ENDPOINT, param);
  },

  getMenu(restaurantId: string, orderId: string) {
    const param = {
      restaurantId: restaurantId,
      orderId: orderId,
    };
    return API.makeGetRequest(MENU_ENDPOINT, param);
  },
  async postMenu(restaurantId: string, menu: MenuDto) {
    const param = {
      restaurantId: restaurantId,
      menu,
    };
    return await API.makePostRequest(MENU_ENDPOINT, JSON.stringify(menu));
  },
};
