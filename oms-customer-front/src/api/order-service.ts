import _ from "lodash";
import { WS_BASE } from "../App";
import { OrderUtil } from "../helper/OrderUtil";
import { EVENT_TYPE, WebSocketEvent, EventData } from "../models/event";
import { OrderDto } from "../models/Order";
import { setWebSocketState } from "../state/action-creators";
import { API } from "./base-api";

const ORDERS_ENDPOINT = "/orders";
const ORDER_ENDPOINT = "/order";
export const OrderService = {
  getOrders(restaurantId: string) {
    const param = {
      restaurantId: restaurantId,
    };
    return API.makeGetRequest(ORDERS_ENDPOINT, param);
  },

  async postOrder(order: OrderDto) {
    return await API.makePostRequest(ORDER_ENDPOINT, JSON.stringify(order));
  },
  getWsEvent(restaurantId: string, order: OrderDto) {
    const notifications: EventData[] = order.kitchenStatus.map((kitchen) => {
      // Create order for each kitchen
      let currentKitchenOrder: OrderDto = OrderUtil.filterKitchenOrder(
        order,
        kitchen.kitchenId
      );

      return {
        type: "Info",
        target: "kitchen",
        targetId: kitchen.kitchenId,
        order: currentKitchenOrder,
      };
    });
    const event: WebSocketEvent = {
      restaurantId: restaurantId,
      type: EVENT_TYPE.ORDER_CREATED,
      data: JSON.stringify(notifications),
    };
    return event;
  },

  getOrder(restaurantId: string, orderId: string) {
    const param = {
      restaurantId: restaurantId,
      orderId: orderId,
    };
    return API.makeGetRequest(ORDER_ENDPOINT, param);
  },
};
