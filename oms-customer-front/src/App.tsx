import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import "./App.css";
import FoodMenuAddPage from "./pages/food-menu-add-page";
import FoodMenuPage from "./pages/food-menu-page";
import FoodOrderFinishPage from "./pages/food-order-finish-page";
import FoodOrderHistoryPage from "./pages/food-order-history-page";
import FoodOrderPage from "./pages/food-order-page";
import RestaurantHome from "./pages/restaurant-home";
import { WEBSOCKET_STATE } from "./state/action";
import {
  setPendingNotifications,
  setWebSocket,
  setWebSocketState,
} from "./state/action-creators";
import { RootState } from "./state/store";

export const WS_BASE =
  "wss://8irvmkg0qk.execute-api.us-east-2.amazonaws.com/dev?restaurantId=1629791235991";

const App: React.FC = () => {
  const dispatch = useDispatch();
  const gwsState = useSelector(
    (state: RootState) => state.webSocket.webSocketState
  );
  const gws = useSelector((state: RootState) => state.webSocket.gsw);
  const pendingNotif = useSelector(
    (state: RootState) => state.webSocket.pendingNotif
  );
  //console.log('Connected '+new Date())

  function initWebSocket() {
    if (
      gwsState === WEBSOCKET_STATE.DISCONNECTED &&
      (!gws || gws.readyState > 1)
    ) {
      console.log("Setting web Socket");
      dispatch(setWebSocketState(WEBSOCKET_STATE.CONNECTING));
      setTimeout(() => {
        dispatch(setWebSocket(new WebSocket(WS_BASE)));
      }, 1000);
    }
  }
  gws.onopen = function () {
    console.log("onopen: Connected " + new Date());
    dispatch(setWebSocketState(WEBSOCKET_STATE.CONNECTED));
    if (pendingNotif.length > 0) {
      console.log(
        "onopen: Sending pending notification " + new Date(),
        pendingNotif.length
      );
      pendingNotif.forEach((notification) => {
        // Action is compulsory for routekey
        gws.send(JSON.stringify({ action: "notify", data: notification }));
      });
      dispatch(setPendingNotifications([]));
      gws.close();
    }
  };
  gws.onclose = function () {
    console.log("connection closed" + new Date());
    // Currently customer front no need to reconnect websocket on close
    // Instead we should create temporary web socket to send notif if gsw is closed & reaconnect it that time
    // dispatch(setWebSocketState(WEBSOCKET_STATE.DISCONNECTED));
  };

  useEffect(() => {
    initWebSocket();
  }, [gwsState]);

  return (
    //<div>Congratulations for your first business Tan3ay...
    <div className="root">
      {/* <BrowserRouter>
        <WebSocketContext.Provider value={gws}> */}
      <BrowserRouter>
        <Route
          path="/restaurant/:restaurantId"
          component={RestaurantHome}
        ></Route>
        <Route path="/menu/" component={FoodMenuPage}></Route>
        <Route path="/orderHistory/" component={FoodOrderHistoryPage}></Route>
        <Route path="/addMenu/:menuId" component={FoodMenuAddPage}></Route>
        <Route path="/order/" component={FoodOrderPage}></Route>
        <Route path="/orderSent/" component={FoodOrderFinishPage}></Route>
        <Route path="/error" component={() => <div>404 Not Found</div>}></Route>
      </BrowserRouter>
      {/* </WebSocketContext.Provider>
      </BrowserRouter> */}
    </div>
  );
};

export default App;
