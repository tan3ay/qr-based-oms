import { useSelector } from "react-redux";
import { MenuDto } from "../models/menu";
import {
  OrderCostDto,
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../models/Order";
import { OrderItemVo, OrderVo } from "../models/orderVo";
import { OrderDispatchType } from "../state/action";
import { RootState } from "../state/store";
import { BrowserCacheUtil, CACH_KEYS } from "./BrowserCacheUtil";
import * as _ from "lodash";
import { MenuDtoVo } from "../models/menuVo";

export const OrderUtil = {
  getOrderItemVo(menu: MenuDtoVo): OrderItemVo {
    return {
      itemId: menu.menuId,
      itemName: menu.name,
      itemDetail: menu.desc,
      itemQuantity: 0,
      itemCost: menu.cost,
      itemStatus: ORDER_ITEM_STATUS.PLACED.valueOf(),
      isCustomizable: menu.isCustomizable,
      customizableValue: {},
      kitchenId: menu.kitchenId,
      isDeleted: false,
    };
  },

  findOrderItem(menuItem: MenuDto, order: OrderVo): OrderItemVo | undefined {
    return order.itemsVo.find((item) => item.itemId == menuItem.menuId);
  },

  getQuantity(menuItem: MenuDto, order: OrderVo): number {
    if (menuItem.isCustomizable !== 1) {
      const orderItem = order.itemsVo?.find(
        (item) => item.itemId == menuItem.menuId
      );
      if (orderItem != null) return orderItem.itemQuantity;
      return 0;
    }
    if (menuItem.isCustomizable === 1) {
      let quantity = 0;
      order.itemsVo?.forEach((item) => {
        if (item.itemId.startsWith(menuItem.menuId))
          quantity += item.itemQuantity;
      });
      return quantity;
    }
    return 0;
  },

  creatOrder(menuDtos: MenuDto[]): OrderVo {
    let order: OrderVo = this.getEmptyOrder();
    menuDtos.forEach((item) => {
      if (item.quantity > 0) {
        let currItemCost: number = item.quantity * item.cost;
        order.itemsVo?.push(this.getOrderItemVo(item));
      }
    });
    order.cost = this.getCost(order);
    return order;
  },

  creatOrderSto(menuDtos: MenuDto[]): OrderVo {
    let order: OrderVo = this.getEmptyOrder();
    menuDtos.forEach((item) => {
      if (item.quantity > 0) {
        let currItemCost: number = item.quantity * item.cost;
        order.itemsVo?.push(this.getOrderItemVo(item));
      }
    });
    order.cost = this.getCost(order);
    return order;
  },

  filterKitchenOrder(order: OrderDto, kitchenId: string): OrderDto {
    let newOrder: OrderDto = _.cloneDeep(order);
    newOrder.items.filter((item) => item.kitchenId !== kitchenId);
    newOrder.cost = this.getCost(newOrder as OrderVo);
    return newOrder;
  },

  getCombineOrderStatus(order: OrderDto): ORDER_STATUS {
    // If all kitchen status x Order status x
    // If Any kitchen status < served Order status is Cooking
    let isAllKitchenItemStatusSame = true;
    let tempStatus: ORDER_STATUS = ORDER_STATUS.INVALID;
    order.kitchenStatus.forEach((item) => {
      if (tempStatus === ORDER_STATUS.INVALID) {
        tempStatus = item.status;
      } else if (tempStatus !== item.status) {
        isAllKitchenItemStatusSame = false;
      }
    });

    if (!isAllKitchenItemStatusSame) {
      return ORDER_STATUS.PREPARING;
    }
    return tempStatus;
  },
  getOrderItemStatusString(status: ORDER_ITEM_STATUS): string {
    if (status === ORDER_ITEM_STATUS.PREPARING) return "Preparing";
    else if (status === ORDER_ITEM_STATUS.SERVED) return "Served";
    else if (status === ORDER_ITEM_STATUS.PLACED) return "Placed";
    else return "Wrong Order Status";
  },
  getOrderItemColorString(status: ORDER_ITEM_STATUS): string {
    if (status === ORDER_ITEM_STATUS.PREPARING) return "yellow";
    else if (status === ORDER_ITEM_STATUS.SERVED) return "green";
    else if (status === ORDER_ITEM_STATUS.PLACED) return "white";
    else return "gray";
  },
  getOrderStatusText(status: string): string {
    switch (status) {
      case "1":
        return "New";
      case "2":
        return "Cooking";
      case "3":
        return "Served";
      case "4":
        return "Finish";
      case "5":
        return "Deleted";
      default:
        return "";
    }
  },

  getInitOrder(type: CACH_KEYS): OrderVo {
    const cachedOrder = BrowserCacheUtil.get(type);
    if (cachedOrder != null) return cachedOrder;
    return this.getEmptyOrder();
  },

  getInitMenu(): MenuDto[] {
    const cachedMenu = BrowserCacheUtil.getMenu();
    if (cachedMenu != null) return cachedMenu;
    return [];
  },

  getEmptyOrder(): OrderVo {
    return {
      restaurantId: "1",
      orderId: "",
      orderKey: "",
      status: ORDER_STATUS.PLACED,
      tableId: "",
      cost: {
        totalCost: 0,
        gst: 0,
        subTotal: 0,
      },
      orderNotes: "",
      items: [],
      itemsVo: [],
      createdAt: 0,
      updatedAt: 0,
      kitchenStatus: [],
    };
  },

  getCost(order: OrderVo): OrderCostDto {
    const TAX = 0;
    let subTotal: number = 0;
    order.itemsVo?.forEach((item) => {
      subTotal += item.itemQuantity * item.itemCost;
    });
    const gst = (subTotal * TAX) / 100;
    const totalCost = subTotal + gst;
    return {
      totalCost: totalCost,
      gst: gst,
      subTotal: subTotal,
    };
  },

  filterItem(order: OrderVo): OrderVo {
    order.itemsVo = order.itemsVo?.filter((item) => item.itemQuantity > 0);
    return order;
  },
};
