import fs from "fs";
import { MenuDto } from "../models/menu";
import { OrderDto } from "../models/Order";
import { OrderVo } from "../models/orderVo";
import { RestaurantDto } from "../models/restaurantMeta";

interface cachItem {
  expiry: number;
  value: string;
}

export enum CACH_KEYS {
  HISTORY_ORDERS = "HISTORY_ORDERS",
  EDIT_ORDER = "EDIT_ORDER",
  VIEW_ORDER = "VIEW_ORDER",
}

export const BrowserCacheUtil = {
  put(content: string, cashKey: string, ttlMin: number): void {
    const ttl = ttlMin * 1000 * 60;
    const later = new Date().getTime() + ttl;
    const key: string = cashKey;
    const item: cachItem = {
      value: content,
      expiry: later,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },
  // Use to append to array

  append(content: string, cashKey: string, ttlMin: number): void {
    let currentData: Array<any> = JSON.parse(this.get(cashKey));
    currentData = currentData === null ? [] : currentData;
    console.log("before", currentData);
    currentData.push(JSON.parse(content));
    console.log("after", currentData);

    // const ttl= ttlMin*1000*60;
    // const later = new Date().getTime()+ttl;
    // const key:string = cashKey;
    // const item:cachItem={
    //     value:content,
    //     expiry:later
    // }
    // localStorage.setItem(key, JSON.stringify(item));
  },

  get: (cashKey: string): any => {
    const key: string = cashKey;
    const str = localStorage.getItem(key);
    if (!str) {
      return null;
    }
    const item: cachItem = JSON.parse(str);
    if (new Date().getTime() > item.expiry) {
      localStorage.removeItem(key);
      return null;
    }
    return JSON.parse(item.value);
  },

  putMenu: (content: string): void => {
    const ttl = 1000 * 15 * 60;
    const later = new Date().getTime() + ttl;
    const key: string = "MENU";
    const item: cachItem = {
      value: content,
      expiry: later,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },
  getMenu: (): MenuDto[] | null => {
    const key: string = "MENU";
    const menuStr = localStorage.getItem(key);
    if (!menuStr) {
      return null;
    }
    const item: cachItem = JSON.parse(menuStr);
    if (new Date().getTime() > item.expiry) {
      localStorage.removeItem(key);
      return null;
    }
    return JSON.parse(item.value);
  },

  putRestaurant: (content: string): void => {
    const ttl = 1000 * 60 * 60;
    const later = new Date().getTime() + ttl;
    const key: string = "KEY_RESTAURANT";
    const item: cachItem = {
      value: content,
      expiry: later,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },
  getRestaurant: (): RestaurantDto | null => {
    const key: string = "KEY_RESTAURANT";
    const str = localStorage.getItem(key);
    if (!str) {
      return null;
    }
    const item: cachItem = JSON.parse(str);
    if (new Date().getTime() > item.expiry) {
      localStorage.removeItem(key);
      return null;
    }
    return JSON.parse(item.value);
  },
};
