export function isEmpty(obj: any) {
  for (var i in obj) return false;
  return true;
}

export function getCostString(cost: number) {
  if (cost === 0 || cost === null) {
    return "";
  }
  //Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(cost);
  return Intl.NumberFormat("de-DE", {
    style: "currency",
    currency: "EUR",
  }).format(cost);
}

export function convertToNumber(cost: any): number {
  if (cost as string) {
    return parseFloat(cost);
  }
  if (cost as number) {
    return cost;
  } else {
    return 0;
  }
}
