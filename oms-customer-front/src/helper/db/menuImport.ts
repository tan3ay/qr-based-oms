import { MenuService } from "../../api/menu-service";
import { MenuDto } from "../../models/menu";
import { menuData } from "./menuData";



async function importMenu(){

    var menuJsonData = require('./menuData.json');
    let dataString = JSON.stringify(menuJsonData)

    const menus:MenuDto[]=JSON.parse(dataString);

    //await MenuService.postMenu('1',menus[1]);
    let i =0;
    menus.forEach(async (item)=>{
        console.log(item);
       try{
           console.log('sending',item);
            const res= await MenuService.postMenu('1',item);
            console.log('sent',res);
       }catch(e){
           console.log(e);
       }
       
    });
}

importMenu();