import { MenuDto } from "../models/menu";
import { SearchVo } from "../pages/food-menu-page";
import MenuListService from "./MenuUtil";

export const MenuSearchService = {
  fiterByName: (menuItems: MenuDto[], value: string): any => {
    if (value === "") {
      return MenuListService.getMenuListEntities(menuItems);
    } else {
      let filteredMenuList = menuItems.filter((item: MenuDto) => {
        return item.name.toLowerCase().indexOf(value.toLowerCase()) > -1;
      });
      return MenuListService.getMenuListEntities(filteredMenuList);
    }
  },

  fiterBySearchVo: (menuItems: MenuDto[], searchVo: SearchVo): any => {
    const keyword = searchVo.key;
    const trueFilters = new Set<string>();

    searchVo.filters.forEach((item) => {
      if (item.selected) trueFilters.add(item.id);
    });

    const isAnyFilterMatch = (
      trueFilters: Set<string>,
      item: MenuDto
    ): boolean => {
      let result = false;
      trueFilters.forEach((filter: string) => {
        if (
          item.characteristics[filter] &&
          item.characteristics[filter] === 1
        ) {
          result = true;
        }
      });
      return result;
    };

    const isAllFilterMatch = (
      trueFilters: Set<string>,
      item: MenuDto
    ): boolean => {
      let result = true;
      trueFilters.forEach((filter: string) => {
        if (
          item.characteristics[filter] !== null &&
          item.characteristics[filter] !== 1
        ) {
          result = false;
        }
      });
      return result;
    };

    if (keyword !== "" || trueFilters.size > 0) {
      const filteredMenus = menuItems.filter((item) => {
        let keyMatch = false,
          filterMatch = false;
        if (keyword == "") {
          keyMatch = true;
        } else if (item.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1)
          keyMatch = true;
        if (trueFilters.size === 0) {
          filterMatch = true;
          //  }else if (trueFilters.has(item.status+"")){
        } else if (isAllFilterMatch(trueFilters, item)) {
          filterMatch = true;
        }
        return keyMatch && filterMatch;
      });
      console.log("search", filteredMenus);
      return MenuListService.getMenuListEntities(filteredMenus);
    }
    return MenuListService.getMenuListEntities(menuItems);
  },
};
