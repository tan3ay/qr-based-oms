import { Chip } from "@mui/material";
import "./chip-item.scss";

export interface IChipItemProp {
  label: string;
  color: string;
}

export const ChipItem: React.FC<IChipItemProp> = ({ label, color }) => {
  return (
    <div className="ChipItem">
      <div className={`chip-item-container ${color}`}>
        <Chip label={label} />
      </div>
    </div>
  );
};
