import { Chip } from "@mui/material";
import React, { RefObject, useEffect, useRef } from "react";

import "./chip-bar.scss";

export enum SELECT_TYPE {
  SINGLE = "SINGLE",
  MULTI = "MULTI",
}

export interface IChipProp {
  label: string;
  value: string;
  selected: boolean;
}

export interface IChipBarProp {
  chipData: IChipProp[];
  selectType?: string;
  scollToSelect?: boolean;
  onSelectChange: (item: IChipProp[]) => void;
}

export const ChipBar: React.FC<IChipBarProp> = ({
  chipData,
  onSelectChange,
  selectType,
  scollToSelect,
}) => {
  const chipBarRef = useRef<HTMLDivElement>(null);
  let chipToRef = new Map<string, RefObject<HTMLDivElement>>();
  chipData.forEach((category) => {
    const scrollRef = React.createRef<HTMLDivElement>();
    chipToRef.set(category.value, scrollRef);
  });

  const bringToFront = () => {
    let isFirstLoad: boolean = true;
    chipData.forEach((item) => {
      if (item.selected && scollToSelect) {
        isFirstLoad = false;
        if (chipToRef.get(item.value)) {
          let parentLeft = chipBarRef.current?.offsetLeft;
          parentLeft = parentLeft ? parentLeft : 0;
          const eleRef = chipToRef.get(item.value);
          if (eleRef && eleRef.current !== null) {
            let eleLeft = eleRef?.current?.offsetLeft; //?.getBoundingClientRect().x;
            eleLeft = eleLeft ? eleLeft : 0;
            console.log("moving to left", eleLeft, item.value);
            if (eleRef != null && parentLeft != null) {
              chipBarRef.current?.scrollTo({
                left: eleLeft - parentLeft,
                behavior: "smooth",
              });
            }
          }
        }
      }
    });
    if (isFirstLoad) {
      if (chipBarRef != null) {
        chipBarRef.current?.scrollTo({
          left: -100,
          behavior: "smooth",
        });
      }
    }
  };

  useEffect(() => {
    if (
      chipData.length > 0 &&
      selectType &&
      selectType === SELECT_TYPE.SINGLE &&
      bringToFront
    ) {
      bringToFront();
    }
  });

  const handleSelect = (target: IChipProp) => {
    if (selectType && selectType === SELECT_TYPE.SINGLE.valueOf()) {
      chipData.forEach((item) => {
        if (item.label === target.label) item.selected = true;
        else item.selected = false;
      });
    } else {
      chipData.forEach((item) => {
        if (item.label === target.label) item.selected = !item.selected;
      });
    }
    onSelectChange(chipData);
  };

  const chips = chipData.map((item: IChipProp) => {
    const chipStyle = `chip-container${item.selected ? "-selected" : ""}`;

    return (
      <div key={item.label} className={chipStyle}>
        <div ref={chipToRef.get(item.value)}>
          <Chip
            key={item.value}
            label={item.label}
            onClick={() => handleSelect(item)}
          />
        </div>
      </div>
    );
  });
  return (
    <div ref={chipBarRef} className="ChipBar">
      {chips}
    </div>
  );
};
