import { FaBoxOpen, FaDropbox } from "react-icons/fa";
import "./empty-data.scss";
interface ILodingProp {
  icon?: any;
  header: string;
  detail?: string;
}

export const EmptyData: React.FC<ILodingProp> = ({ icon, header, detail }) => {
  return (
    <div className="EmptyData ">
      <div className="icon">
        <FaBoxOpen />
      </div>
      <div className="header">{header}</div>
      <div className="detail">{detail}</div>
    </div>
  );
};
