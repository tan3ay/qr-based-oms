import { Autocomplete, TextField } from "@mui/material";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { OrderService } from "../../api/order-service";
import { BrowserCacheUtil } from "../../helper/BrowserCacheUtil";
import { getCostString } from "../../helper/common-util";
import { OrderUtil } from "../../helper/OrderUtil";
import { OrderDto } from "../../models/Order";
import {
  cloneOrderDtoToVo,
  cloneOrderVoToDto,
  OrderItemVo,
  OrderVo,
} from "../../models/orderVo";
import { TableDto } from "../../models/restaurantMeta";
import { WEBSOCKET_STATE } from "../../state/action";
import {
  addHistoryOrder,
  setDataLoading,
  setEditOrder,
  setOrderItem,
  setPageError,
  setPendingNotification,
  setViewOrder,
  setWebSocketState,
} from "../../state/action-creators";
import { RootState } from "../../state/store";
import { ERROR_TYPE } from "../error-fallback/error-fallback";
import { OrderMenuItem } from "../order-menu-item/order-menu-item";
import { FoodQuantitySelector } from "../quantity-selector/food-quantity-selector";
import "./food-order-edit.scss";

interface IOrderDetailLine {
  left: string;
  right: string;
}

interface IOrderDetailLineProp {
  item: OrderItemVo;
}

const OrderDetailLine: React.FC<IOrderDetailLine> = ({ left, right }) => {
  return (
    <div className="order-line-container h-container">
      <div className="order-line-left header left">{left}</div>
      <div className="order-line-right detail right">{right}</div>
    </div>
  );
};

const OrderDetailItemLine: React.FC<IOrderDetailLineProp> = ({ item }) => {
  const dispatch = useDispatch();
  const handleIncreament = (quantity: number) => {
    dispatch(setOrderItem({ ...item, itemQuantity: quantity }));
  };

  const handleDecreament = (quantity: number) => {
    dispatch(setOrderItem({ ...item, itemQuantity: quantity }));
  };
  const itemDetail = item.isCustomizable ? item.itemDetail : "";
  return (
    <div className="order-line-container h-container pd-2-t  pd-2-b">
      <FoodQuantitySelector
        type="empty"
        size="s"
        quantity={item.itemQuantity}
        onIncreament={handleIncreament}
        onDecreament={handleDecreament}
      />
      <div className="order-line-left pd-3-l left">
        <OrderMenuItem header={item.itemName} detail={itemDetail} />
      </div>
      <div className="order-line-right right">
        {item.itemCost * item.itemQuantity + ""}
      </div>
    </div>
  );
};

interface IFoodOrderEdit {
  order: OrderVo;
}

const FoodOrderEdit: React.FC<IFoodOrderEdit> = ({ order }) => {
  const dispatch = useDispatch();
  // const gws = useContext(WebSocketContext);

  const gws = useSelector((state: RootState) => state.webSocket.gsw);
  const gwsState = useSelector(
    (state: RootState) => state.webSocket.webSocketState
  );
  let history = useHistory();
  let restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );

  // Just to be sure we will try to fetch from browser util as well if restaurant meta is null
  if (restaurantMeta === null) {
    restaurantMeta = BrowserCacheUtil.getRestaurant();
  }

  const tablesVo: TableDto[] = restaurantMeta ? restaurantMeta.tables : [];
  const tables: string[] = tablesVo.map((item) => item.tableNo);

  const orderItem = order.itemsVo.map((item: OrderItemVo) => {
    return (
      <OrderDetailItemLine key={item.itemId} item={item}></OrderDetailItemLine>
    );
  });

  const handleOrderNotesChange = (value: string) => {
    if (value !== "") {
      order.orderNotes = value;
      dispatch(setEditOrder(order));
    }
  };

  const validateForm = (): boolean => {
    if (!isValidTable(order.tableId)) {
      return false;
    }
    return true;
  };

  const handleSendOrder = async () => {
    try {
      if (!validateForm()) {
        return;
      }
      if (restaurantMeta) {
        const orderReady: OrderDto = cloneOrderVoToDto(order);
        orderReady.restaurantId = restaurantMeta?.restaurantId;

        dispatch(setDataLoading(true));
        const data = (await OrderService.postOrder(orderReady)).data;
        dispatch(addHistoryOrder(JSON.parse(JSON.stringify(data.Item))));

        dispatch(setEditOrder(OrderUtil.getEmptyOrder()));
        dispatch(
          setViewOrder(cloneOrderDtoToVo(JSON.parse(JSON.stringify(data.Item))))
        );

        if (!gws || gws.readyState > 1) {
          console.log(
            "food order setting websocket to disconnect" + new Date()
          );
          dispatch(setWebSocketState(WEBSOCKET_STATE.DISCONNECTED));
          console.log("food order setting pending notification" + new Date());
          dispatch(
            setPendingNotification(
              OrderService.getWsEvent(restaurantMeta.restaurantId, orderReady)
            )
          );
        } else if (gws.readyState == 1) {
          //gws.send(JSON.stringify(OrderService.getWsEvent(restaurantMeta.restaurantId,orderReady)));
          gws.send(
            JSON.stringify({
              action: "notify",
              data: OrderService.getWsEvent(
                restaurantMeta.restaurantId,
                orderReady
              ),
            })
          );
          gws.close();
        }

        history.replace("restaurant/" + restaurantMeta.restaurantId);
        history.push("/orderSent");

        dispatch(setDataLoading(false));
      } else {
        console.error("Restaurant information not found");
      }
    } catch (err) {
      dispatch(setDataLoading(false));
      dispatch(setPageError(true, ERROR_TYPE.NETWORK_ERROR));
    }
  };

  const [inputFieldClass, setInputFieldClass] = useState("");

  const isValidTable = (table: string): boolean => {
    return tables.find((item) => item == table) ? true : false;
  };

  const handleTableChange = (value: string | null) => {
    if (value == null) {
      value = "";
    } else {
      dispatch(setEditOrder({ ...order, tableId: value }));
      if (!isValidTable(value)) setInputFieldClass("input-error");
      else setInputFieldClass("");
    }
  };

  return (
    <div className="FoodOrderEdit">
      <div className="table-number-container v-container pd-4-t">
        <Autocomplete
          className={inputFieldClass}
          options={tables}
          id="table-auto-complete"
          autoComplete
          blurOnSelect
          fullWidth
          includeInputInList
          disableCloseOnSelect
          freeSolo
          renderInput={(params) => (
            <TextField
              {...params}
              required
              label="Table no"
              margin="normal"
              fullWidth
              onChange={(e) => handleTableChange(e.target.value)}
            />
          )}
          onChange={(e, value) => handleTableChange(value)}
        />
        {inputFieldClass === "input-error" && (
          <div className="table-error-text"> Please enter valid table no</div>
        )}
      </div>
      <div className="order-notes-container v-container pd-5-t">
        <TextField
          id="order-notes"
          label="Order notes..."
          multiline
          rows={3}
          variant="outlined"
          onChange={(e) => handleOrderNotesChange(e.target.value)}
        />
      </div>

      <div className="order-detail-container v-container pd-3-t ">
        <div className="order-item-container pd-2-b border-1-b">
          {orderItem}
        </div>
        <div className="order-total-cost-order pd-2-b border-1-b">
          <OrderDetailLine
            left="Total"
            right={getCostString(order.cost.totalCost)}
          ></OrderDetailLine>
        </div>
        {order.cost.gst !== 0 && (
          <div className="order-detail-cost-container">
            <OrderDetailLine
              left="Sub Total"
              right={getCostString(order.cost.subTotal)}
            ></OrderDetailLine>
            <OrderDetailLine
              left="GST"
              right={getCostString(order.cost.gst)}
            ></OrderDetailLine>
          </div>
        )}
      </div>
      <div className="send-order-container h-container center ">
        {validateForm() && (
          <button
            className="btn center line full-width"
            onClick={(e) => handleSendOrder()}
          >
            Send order to kitchen
            <Link to="/orderSent"></Link>
          </button>
        )}
        {!validateForm() && (
          <button disabled className="btn disable center line full-width ">
            Send order to kitchen
          </button>
        )}
      </div>
    </div>
  );
};

export default FoodOrderEdit;
function throwError(arg0: Error) {
  throw new Error("Function not implemented.");
}
