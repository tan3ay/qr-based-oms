import React, { useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { getCostString } from "../../helper/common-util";
import { OrderUtil } from "../../helper/OrderUtil";
import { OrderItemVo, OrderVo } from "../../models/orderVo";
import { setViewOrder } from "../../state/action-creators";
import { RootState } from "../../state/store";
import { WebSocketContext } from "../WebSocket/WebSocket";
import "./food-order-view.scss";

interface IOrderDetailLine {
  left: string;
  right: string;
}

interface IOrderDetailLineProp {
  item: OrderItemVo;
}

const OrderDetailLine: React.FC<IOrderDetailLine> = ({ left, right }) => {
  return (
    <div className="order-line-container h-container pd-2-t">
      <div className="order-line-left header left">{left}</div>
      <div className="order-line-right detail right">{right}</div>
    </div>
  );
};

const OrderDetailItemLine: React.FC<IOrderDetailLineProp> = ({ item }) => {
  const dispatch = useDispatch();

  return (
    <div className="order-line-container h-container pd-2-t pd-2-b">
      <div className="order-line-left left">{item.itemQuantity}x</div>
      <div className="order-line-left left">{item.itemName}</div>
      <div className="order-line-right right">
        {item.itemCost * item.itemQuantity + ""}
      </div>
    </div>
  );
};

interface IFoodOrderView {
  order: OrderVo;
}

const FoodOrderView: React.FC<IFoodOrderView> = ({ order }) => {
  const dispatch = useDispatch();
  const ws = useContext(WebSocketContext);
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const history = useHistory();

  const orderItem = order.itemsVo.map((item: OrderItemVo) => {
    return (
      <OrderDetailItemLine key={item.itemId} item={item}></OrderDetailItemLine>
    );
  });

  const handleBackToMenu = () => {
    if (restaurantMeta) {
      dispatch(setViewOrder(OrderUtil.getEmptyOrder()));
      history.replace("restaurant/" + restaurantMeta.restaurantId);
      history.push("/menu");
    } else {
      console.log("Restaurant inforamtion not found");
    }
  };

  const handleOrderHistory = () => {
    if (restaurantMeta) {
      dispatch(setViewOrder(OrderUtil.getEmptyOrder()));
      history.replace("restaurant/" + restaurantMeta.restaurantId);
      history.push("/orderHistory");
    } else {
      console.log("Restaurant inforamtion not found");
    }
  };

  return (
    <div className="FoodOrderView">
      {/* <div className="h-container center title">Order sent</div> */}
      <div className="h-container center sub-title">
        Order successfully sent to the kitchen..!!!
      </div>
      <div className="table-number-container h-container pd-4-t">
        <span className="header">Table No : </span>
        <span className="detail">{order.tableId}</span>
      </div>
      <div className="text-input-container v-container pd-4-t">
        <div className="header">Order Notes </div>
        <div className="detail">{order.orderNotes}</div>
      </div>

      <div className="order-detail-container v-container pd-3-t ">
        <div className="order-item-container pd-2-b border-1-b">
          {orderItem}
        </div>
        <div className="order-total-cost-order pd-2-b border-1-b">
          <OrderDetailLine
            left="Total Cost"
            right={getCostString(order.cost.totalCost)}
          ></OrderDetailLine>
        </div>

        {order.cost.gst !== 0 && (
          <div className="order-detail-cost-container">
            <OrderDetailLine
              left="Sub Total"
              right={getCostString(order.cost.subTotal)}
            ></OrderDetailLine>
            <OrderDetailLine
              left="GST"
              right={getCostString(order.cost.gst)}
            ></OrderDetailLine>
          </div>
        )}
      </div>

      <div className="button-container h-container center">
        <button
          className="btn  center line full-width big-button"
          onClick={(e) => handleOrderHistory()}
        >
          View Order Status<Link to="/menu"></Link>
        </button>
        <button
          className="btn link center line full-width big-button"
          onClick={(e) => handleBackToMenu()}
        >
          Place New Order<Link to="/menu"></Link>
        </button>
      </div>
    </div>
  );
};

export default FoodOrderView;
