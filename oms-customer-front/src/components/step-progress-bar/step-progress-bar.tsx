import { OrderUtil } from "../../helper/OrderUtil";
import { ORDER_STATUS } from "../../models/Order";
import "./step-progress-bar.scss";

interface step {
  label: string;
}

const stepsVo = [
  {
    label: OrderUtil.getOrderStatusText(ORDER_STATUS.PLACED.toString()),
    value: ORDER_STATUS.PLACED,
  },
  {
    label: OrderUtil.getOrderStatusText(ORDER_STATUS.PREPARING.toString()),
    value: ORDER_STATUS.PREPARING,
  },
  {
    label: OrderUtil.getOrderStatusText(ORDER_STATUS.SERVED.toString()),
    value: ORDER_STATUS.SERVED,
  },
  {
    label: OrderUtil.getOrderStatusText(ORDER_STATUS.PAID.toString()),
    value: ORDER_STATUS.PAID,
  },
];

interface ISelectMarkProp {
  selectedStep: number;
  stepWidth: number;
}

export const OrderProgressBar: React.FC<ISelectMarkProp> = ({
  selectedStep,
  stepWidth,
}) => {
  const step1Content = <h1>Step 1 Content</h1>;
  let stepIndex: number = 0;
  let selectedIndex: number = 0;
  const stepProp = stepsVo.map((step) => {
    let left = 0;
    left = (stepIndex / (stepsVo.length - 1)) * 100;

    if (selectedStep === step.value.valueOf()) {
      selectedIndex = stepIndex;
    }
    let leftShift = stepWidth / 2;
    let leftPos = `calc(${left}% - ${leftShift}px)`;
    let stepSelectClass = "";
    if (selectedStep >= step.value.valueOf()) {
      stepSelectClass = "selected";
    }
    stepIndex++;
    return (
      <div
        key={step.value}
        className="step"
        style={{ left: leftPos, width: stepWidth + "px" }}
      >
        <div className={`circle ${stepSelectClass}`}></div>
        <div className="step-label">{step.label}</div>
      </div>
    );
  });

  const finishedStep = (selectedIndex / (stepsVo.length - 1)) * 100;
  return (
    <div className="OrderProgressBar">
      <div className="progress" style={{ width: "100%" }}>
        <div style={{ width: `${finishedStep}%` }} className="progress-bar" />
      </div>
      <br />
      {/* <ProgressBar now = {finishedStep} max={100}/> */}
      <div className="step-container" style={{ height: "36px" }}>
        {stepProp}
      </div>
    </div>
  );
};
