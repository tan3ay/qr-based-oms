/* eslint-disable no-use-before-define */
import React from "react";
import {
  FaMinus,
  FaMinusCircle,
  FaPlus,
  FaPlusCircle,
  FaTrash,
} from "react-icons/fa";
import "./food-quantity-selector.scss";

interface IQuantityProp {
  quantity: number;
  type: string;
  size?: string;
  onIncreament: (value: number) => void;
  onDecreament: (value: number) => void;
}

export const FoodQuantitySelector: React.FC<IQuantityProp> = ({
  type,
  size,
  quantity,
  onIncreament,
  onDecreament,
}) => {
  const disable = type === "filled" && quantity === 0 ? "disable" : "";

  return (
    <div className="FoodQuantitySelector ">
      <div className={`quantity-container ${size}`}>
        <div
          className={`btn-container ${disable}`}
          onClick={() => {
            if (quantity != 0) onDecreament(quantity - 1);
          }}
        >
          {type === "empty" && quantity > 1 && <FaMinus className="empty" />}
          {type === "empty" && quantity == 1 && (
            <div className="trash-container">
              <FaTrash />
            </div>
          )}
          {type === "filled" && (
            <FaMinusCircle className="filled" fontSize="18px" />
          )}
        </div>
        <div className="quantity">{quantity}</div>
        <div
          className="btn-container"
          onClick={() => {
            onIncreament(quantity + 1);
          }}
        >
          {type === "filled" && (
            <FaPlusCircle className="filled" fontSize="18px" />
          )}
          {type === "empty" && <FaPlus />}
        </div>
      </div>
    </div>
  );
};
