/* eslint-disable no-use-before-define */
import React, { useState } from "react";
import { FaWindowClose } from "react-icons/fa";
import MenuUtil from "../../helper/MenuUtil";
import {
  CheckList,
  ICheckList,
  ICheckListItem,
  OPTION_TYPE,
} from "../checked-list/check-list";
import { PageHeader } from "../page-header/page-header";
import "./filter.scss";
interface IsearchProp {
  filters: ICheckListItem[];
  onFilterSelect: (filters: ICheckListItem[]) => void;
  onApplyFilter: () => void;
  onClose: () => void;
}

export const Filter: React.FC<IsearchProp> = ({
  filters,
  onFilterSelect,
  onClose,
  onApplyFilter,
}) => {
  const topMenu = MenuUtil.getMenuItems();

  const filterCheckList: ICheckList = {
    id: "filter-check-list",
    header: "",
    currentSelected: 0,
    maxSelected: 4,
    options: filters,
  };
  const handleFitlterSelect = (
    itemId: string,
    checkId: string,
    optionType: OPTION_TYPE
  ) => {
    filters.forEach((filter) => {
      if (filter.id === checkId) {
        filter.selected = !filter.selected;
      }
    });
    onFilterSelect(filters);
    console.log("filter selected" + itemId, checkId, optionType);
  };

  const handleApplyFilter = () => {
    onApplyFilter();
  };

  const handleClearAll = () => {
    filters.forEach((filter) => {
      filter.selected = false;
    });
    onFilterSelect(filters);
    onApplyFilter();
  };
  return (
    <div className="Filter">
      <div className="filter-container">
        <div className="left-overlay" onClick={onClose}></div>
        <div className="filter-item-container">
          <PageHeader
            header={"Filter"}
            right={
              <button
                className="btn link clear-all"
                onClick={(e) => handleClearAll()}
              >
                Clear all
              </button>
            }
          />
          <CheckList
            data={filterCheckList}
            onChange={handleFitlterSelect}
          ></CheckList>
          <div className="button-container">
            <button
              className="btn big-button full-width clear-all"
              onClick={(e) => handleApplyFilter()}
            >
              Apply
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
