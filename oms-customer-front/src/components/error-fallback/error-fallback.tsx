import "./error-fallback.scss";

export enum ERROR_TYPE {
  NETWORK_ERROR = "NETWORK_ERROR",
  UNKOWN_ERROR = "UNKOWN_ERROR",
  NONE = "NONE",
}

interface IErrorFallbackProp {
  error: ERROR_TYPE;
  onRefresh?: (error: ERROR_TYPE) => void;
}

export const ErrorFallback: React.FC<IErrorFallbackProp> = ({
  error,
  onRefresh,
}) => {
  return (
    <div className="ErrorFallback" role="alert">
      <div className="warning-text-container">
        <p className="header">Oops! something went wrong...</p>
        <p className="detail">
          Please check your internet connection and try again
        </p>
      </div>
      <pre></pre>
      <div className="warning-button-container">
        {onRefresh && (
          <button
            className="btn link"
            onClick={(e) => {
              onRefresh(error);
            }}
          >
            Refresh page
          </button>
        )}
      </div>
    </div>
  );
};
