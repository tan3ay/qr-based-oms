import { convertToNumber, getCostString } from "../../helper/common-util";
import { SelectMark } from "../selectMark/select-mark";
import "./check-list.scss";

export enum OPTION_TYPE {
  CHECKBOX = "check",
  RADIO = "radio",
}
export interface ICheckListItem {
  id: string;
  label: string;
  selected: boolean;
  cost?: number;
}

export interface ICheckListItemProp {
  option: ICheckListItem;
  disabled: boolean;
  optionType: OPTION_TYPE;
  onChange: (checkId: string) => void;
}

const CheckListItem: React.FC<ICheckListItemProp> = ({
  option,
  optionType,
  disabled,
  onChange,
}) => {
  const handleChange = () => {
    if (!disabled) onChange(option.id);
  };
  const hasCost = convertToNumber(option.cost) !== 0;
  return (
    <div className="CheckListItem">
      <div className=" item-container" onClick={(e) => handleChange()}>
        <div className="input-container">
          {
            <SelectMark
              type={optionType.valueOf()}
              disable={disabled}
              border={true}
              selected={option.selected}
            />
          }
        </div>

        <div className="label-container">
          <div className="label">{option.label}</div>
        </div>
        {hasCost && (
          <div className="cost-container">
            <div className="cost">
              {getCostString(convertToNumber(option.cost))}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};

export interface ICheckList {
  id: string;
  header: string;
  currentSelected: number;
  maxSelected: number;
  options: ICheckListItem[];
}

export interface ICheckListProp {
  data: ICheckList;
  onChange?: (itemId: string, checkId: string, optionType: OPTION_TYPE) => void;
}

export const CheckList: React.FC<ICheckListProp> = ({ data, onChange }) => {
  const optionType: OPTION_TYPE =
    data.maxSelected !== 1 ? OPTION_TYPE.CHECKBOX : OPTION_TYPE.RADIO;

  const handleChange = (optionId: string) => {
    if (onChange) onChange(data.id, optionId, optionType);
  };

  const listChecksDom = data.options.map((option) => {
    if (
      data.currentSelected < data.maxSelected ||
      option.selected ||
      optionType === "radio"
    ) {
      return (
        <CheckListItem
          key={option.id}
          option={option}
          optionType={optionType}
          disabled={false}
          onChange={handleChange}
        />
      );
    } else {
      return (
        <CheckListItem
          key={option.id}
          option={option}
          optionType={optionType}
          disabled={true}
          onChange={handleChange}
        />
      );
    }
  });

  return (
    <div className="CheckList">
      {data.header !== "" && (
        <div className="header-container">
          <div className="header">{data.header}</div>
        </div>
      )}
      <div className="sub-header-container">
        {optionType === OPTION_TYPE.CHECKBOX && (
          <div className="subheader">Select up to {data.maxSelected}</div>
        )}
        {optionType === OPTION_TYPE.RADIO && (
          <div className="subheader">Select {data.maxSelected}</div>
        )}
      </div>
      <div className="list-container">{listChecksDom}</div>
    </div>
  );
};
