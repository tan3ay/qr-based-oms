import "./page-header.scss";

interface IPageHeaderProp {
  header: string;
  headerAlign?: string;
  left?: JSX.Element;
  right?: JSX.Element;
}

export const PageHeader: React.FC<IPageHeaderProp> = ({
  header,
  headerAlign,
  left,
  right,
}) => {
  headerAlign = headerAlign ? headerAlign : "";

  return (
    <div className="PageHeader">
      {left && <div className="left">{left}</div>}
      <div className={`header ${headerAlign}`}>{header}</div>
      {right && <div className="right">{right}</div>}
    </div>
  );
};
