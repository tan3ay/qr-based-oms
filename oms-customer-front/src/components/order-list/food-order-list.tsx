import _ from "lodash";
import React, { useEffect, useRef } from "react";
import { BrowserCacheUtil } from "../../helper/BrowserCacheUtil";
import { OrderDto } from "../../models/Order";
import { EmptyData } from "../empty-data/empty-data";
import FoodOrderListItem from "./food-order-list-item";
import "./food-order-list.scss";
import { HistoryOrderListItem } from "./order-history-item/history-order-list-item";

export enum LIST_TYPE {
  HISTORY = "HISTORY",
}

interface IListProp {
  listType: LIST_TYPE;
  orders: OrderDto[];
  scrollable?: boolean;
  selectMode?: boolean;
  selectedItems?: string[];
  onUpdate?: (updateStatus: string) => void;
  onSelect?: (selectedKeys: string[]) => void;
}

const FoodOrderList: React.FC<IListProp> = ({
  selectMode,
  selectedItems,
  orders,
  listType,
  scrollable,
  onSelect,
}) => {
  const orderItemsDom = orders.map((order: OrderDto) => {
    return (
      <FoodOrderListItem key={order.orderId + "history"}>
        <HistoryOrderListItem
          key={order.orderId + "history"}
          orderDto={order}
        ></HistoryOrderListItem>
      </FoodOrderListItem>
    );
  });

  const listRef = useRef<HTMLDivElement>(null);

  const scrollPointerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (scrollable) {
      if (listRef !== null && listRef.current != null) {
        const offsetTop = BrowserCacheUtil.get("list_scroll_key");
        listRef.current.scrollTo({
          top: parseFloat(offsetTop),
        });
      }
    }
  }, []);

  const handleScroll = _.throttle(() => {
    let scrollOffset = 0;
    if (scrollPointerRef.current) {
      scrollOffset =
        scrollPointerRef.current?.offsetTop +
        Math.abs(scrollPointerRef.current?.getBoundingClientRect().top);
      BrowserCacheUtil.put(scrollOffset + "", "list_scroll_key", 3);
    }
  }, 600);

  return (
    <div
      ref={listRef}
      onScroll={handleScroll}
      className={`FoodOrderList ${scrollable ? "scrollable" : ""}`}
    >
      <div ref={scrollPointerRef}></div>
      {orderItemsDom}
      {orderItemsDom.length === 0 && (
        <EmptyData header="No more orders found." />
      )}
    </div>
  );
};

export default FoodOrderList;
