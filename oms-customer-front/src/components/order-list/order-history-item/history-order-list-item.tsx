import { FaPencilAlt, FaPrint } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { useEffect, useState } from "react";
import { getCostString } from "../../../helper/common-util";
import { OrderUtil } from "../../../helper/OrderUtil";
import {
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../../../models/Order";
import { RootState } from "../../../state/store";
import { ChipItem } from "../../ChipItem/chip-item";

import { OrderMenuItem } from "../../order-menu-item/order-menu-item";
import { OrderProgressBar } from "../../step-progress-bar/step-progress-bar";
import { OrderDetailLine } from "./history-order-detail-line";
import "./history-order-list-item.scss";

interface IOrderDetailLineProp {
  item: OrderItemDto;
  parent: OrderDto;
}

interface IProp {
  orderDto: OrderDto;
  selectMode?: boolean;
}

const OrderItemViewLine: React.FC<IOrderDetailLineProp> = ({
  item,
  parent,
}) => {
  const dispatch = useDispatch();
  const itemDetail = item.isCustomizable ? item.itemDetail : "";
  return (
    <div className="order-line-container">
      <div className="order-item-contents">
        {item.itemStatus !== ORDER_ITEM_STATUS.DELETED && (
          <div className="order-line h-container">
            <div className="order-line-left left header">
              <OrderMenuItem
                header={`${item.itemQuantity} x ${item.itemName}`}
                detail={itemDetail}
              />
            </div>
            {/* <OrderMenuItem header={`${item.itemQuantity} x ${item.itemName}`} detail={itemDetail}/> */}
            <ChipItem
              label={OrderUtil.getOrderItemStatusString(item.itemStatus)}
              color={OrderUtil.getOrderItemColorString(item.itemStatus)}
            />
            <div className="order-line-left right detail">
              {item.itemCost * item.itemQuantity}
            </div>
          </div>
        )}
        {item.itemStatus === ORDER_ITEM_STATUS.DELETED && (
          <div className="order-line h-container">
            <div className="order-line-left left header text-strike">
              {item.itemQuantity}x{item.itemName}
            </div>
            <ChipItem label="Deleted" color="gray" />
            <div className="order-line-left right detail text-strike">
              {item.itemCost * item.itemQuantity}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
export const HistoryOrderListItem: React.FC<IProp> = ({
  selectMode,
  orderDto,
}) => {
  const dispatch = useDispatch();
  const [orderStatus, setOrderStatus] = useState(ORDER_STATUS.PLACED);
  const history = useHistory();

  useEffect(() => {
    setOrderStatus(OrderUtil.getCombineOrderStatus(orderDto));
    console.log("useEffect setStatus");
  });

  const orderItem = orderDto.items.map((item: OrderItemDto) => {
    return (
      <OrderItemViewLine
        key={item.itemId}
        item={item}
        parent={orderDto}
      ></OrderItemViewLine>
    );
  });
  const stepWidth = 50;
  let containerPadding = 8 + stepWidth / 2;
  let paddingStyle = `24px ${containerPadding}px`;
  return (
    <div className="HistoryOrderListItem">
      <div
        className={` list-item-container ${selectMode ? "select-mode" : ""}`}
      >
        <div className="v-container">
          <div className="header-container">
            <div className="order-status-container">
              <div className="order-status-top"></div>
              <div className="order-status">
                {OrderUtil.getOrderStatusText(orderStatus.toString())}
              </div>
            </div>

            <div
              className="progress-step-container"
              style={{ padding: paddingStyle }}
            >
              <OrderProgressBar
                selectedStep={orderStatus.valueOf()}
                stepWidth={stepWidth}
              />
            </div>
          </div>
          <div className="header-container">
            <div className="order-id-container">
              <span className="header">Order Id:</span>
              <span className="detail">{orderDto.orderId}</span>
            </div>
            <div className="table">Table No:{orderDto.tableId}</div>
          </div>

          <div className="order-edit-container">
            <div className="order-detail-container v-container pd-3-t ">
              <div className="order-total-cost-order pd-2-b border-1-b">
                <OrderDetailLine
                  left="Total Cost"
                  right={getCostString(orderDto.cost.totalCost)}
                ></OrderDetailLine>
              </div>
              <div className="menu-item-container">{orderItem}</div>

              {orderDto.cost.gst !== 0 && (
                <div className="order-detail-cost-container">
                  <OrderDetailLine
                    left="Sub Total"
                    right={getCostString(orderDto.cost.subTotal)}
                  ></OrderDetailLine>
                  <OrderDetailLine
                    left="GST"
                    right={getCostString(orderDto.cost.gst)}
                  ></OrderDetailLine>
                </div>
              )}
            </div>

            <div className="button-container h-container "></div>
          </div>
        </div>
      </div>
    </div>
  );
};
