interface IOrderDetailLine {
  left: string;
  right: string;
}

export const OrderDetailLine: React.FC<IOrderDetailLine> = ({
  left,
  right,
}) => {
  return (
    <div className="order-line-container h-container ">
      <div className="order-line-left left header">{left}</div>
      <div className="order-line-right right detail">{right}</div>
    </div>
  );
};
