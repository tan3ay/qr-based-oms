import _, { throttle } from "lodash";
import React, { RefObject, useEffect, useRef, useState } from "react";
import { BrowserCacheUtil } from "../../helper/BrowserCacheUtil";
import { MenuLists } from "../../models/menu";
import { ChipBar, IChipProp, SELECT_TYPE } from "../chip-bar/chip-bar";
import FoodMenuList from "./food-menu-list";
import "./food-menu-list-container.scss";

interface IListContainerProp {
  menuLists: MenuLists[];
  refs?: Map<string, RefObject<unknown>>;
}

const FoodMenuListContainer: React.FC<IListContainerProp> = ({ menuLists }) => {
  const getCategory = () => {
    return menuLists.map((category) => {
      return {
        label: category.listName,
        value: category.listName,
        selected: false,
      };
    });
  };

  const [categoryListVo, setCategoryListVo] = useState<IChipProp[]>([]);

  const initCategory = () => {
    console.log("setting category", getCategory());
    setCategoryListVo([...getCategory()]);
  };

  let catToRef = new Map<string, RefObject<HTMLDivElement>>();

  useEffect(() => {
    // if(categoryListVo.length===0){
    initCategory();
    // }
  }, [menuLists]);

  menuLists.forEach((category) => {
    const scrollRef = React.createRef<HTMLDivElement>();
    catToRef.set(category.listName, scrollRef);
  });

  const parentRef = useRef<HTMLDivElement>(null);
  const handleCategorySelect = (items: IChipProp[]) => {
    const jumpToItem = items.find((item) => item.selected);
    console.log(items);
    if (jumpToItem) {
      setCategoryListVo([...items]);
      if (catToRef.get(jumpToItem.value)) {
        let parentTop = parentRef.current?.offsetTop;
        parentTop = parentTop ? parentTop : 0;
        const eleRef = catToRef.get(jumpToItem.value);
        let eleTop = eleRef?.current?.offsetTop;
        eleTop = eleTop ? eleTop : 0;
        if (eleRef != null && parentTop != null) {
          parentRef.current?.scrollTo({
            top: eleTop - parentTop,
          });
        }
      }
    }
  };

  const scrollPointerRef = useRef<HTMLDivElement>(null);
  const handleListScroll = (e: React.UIEvent<HTMLDivElement, UIEvent>) => {
    let min: number = Number.MAX_SAFE_INTEGER;
    let targetEle: string = "";
    const oldCategoryList = _.cloneDeep(categoryListVo);
    menuLists.forEach((list) => {
      let parentTop = parentRef?.current?.getBoundingClientRect().y;
      parentTop = parentTop ? parentTop : 0;
      const eleRef = catToRef.get(list.listName);
      let eleTop = eleRef?.current?.getBoundingClientRect().y;
      eleTop = eleTop ? eleTop : 0;
      let currentEleTop = Math.abs(eleTop - parentTop);
      // console.log(eleTop,parentTop,currentEleTop,list.listName)
      if (currentEleTop < min) {
        min = currentEleTop;
        targetEle = list.listName;
      }
    });

    categoryListVo.forEach((category) => {
      category.selected = category.label === targetEle ? true : false;
    });
    if (!_.isEqual(oldCategoryList, categoryListVo)) {
      console.log("set" + targetEle);
      setCategoryListVo([...categoryListVo]);
    }

    let scrollOffset = 0;
    if (scrollPointerRef.current) {
      scrollOffset =
        scrollPointerRef.current?.offsetTop +
        Math.abs(scrollPointerRef.current?.getBoundingClientRect().top);
      BrowserCacheUtil.put(scrollOffset + "", "list_scroll_key", 3);
    }
  };

  const handleListScrollT = throttle(handleListScroll, 400);

  useEffect(() => {
    if (parentRef !== null && parentRef.current != null) {
      const offsetTop = BrowserCacheUtil.get("list_scroll_key");
      parentRef.current.scrollTo({
        top: parseFloat(offsetTop),
      });
    }
  }, []);

  const menuItemsDom = menuLists.map((item: MenuLists) => {
    return (
      <div key={item.listName}>
        <div
          ref={catToRef.get(item.listName)}
          className={`list-header ${item.listName}`}
        >
          {item.listName}
        </div>
        <FoodMenuList
          key={item.listName}
          menuItems={item.menuList}
        ></FoodMenuList>
      </div>
    );
  });

  return (
    <div className="FoodMenuListContainer">
      <div className="menu-category-filter flex-content-fixed">
        <ChipBar
          chipData={categoryListVo}
          scollToSelect={true}
          selectType={SELECT_TYPE.SINGLE}
          onSelectChange={(items) => handleCategorySelect(items)}
        ></ChipBar>
      </div>
      <div
        ref={parentRef}
        onScroll={(e) => handleListScrollT(e)}
        className="list-content flex-content-scroll"
      >
        <span></span>
        <div ref={scrollPointerRef}></div>
        {menuItemsDom}
      </div>
    </div>
  );
};

export default FoodMenuListContainer;
