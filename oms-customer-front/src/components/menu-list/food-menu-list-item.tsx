import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { getCostString } from "../../helper/common-util";
import { OrderUtil } from "../../helper/OrderUtil";
import { MenuDto } from "../../models/menu";
import { setOrderItem } from "../../state/action-creators";
import { RootState } from "../../state/store";
import { FoodQuantitySelector } from "../quantity-selector/food-quantity-selector";
import "./food-menu-list-item.scss";

interface IProp {
  menuItem: MenuDto;
}

const FoodMenuListItem: React.FC<IProp> = ({ menuItem }) => {
  const history = useHistory();
  const dispatch = useDispatch();
  let orderState = useSelector((state: RootState) => state.order);
  let order = useSelector((state: RootState) => state.order.editOrder);
  const [quantity, setQuantity] = useState(
    OrderUtil.getQuantity(menuItem, order)
  );

  useEffect(() => {
    setQuantity(OrderUtil.getQuantity(menuItem, order));
  }, [order, menuItem]);

  const handleDecreament = (quantity: number) => {
    const item = OrderUtil.findOrderItem(menuItem, order);

    if (item) {
      dispatch(
        setOrderItem({
          ...OrderUtil.getOrderItemVo(menuItem),
          itemQuantity: quantity,
        })
      );
    }
  };

  const handleIncreament = (quantity: number) => {
    dispatch(
      setOrderItem({
        ...OrderUtil.getOrderItemVo(menuItem),
        itemQuantity: quantity,
      })
    );
  };

  const handleAddClick = () => {
    history.push("/addMenu/" + menuItem.menuId);
  };

  const imagePresent = menuItem.imgUrl !== "";

  return (
    <div className="FoodMenuListItem">
      <div className="list-item-container">
        {/* { menuItem.isCustomizable===1 && quantity>0 && */}
        {quantity > 0 && (
          <div className="quantity-tag-container">
            <div className="quantity-triangle"></div>
            <div className="quantity-tag">{quantity}</div>
          </div>
        )}
        <div className="image-container">
          {imagePresent && (
            <img className="image" alt="" src={menuItem.imgUrl}></img>
          )}
          {!imagePresent && quantity > 0 && <div className="empty-image"></div>}
        </div>
        <div className="v-container">
          <div className="header-container">
            <div className="header">{menuItem.name}</div>
            <div className="cost">{getCostString(menuItem.cost)}</div>
          </div>
          <div className="">
            <div className="detail">{menuItem.desc}</div>
          </div>
          {menuItem.isCustomizable === 0 && (
            <div className="quantity-container">
              <FoodQuantitySelector
                type="filled"
                size="m"
                quantity={quantity}
                onIncreament={handleIncreament}
                onDecreament={handleDecreament}
              />
            </div>
          )}

          {menuItem.isCustomizable === 1 && (
            <div className="button-container">
              <button className="btn normal" onClick={(e) => handleAddClick()}>
                Add<Link to="/addMenu"></Link>
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

/*
    


*/

/*
    <div className="container FoodMenuListItem">
      <div className="row">
        <img className="col-md-3 img-thumbnail" src={menuItem.imageUrl} style={{width:"150px", height:"150px"}}></img>
        <div className="col-md-9">
          <div className="row">
            <h2 className="col-md-10">{menuItem.name}</h2>
            <h2 className="col-md-2">${menuItem.cost}</h2>
          </div>
          <div className="row">
            <p className="">{menuItem.desc}</p>
          </div>
          <div className="">
            <button className="rounded-btn" onClick={()=>decreaseQuantity()}>-</button>   
            <span className=" ">{quantity}</span>         
            <button className="rounded-btn" onClick={()=>increaseQuantity()} >+</button>
          </div>
          <div className="row ">
            <Button className="col-md-6 btn " >Add To Basket</Button>
          </div>
          </div>
        </div>
        
    </div>

*/
export default FoodMenuListItem;
