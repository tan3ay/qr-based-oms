import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RouteComponentProps, useParams } from "react-router-dom";
import { RestaurantService } from "../api/restaurant-service";
import { Loader } from "../components/Loader/loader";
import { BrowserCacheUtil } from "../helper/BrowserCacheUtil";
import { setRestaurant } from "../state/action-creators";
import { RootState } from "../state/store";
import "./restaurant-home.scss";

interface IProps extends RouteComponentProps {}

interface RouteParams {
  restaurantId: string;
}

const RestaurantHome: React.FC<IProps> = ({ history }) => {
  let { restaurantId } = useParams<RouteParams>();
  const dispatch = useDispatch();

  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const loadRestaurantData = async () => {
    const res = await RestaurantService.getRestaurantData(restaurantId);
    dispatch(setRestaurant(res.data));
    BrowserCacheUtil.putRestaurant(JSON.stringify(res.data));
  };

  useEffect(() => {
    if (restaurantMeta === null) {
      loadRestaurantData();
    }
  }, []);

  return (
    <div>
      {restaurantMeta !== null && (
        <div className="RestaurantHome">
          <div className="container">
            <div className="restaurant-name-container">
              {restaurantMeta.name}
            </div>
            <div className="button-container">
              <button
                className="btn full-width big-button"
                onClick={(e) => history.push("/menu")}
              >
                Order Now
              </button>
              <button
                className="btn link full-width big-button"
                onClick={(e) => history.push("/orderHistory")}
              >
                My Previous Orders
              </button>
            </div>
          </div>
        </div>
      )}
      {restaurantMeta === null && <Loader />}
    </div>
  );
};

export default RestaurantHome;
