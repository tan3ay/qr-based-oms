import React, { useContext, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Loader } from "../components/Loader/loader";
import FoodOrderView from "../components/order/food-order-view";
import { PageHeader } from "../components/page-header/page-header";
import { WebSocketContext } from "../components/WebSocket/WebSocket";
import { BrowserCacheUtil, CACH_KEYS } from "../helper/BrowserCacheUtil";
import { isEmpty } from "../helper/common-util";
import { setEditOrder, setViewOrder } from "../state/action-creators";
import { RootState } from "../state/store";
import "./food-order-finish-page.scss";

interface IProps extends RouteComponentProps {}

const FoodOrderFinishPage: React.FC<IProps> = ({ history, match }) => {
  // const history = useHistory();
  const dispatch = useDispatch();
  const ws = useContext(WebSocketContext);
  const order = useSelector((state: RootState) => state.order.viewOrder);

  // useEffect(()=>{
  //   const order = BrowserCacheUtil.get(CACH_KEYS.VIEW_ORDER)//.getOrder();
  //   if(order!=null)
  //     dispatch(setViewOrder(order));
  // },[]);

  const handleBankClick = () => {
    history.goBack();
  };

  return (
    <div className="FoodOrderFinishPage flex-v">
      <div className="page-header-container flex-content-fixed">
        <PageHeader header="Order sent"></PageHeader>
      </div>
      <div className="title-header"></div>
      <div className="order-container flex-content-scroll">
        {!isEmpty(order) && <FoodOrderView order={order} />}
        {isEmpty(order) && <Loader loadingType="simple" />}
      </div>
    </div>
  );
};

export default FoodOrderFinishPage;
