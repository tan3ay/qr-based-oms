import React, { useEffect, useState } from "react";
import { FaArrowLeft, FaRedoAlt } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { RouteComponentProps } from "react-router-dom";
import { OrderService } from "../api/order-service";
import { Loader } from "../components/Loader/loader";
import FoodOrderList, {
  LIST_TYPE,
} from "../components/order-list/food-order-list";
import { PageHeader } from "../components/page-header/page-header";
import { BrowserCacheUtil, CACH_KEYS } from "../helper/BrowserCacheUtil";
import { OrderDto } from "../models/Order";
import { setDataLoading, setHistoryOrders } from "../state/action-creators";
import { RootState } from "../state/store";
import "./food-order-history-page.scss";

interface IFoodMenuPageProp extends RouteComponentProps {}

const FoodOrderHistoryPage: React.FC<IFoodMenuPageProp> = ({ history }) => {
  const dispatch = useDispatch();
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const orders: OrderDto[] = useSelector(
    (state: RootState) => state.historyOrders.orders
  );
  const ordersUpdateTime: number = useSelector(
    (state: RootState) => state.historyOrders.updateTime
  );
  const loadingState = useSelector(
    (state: RootState) => state.pageLoadingState.allPage
  );
  const [loading, setLoading] = useState(loadingState);

  useEffect(() => {
    setLoading(loadingState);
  }, [loadingState]);

  const updateHistoryOrderData = async () => {
    if (restaurantMeta) {
      dispatch(setDataLoading(true));
      const latestOrders: OrderDto[] = [];
      const localSessionHistoryOrders: OrderDto[] = BrowserCacheUtil.get(
        CACH_KEYS.HISTORY_ORDERS
      )
        ? BrowserCacheUtil.get(CACH_KEYS.HISTORY_ORDERS)
        : [];
      await Promise.all(
        localSessionHistoryOrders.map(async (order) => {
          const res = await OrderService.getOrder(
            restaurantMeta.restaurantId,
            order.orderId
          );
          latestOrders.push(res.data);
        })
      );
      //console.log(latestOrders);
      latestOrders.sort(
        (o1, o2) => parseInt(o1.orderId) - parseInt(o2.orderId)
      );
      dispatch(setHistoryOrders(latestOrders));
      // Set in browser cache
      BrowserCacheUtil.put(
        JSON.stringify(latestOrders),
        CACH_KEYS.HISTORY_ORDERS,
        3 * 60
      );

      dispatch(setDataLoading(false));
    }
  };

  // sync browser cache
  window.addEventListener(
    "storage",
    function (e: StorageEvent) {
      if (e.key === CACH_KEYS.HISTORY_ORDERS) {
        const latestOrders = BrowserCacheUtil.get(CACH_KEYS.HISTORY_ORDERS)
          ? BrowserCacheUtil.get(CACH_KEYS.HISTORY_ORDERS)
          : [];
        dispatch(setHistoryOrders(latestOrders));
      }
    }.bind(this)
  );

  const goBackToDashBoard = () => {
    history.push("/restaurant/" + restaurantMeta?.restaurantId);
  };

  const lastUpdateTime = Math.round(
    (Date.now().valueOf() - ordersUpdateTime) / 60000
  );

  return (
    <div className="FoodOrderHistoryPage flex-v">
      <div className="page-header-container">
        <PageHeader
          left={
            <button
              className="back-button btn icon"
              onClick={(e) => {
                goBackToDashBoard();
              }}
            >
              <FaArrowLeft />
            </button>
          }
          header={"Order History"}
          // right={
          //   <button className="back-button btn icon" onClick={
          //     (e)=>{updateHistoryOrderData()}
          //   }><FaRedoAlt/>
          //    </button>
          // }
        ></PageHeader>
      </div>

      <div className="content flex-content-fixed">
        <div className="food-order-list-action-container">
          <div className="left">
            <span> Updated {lastUpdateTime} min ago. </span>
          </div>
          <div className="right">
            <button
              className="btn icon"
              onClick={(e) => {
                updateHistoryOrderData();
              }}
            >
              <FaRedoAlt />
            </button>
          </div>
        </div>
        <div className="food-order-list-container icon">
          {loading && <Loader loadingType="simple" position="top" />}
          <FoodOrderList
            orders={orders}
            scrollable={true}
            listType={LIST_TYPE.HISTORY}
          />
        </div>
      </div>
    </div>
  );
};

export default FoodOrderHistoryPage;
