import { Badge, Button } from "@mui/material";
import React, { useState } from "react";
import { FaArrowLeft, FaShoppingCart, FaSlidersH } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { Link, RouteComponentProps } from "react-router-dom";
import { MenuService } from "../api/menu-service";
import { ICheckListItem } from "../components/checked-list/check-list";
import { Filter } from "../components/filter/filter";
import { Loader } from "../components/Loader/loader";
import FoodMenuListContainer from "../components/menu-list/food-menu-list-container";
import { PageHeader } from "../components/page-header/page-header";
import { SearchBar } from "../components/search/search-bar";
import { BrowserCacheUtil } from "../helper/BrowserCacheUtil";
import { MenuSearchService } from "../helper/MenuSearchService";
import MenuUtil from "../helper/MenuUtil";
import { Filters, MenuDto } from "../models/menu";
import { setMenu, setRestaurant } from "../state/action-creators";
import { RootState } from "../state/store";
import "./food-menu-page.scss";

interface Props {
  restaurantId: string;
  filters: Filters;
  menu: MenuDto;
}

export interface SearchVo {
  key: string;
  filters: ICheckListItem[];
}

interface IFoodMenuPageProp extends RouteComponentProps {}

const FoodMenuPage: React.FC<IFoodMenuPageProp> = ({ history }) => {
  const dispatch = useDispatch();
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const menuDtos: MenuDto[] = useSelector(
    (state: RootState) => state.menu.menuItems
  );
  // const menuListsVo = MenuUtil.getMenuListEntities(menuDtos);
  const [menuListsVo, setMenuListsVo] = useState(
    MenuUtil.getMenuListEntities(menuDtos)
  );
  const [loading, setLoading] = useState(menuDtos.length === 0);

  const order = useSelector((state: RootState) => state.order.editOrder);

  const filtersVo = [
    { label: "Chef special", id: "isChefSpecial", selected: false },
    { label: "Vegan", id: "isVegan", selected: false },
    { label: "Vegetarian", id: "isVeg", selected: false },
    { label: "Drinks", id: "isDrink", selected: false },
  ];
  const [searchVo, setSearchVo] = useState({ key: "", filters: filtersVo });

  const initializeMenu = async (restaurantId: string) => {
    const res = await MenuService.getMenus(restaurantId);
    const menus: MenuDto[] = res.data;
    setLoading(false);
    dispatch(setMenu(menus));
    setMenuListsVo([...MenuUtil.getMenuListEntities(menus)]);
  };

  React.useEffect(() => {
    if (menuDtos.length == 0) {
      if (restaurantMeta !== null) {
        initializeMenu(restaurantMeta.restaurantId);
      } else {
        let restMeta = BrowserCacheUtil.getRestaurant();
        if (restMeta != null) {
          setRestaurant(restMeta);
          initializeMenu(restMeta.restaurantId);
        }
      }
    }
  }, []);

  const handleSearch = (value: string) => {
    searchVo.key = value;
    setSearchVo({ ...searchVo, key: value });

    updateSearchResult(searchVo);
  };

  const handleFilterSelect = (items: ICheckListItem[]) => {
    setSearchVo({ ...searchVo, filters: items });
  };
  const handleApplyFilter = () => {
    updateSearchResult(searchVo);
  };

  const updateSearchResult = (searchVo: SearchVo) => {
    if (menuDtos.length > 0) {
      let filteredList = MenuSearchService.fiterBySearchVo(menuDtos, searchVo);
      console.log("setting filtered menu list");
      setMenuListsVo([...filteredList]);
    }
  };

  const handleCartClick = () => {
    if (order.itemsVo.length > 0) history.push("/order");
    else {
    }
  };

  const [filter, setFilter] = useState(false);
  const handleFilterClose = () => {
    console.log("Filter closed");
    setFilter(false);
  };
  const handleFilterOpen = () => {
    console.log("Filter open");
    setFilter(true);
  };
  const goBackToDashBoard = () => {
    history.push("/restaurant/" + restaurantMeta?.restaurantId);
  };

  return (
    <div className="FoodMenuPage webkit-height flex-v">
      {filter && (
        <Filter
          filters={searchVo.filters}
          onFilterSelect={handleFilterSelect}
          onApplyFilter={handleApplyFilter}
          onClose={handleFilterClose}
        />
      )}
      <div className="fixed-header flex-content-fixed">
        <div className="page-header-container">
          <PageHeader
            left={
              <button
                className="back-button btn icon"
                onClick={(e) => {
                  goBackToDashBoard();
                }}
              >
                <FaArrowLeft />
                <Link to="/Menu"></Link>
              </button>
            }
            header={restaurantMeta ? restaurantMeta.name : ""}
            right={
              <Button
                className=".btn cart-button"
                onClick={(e) => {
                  handleCartClick();
                }}
              >
                <Badge badgeContent={order.itemsVo.length} color="primary">
                  <FaShoppingCart />
                  {/* <FaShoppingBasket/>   */}
                </Badge>
                <Link to="/Order"></Link>
              </Button>
            }
          ></PageHeader>
        </div>

        <div className="search-container">
          <SearchBar onChange={handleSearch}></SearchBar>

          <Button
            className=".btn cart-button"
            onClick={(e) => {
              handleFilterOpen();
            }}
          >
            <Badge
              badgeContent={
                searchVo.filters.filter((filter) => filter.selected).length
              }
              color="primary"
            >
              <FaSlidersH />
              {/* <FaShoppingBasket/>   */}
            </Badge>
            <Link to="/Order"></Link>
          </Button>
        </div>
      </div>
      <div className="content flex-content-fixed">
        {!loading && (
          <div className="menu-list-container">
            <FoodMenuListContainer menuLists={menuListsVo} />
          </div>
        )}
        {loading && <Loader loadingType="simple" position="top" />}
      </div>
    </div>
  );
};

export default FoodMenuPage;
