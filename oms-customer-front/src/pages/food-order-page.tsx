import React, { useContext, useEffect, useState } from "react";
import { FaArrowLeft, FaShoppingBasket } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { RouteComponentProps } from "react-router";
import { Link } from "react-router-dom";
import {
  ErrorFallback,
  ERROR_TYPE,
} from "../components/error-fallback/error-fallback";
import { Loader } from "../components/Loader/loader";
import FoodOrderEdit from "../components/order/food-order-edit";
import { PageHeader } from "../components/page-header/page-header";
import { WebSocketContext } from "../components/WebSocket/WebSocket";
import { isEmpty } from "../helper/common-util";
import { RootState } from "../state/store";
import "./food-order-page.scss";

interface IProps extends RouteComponentProps {}

const FoodOrderPage: React.FC<IProps> = ({ history, match }) => {
  const dispatch = useDispatch();

  const loadingState = useSelector(
    (state: RootState) => state.pageLoadingState.allPage
  );
  const [loading, setLoading] = useState(loadingState);
  useEffect(() => {
    setLoading(loadingState);
  }, [loadingState]);

  const ws = useContext(WebSocketContext);
  let order = useSelector((state: RootState) => state.order.editOrder);

  const errorState = useSelector((state: RootState) => state.pageError.allPage);
  const pageError = useSelector((state: RootState) => state.pageError.error);

  const goBackToMenu = () => {
    history.push("/menu");
  };
  const isEmptyOrder = order.itemsVo.length === 0;

  const handleRefreshOnError = (error: ERROR_TYPE) => {
    history.go(0);
  };

  return (
    <div className="FoodOrderPage flex-v">
      <div className="page-header-container flex-content-fixed">
        <PageHeader
          header="Basket"
          headerAlign="center"
          left={
            <button
              className="back-button btn icon"
              onClick={(e) => {
                goBackToMenu();
              }}
            >
              <FaArrowLeft />
              <Link to="/Menu"></Link>
            </button>
          }
        ></PageHeader>
      </div>

      <div className="order-container flex-content-scroll">
        {loading && <Loader loadingType="simple" position="top" />}

        {!isEmpty(order) && !isEmptyOrder && <FoodOrderEdit order={order} />}

        {errorState && (
          <ErrorFallback error={pageError} onRefresh={handleRefreshOnError} />
        )}

        {!isEmpty(order) && isEmptyOrder && (
          <div className="empty-container">
            <div className="empty-container-icon">
              <FaShoppingBasket />
            </div>
            <div className="empty-container-header">It's empty here.</div>
            <div className="empty-container-detail">
              Your basket is empty check out our menu to have awsome meal.
            </div>
            <div className="empty-container-button">
              <button className="btn center  full-width" onClick={goBackToMenu}>
                Go back to menu
                <Link to="/Menu"></Link>
              </button>
            </div>
          </div>
        )}
        {isEmpty(order) && <Loader loadingType="simple" />}
      </div>
    </div>
  );
};

export default FoodOrderPage;
