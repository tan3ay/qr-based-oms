set -e
BUCKET_NAME=$1
#oms-client-front-1
DISTRIBUTION_ID=$2
PROFILE=ci_cd_user
echo "-----------"
echo "--Install--"
npm install

echo "-----------"
echo "--Build--"
npm run build

echo "-----------"
echo "--Deploy--"
aws s3 sync build/ s3://$BUCKET_NAME --profile $PROFILE
aws cloudfront create-invalidation --distribution-id $DISTRIBUTION_ID --path "/*" --profile $PROFILE

echo "-- Application deployed successfully--"