/* eslint-disable no-use-before-define */
import React from 'react';
import { FaWindowClose } from 'react-icons/fa';
import MenuUtil from '../../helper/MenuUtil';
import { CheckList, ICheckList, ICheckListItem, OPTION_TYPE } from '../checked-list/check-list';
import './filter.scss';
interface IsearchProp{
  filters:ICheckListItem[],
  onFilterSelect:(filters:ICheckListItem[])=>void
  onClose:()=>void
}

export const Filter:React.FC<IsearchProp> = ({filters,onFilterSelect,onClose}) =>{
  const topMenu = MenuUtil.getMenuItems();


  const filterCheckList:ICheckList={
    id:'filter-check-list',
    header:'Filter',
    currentSelected:0,
    maxSelected:4,
    options:filters,
  }
  const handleFitlterSelect = (itemId:string,checkId:string,optionType:OPTION_TYPE)=>{
    filters.forEach(filter=>{
      if(filter.id===checkId){
        filter.selected=!filter.selected;
      }
    })
    onFilterSelect(filters);
    console.log('filter selected'+itemId,checkId,optionType)
  }

  return (
    <div className="Filter">
      <div className="filter-container"> 
        <div className="left-overlay" onClick={onClose}>
        </div>
        
        <div className="filter-item-container">
          <CheckList data={filterCheckList} onChange={handleFitlterSelect}></CheckList>
        </div>
      </div>
      
    </div>
  );
}

