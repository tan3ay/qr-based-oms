/* eslint-disable no-use-before-define */
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';

import './search-bar.scss'

interface IsearchProp{
    onChange:(value:string)=>void
    placeHolder:string
}

export const SearchBar:React.FC<IsearchProp> = ({onChange,placeHolder}) =>{
  return (
    <div className="SearchBar">
        <TextField  label={placeHolder} fullWidth={true} onChange={(e)=>onChange(e.target.value)}/>
    </div>
  );
}

