/* eslint-disable no-use-before-define */
import React from 'react';
import TextField from '@material-ui/core/TextField';
import Autocomplete from '@material-ui/lab/Autocomplete';
import MenuListService from '../../helper/MenuUtil';
import './search-bar.scss'

interface IsearchProp{
    onChange:(value:string)=>void
}

export const SearchBar:React.FC<IsearchProp> = ({onChange}) =>{
  const topMenu = MenuListService.getMenuItems();
  const handleChange = (e:React.ChangeEvent<{}>,value:string|null)=>{
    
    if(value==null){
      value='';
    }
    onChange(value);
  }
  return (
    <div className="SearchBar">
        <Autocomplete
            id="free-solo-demo"
            freeSolo
            options={topMenu.map((option) => option.name)}
            renderInput={(params) => (
            <TextField {...params}  label="Search Menu" margin="dense" variant="outlined" onChange={(e)=>onChange(e.target.value)}/>
            )}
        onChange={(e,value)=>handleChange(e,value)}/>
    </div>
  );
}

