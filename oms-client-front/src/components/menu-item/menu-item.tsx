
import { size } from "lodash";
import { Link, useHistory } from "react-router-dom";
import './menu-item.scss'

interface IMenuItemProp{
    header:string,
    detail:string,
    link:string,
    size?:string
}




export const MenuItem:React.FC<IMenuItemProp> = ({header,size,link,detail}) =>{
    const history = useHistory();

    const handleMenuClick = ()=>{
        history.push(link);
    }
    return (
        <div className="MenuItem">
            <div className="menu-item-container" onClick={(e)=>handleMenuClick()}>
                <div className={`menu-item `+size}>
                    <div className="header">
                        {header}
                    </div>
                    <div>
                        {detail}
                    </div>
                </div>
            <Link to={link}></Link>
            </div> 
        </div>
);
            
}