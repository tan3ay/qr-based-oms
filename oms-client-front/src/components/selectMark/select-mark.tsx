import { FaCheck, FaCircle } from "react-icons/fa";

import './select-mark.scss'

export enum SELECT_MARK{
    CHECK='check',
    OPTION='option'
}

interface ISelectMarkProp{
    type:string,
    selected:boolean,
    disable?:boolean
    border?:boolean,
    onClick?:(selected?:boolean)=>void
}

export const SelectMark:React.FC<ISelectMarkProp> = ({type,selected,onClick,disable,border}) =>{
    const isCheckMark = type===SELECT_MARK.CHECK.valueOf()?true:false;
    const classType=selected?'selected':''
    const handleClick=()=>{
        if(onClick)
        onClick();
    }
    return (
        <div className="SelectMark" onClick={handleClick}>
            {
            isCheckMark &&
            <div className={`check-mark ${border?'border':''} ${disable?'disable':''}`} >
                {selected && <FaCheck/>}
            </div>
            }
            {
            !isCheckMark &&
           
            <div className={`option-mark ${border?'border':''} ${disable?'disable':''}`} >
                {selected && <FaCircle/>}
            </div>
           
            }
        </div>
    );
}
