import './loader.scss'

export enum LOADING_TYPES{
    SIMPLE_DIV="SIMPLE_DIV"
}

interface ILodingProp{
    position?:string
    loadingType?:string
}

export const Loader:React.FC<ILodingProp> = ({position,loadingType}) =>{
  
  position =position?position:'top';

  return (
    <div className="Loader loader-container">
      <div className={`spinner-container ${position}`}>
        <div className="loadingio-spinner-eclipse">
          <div className="ldio-8vbapn2qcl5 ">
            <div></div>
          </div>
        </div>
        </div>
    </div>
  );
}