
import { useHistory } from "react-router-dom";
import './order-menu-item.scss'

interface IOrderMenuItemProp{
    header:string,
    detail:string
}




export const OrderMenuItem:React.FC<IOrderMenuItemProp> = ({header,detail}) =>{
    const history = useHistory();
    return (
        <div className="OrderMenuItem">
            <div className="order-menu-item-container" >
                <div className="order-menu-item">
                    <div className="menu-item-header">
                        {header}
                    </div>
                    <div className="menu-item-detail">
                        {detail}
                    </div>
                </div>
            
            </div> 
        </div>
);
            
}