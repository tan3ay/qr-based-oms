import React from 'react';
import { Badge } from '@material-ui/core';
import './toggle-header-item.scss'

interface ItoggleHeaderItem{
selected:boolean,
header:string,
badgeCount?:string,
onClick:()=>void
}
const ToggleHeaderItem:React.FC<ItoggleHeaderItem> = ({selected,header,badgeCount,onClick}) =>{
    const headerClass=`header ${selected ? 'selected' : ''}`
    return (
        <div className="ToggleHeaderItem" onClick={()=>onClick()}>
            <div className="header-container">
                {!badgeCount && 
                    <div className={headerClass}>
                        <Badge badgeContent={badgeCount} invisible color="primary">
                        <span>{header}</span>
                        </Badge>
                    </div>}
                {badgeCount && 
                
                    <div className={headerClass}>
                        <Badge badgeContent={badgeCount}  color="primary">
                        <span>{header}</span>
                        </Badge>
                    </div>
                }
                
            </div>
        </div>

    );
}

export default ToggleHeaderItem;