
import { Dropdown } from 'react-bootstrap';
import { FaEllipsisV } from 'react-icons/fa';
import './button-list.scss';

export interface IButtonProp{
  id:string,
  label:string,
  disable?:boolean,
  onClick?:(id:string)=>void
}

export const ButtonItem:React.FC<IButtonProp> = ({id,label,disable,onClick}) =>{

disable=disable?disable:false
const handleClick = ()=>{
  if(!disable && onClick){
    onClick(id)
  }
}
  return (
    <div className={`button-item-container ${disable?'disable':''}`} onClick={handleClick}>
      <div className="button-item">{label}</div>
    </div>
  );
}

export interface IButtonListProp{
  buttons:IButtonProp[],
  onClick?:(id:string)=>void
}

export const ButtonList:React.FC<IButtonListProp> = ({buttons,onClick}) =>{

    const handleButtonClick = (id:string) =>{
      
      if(onClick){
        onClick(id)
      }
    }
    
    const butoonsDom = buttons.map(
        item => {
          return (
          <ButtonItem key={item.id} id={item.id} label={item.label} disable={item.disable} onClick={handleButtonClick}/>
        );
      });

  return (
    <div className="ButtonList">
      <div className="button-list-container">
        <Dropdown>
        <Dropdown.Toggle id="action-buttons" className="btn icon">
          <FaEllipsisV/>
        </Dropdown.Toggle>

        <Dropdown.Menu>
          {butoonsDom}
        </Dropdown.Menu>
        </Dropdown>
      </div>
      
    </div>
  );
}

