import React from 'react';

import { MenuDto } from '../../models/menu';
import FoodMenuListItem from './food-menu-list-item';
import './food-menu-list.scss';

interface IListProp {
      menuItems:MenuDto[]
}

const FoodMenuList:React.FC<IListProp> = ({menuItems}) =>{

 const menuItemsDom = menuItems.map((item:MenuDto)=>
    {
    return (<FoodMenuListItem key={item.menuId} menuItem={item}></FoodMenuListItem>);
   }
 );

 return(
    <div className="FoodMenuList">
        {menuItemsDom}
    </div>
  );
}

export default FoodMenuList;
