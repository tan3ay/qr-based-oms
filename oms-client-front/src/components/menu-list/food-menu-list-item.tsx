import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { getCostString } from "../../helper/common-util";
import { OrderUtil } from "../../helper/OrderUtil";
import { MenuDto } from "../../models/menu";
import { setMenuOrderItem } from "../../state/reducers/menuReducer";

import { RootState } from "../../state/store";
import { FoodQuantitySelector } from "../quantity-selector/food-quantity-selector";
import "./food-menu-list-item.scss";

interface IProp {
  menuItem: MenuDto;
}

const FoodMenuListItem: React.FC<IProp> = ({ menuItem }) => {
  const history = useHistory();
  const dispatch = useDispatch();

  const order = useSelector((state: RootState) => state.menu.menuCart);

  const quantity = OrderUtil.getQuantity(menuItem, order);

  const handleDecreament = (quantity: number) => {
    const item = OrderUtil.findOrderItem(menuItem, order);
    console.log(item);
    if (item) {
      dispatch(
        setMenuOrderItem({
          ...OrderUtil.getOrderItemVo(menuItem),
          itemQuantity: quantity,
        })
      );
    }
  };

  const handleIncreament = (quantity: number) => {
    dispatch(
      setMenuOrderItem({
        ...OrderUtil.getOrderItemVo(menuItem),
        itemQuantity: quantity,
      })
    );
  };

  const handleAddClick = () => {
    console.log("addMenu");
    history.push("/addMenu/" + menuItem.menuId);
  };
  const imagePresent = menuItem.imgUrl !== "";
  return (
    <div className="FoodMenuListItem">
      <div className="list-item-container">
        {/* { menuItem.isCustomizable===1 && quantity>0 && */}
        {quantity > 0 && (
          <div className="quantity-tag-container">
            <div className="quantity-triangle"></div>
            <div className="quantity-tag">{quantity}</div>
          </div>
        )}
        <div className="image-container">
          {imagePresent && (
            <img className="image" alt="" src={menuItem.imgUrl}></img>
          )}
          {!imagePresent && quantity > 0 && <div className="empty-image"></div>}
        </div>
        <div className="v-container">
          <div className="header-container">
            <div className="header">{menuItem.name}</div>
            <div className="cost">{getCostString(menuItem.cost)}</div>
          </div>
          <div className="">
            <div className="detail">{menuItem.desc}</div>
          </div>
          {menuItem.isCustomizable === 0 && (
            <div className="quantity-container">
              <FoodQuantitySelector
                type="filled"
                quantity={quantity}
                onIncreament={handleIncreament}
                onDecreament={handleDecreament}
              />
            </div>
          )}

          {menuItem.isCustomizable === 1 && (
            <div className="button-container">
              <button className="btn normal" onClick={(e) => handleAddClick()}>
                Add<Link to="/addMenu"></Link>
              </button>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default FoodMenuListItem;
