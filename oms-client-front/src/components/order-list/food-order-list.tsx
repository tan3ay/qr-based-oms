import _ from "lodash";
import React, { useRef } from "react";
import { useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router";
import { BrowserCacheUtil } from "../../helper/BrowserCacheUtil";
import { OrderUtil } from "../../helper/OrderUtil";
import { OrderDto } from "../../models/order";
import { RootState } from "../../state/store";
import { EmptyData } from "../empty-data/empty-data";
import FoodOrderContainer from "./food-order-container";
import "./food-order-list.scss";
import KitchenOrder from "./kitchen-order-item/kitchen-order";
import KitchenOrderListItem from "./kitchen-order-item/kitchen-order";
import PaymentOrder from "./payment-order-item/payment-order";
import PaymentOrderListItem from "./payment-order-item/payment-order";

interface RouteParams {
  kitchenId: string;
}

export enum LIST_TYPE {
  KITCHEN = "KITCHEN",
  PAYMENT = "PAYMENT",
}

interface IListProp {
  listType: LIST_TYPE;
  orders: OrderDto[];
  scrollable?: boolean;
  selectMode?: boolean;
  selectedItems?: string[];
  onUpdate?: (updateStatus: string) => void;
  onSelect?: (selectedKeys: string[]) => void;
}

const FoodOrderList: React.FC<IListProp> = ({
  selectMode,
  selectedItems,
  orders,
  listType,
  scrollable,
  onSelect,
}) => {
  let { kitchenId } = useParams<RouteParams>();
  // const [selectedItems,setSelectedItems] = useState<string[]>([]);
  const orderState = useSelector((state: RootState) => state.editOrder);

  selectedItems = selectedItems ? selectedItems : [];
  const handleSelect = (selectKey: string, selected: boolean) => {
    if (onSelect && selectedItems) {
      if (selected) {
        const newItems = [...selectedItems, selectKey];
        console.log(newItems, selected, selectKey);
        // setSelectedItems([...newItems])
        onSelect(newItems);
      } else {
        const newItems = selectedItems.filter((item) => item !== selectKey);
        console.log(newItems, selected, selectKey);
        // setSelectedItems([...newItems])
        onSelect(newItems);
      }
      //console.log(selectKey,selected);
    }
  };

  const orderItemsDom = orders.map((order: OrderDto) => {
    if (
      listType === LIST_TYPE.KITCHEN &&
      OrderUtil.isOrderBelongsToKitchen(order, kitchenId)
    ) {
      return (
        <KitchenOrder
          key={order.orderId + "kitchen"}
          orderDto={order}
          kitchenId={kitchenId}
        ></KitchenOrder>
      );
    } else if (listType === LIST_TYPE.PAYMENT) {
      if (order.orderId === orderState.editOrder.orderId) {
        selectedItems = selectedItems ? selectedItems : [];
        return (
          <FoodOrderContainer
            key={orderState.editOrder.orderId + "edit"}
            selectMode={selectMode}
            selected={selectedItems?.indexOf(order.orderId) > -1}
            selectKey={order.orderId}
            onSelect={handleSelect}
          >
            <PaymentOrder
              orderDto={orderState.editOrder}
              isEditOrder={true}
            ></PaymentOrder>
          </FoodOrderContainer>
        );
      } else {
        selectedItems = selectedItems ? selectedItems : [];
        return (
          <FoodOrderContainer
            key={order.orderId + "view"}
            selectMode={selectMode}
            selected={selectedItems?.indexOf(order.orderId) > -1}
            selectKey={order.orderId}
            onSelect={handleSelect}
          >
            <PaymentOrderListItem
              orderDto={order}
              selectMode={selectMode}
              isEditOrder={false}
            ></PaymentOrderListItem>
          </FoodOrderContainer>
        );
      }
    }
  });

  const listRef = useRef<HTMLDivElement>(null);

  const scrollPointerRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (scrollable) {
      if (listRef !== null && listRef.current != null) {
        const offsetTop = BrowserCacheUtil.get("list_scroll_key");
        listRef.current.scrollTo({
          top: parseFloat(offsetTop),
        });
      }
    }
  }, []);

  const handleScroll = _.throttle(() => {
    let scrollOffset = 0;
    if (scrollPointerRef.current) {
      scrollOffset =
        scrollPointerRef.current?.offsetTop +
        Math.abs(scrollPointerRef.current?.getBoundingClientRect().top);
      BrowserCacheUtil.put(scrollOffset + "", "list_scroll_key", 3);
    }
  }, 600);

  return (
    <div
      ref={listRef}
      onScroll={handleScroll}
      className={`FoodOrderList ${scrollable ? "scrollable" : ""}`}
    >
      <div ref={scrollPointerRef}></div>
      {orderItemsDom}
      {orderItemsDom.length === 0 && (
        <EmptyData header="No more orders found." />
      )}
    </div>
  );
};

export default FoodOrderList;
