import React, { useState } from "react";
import { SelectMark, SELECT_MARK } from "../selectMark/select-mark";
import "./food-order-container.scss";

interface IListItemProp {
  selectKey: string;
  selectMode?: boolean;
  selected?: boolean;
  onSelect?: (key: string, selected: boolean) => void;
}

const FoodOrderContainer: React.FC<IListItemProp> = ({
  selectKey,
  selectMode,
  selected,
  onSelect,
  ...props
}) => {
  selected = selected ? selected : false;
  const handleClick = () => {
    if (onSelect) {
      if (selectMode) onSelect(selectKey, !selected);
    }
  };
  let selectClass = "";
  if (selectMode !== undefined && selectMode) {
    if (selected !== undefined) {
      selectClass = selected ? "selected" : "unselected";
    }
  }
  return (
    <div className="FoodOrderContainer">
      <div
        className={`food-order-item-container ${selectClass}`}
        onClick={(e) => handleClick()}
      >
        <div className={`food-order-item-selector-container`}>
          {selectMode && (
            <div className="selector-tag-container">
              <div className="selector-triangle"></div>
              <div className="select-tag">
                <SelectMark
                  selected={selected}
                  onClick={handleClick}
                  type={SELECT_MARK.CHECK}
                />
              </div>
            </div>
          )}
        </div>
        {props.children}
      </div>
    </div>
  );
};

export default FoodOrderContainer;
