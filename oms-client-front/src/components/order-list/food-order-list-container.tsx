import React, { useState } from 'react';
import { OrderLists } from '../../models/order';


import './food-order-list-container.scss';



interface IListContainerProp{
  orderLists:OrderLists[]
}

const FoodOrderListContainer:React.FC<IListContainerProp> = ({orderLists}) =>{
  
  
  const orderItemsDom = orderLists.map((item:OrderLists)=>
     {
       return (<div key={"Orders"}>
                <div className="list-header">Orders</div>
                {/* <FoodOrderList key={1} orders={item.orderList}></FoodOrderList> */}
              </div>);
            }
 );


  return (<div className="FoodOrderListContainer">
              {orderItemsDom}
  </div>);
}

export default FoodOrderListContainer;
