import _ from "lodash";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { OrderUtil } from "../../../helper/OrderUtil";
import {
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../../../models/order";
import {
  itemStatusUpdate,
  orderServed,
  startOrder,
  undoOrder,
} from "../../../state/reducers/orderReducer";
import { RootState } from "../../../state/store";
import { ChipItem } from "../../ChipItem/chip-item";
import { OrderMenuItem } from "../../order-menu-item/order-menu-item";
import { SelectMark } from "../../selectMark/select-mark";
import "./kitchen-order.scss";

interface IOrderDetailLineProp {
  item: OrderItemDto;
  parent: OrderDto;
}

const OrderDetailItemLine: React.FC<IOrderDetailLineProp> = ({
  item,
  parent,
}) => {
  const dispatch = useDispatch();

  const kitchenStatus: ORDER_STATUS = OrderUtil.getKitchenStatusForOrder(
    parent,
    item.kitchenId
  );
  //defaultChecked={item.itemStatus===ORDER_ITEM_STATUS.SERVED}
  //item.isDeleted=true;
  const handleChekBoxClick = () => {
    if (kitchenStatus === ORDER_STATUS.PREPARING) {
      if (item.itemStatus === ORDER_ITEM_STATUS.PREPARING) {
        dispatch(
          itemStatusUpdate(parent, item.itemId, ORDER_ITEM_STATUS.SERVED)
        );
      } else {
        dispatch(
          itemStatusUpdate(parent, item.itemId, ORDER_ITEM_STATUS.PREPARING)
        );
      }
    }
  };

  // console.log(item);
  // console.log(!item.isDeleted && kitchenStatus<=ORDER_STATUS.PREPARING.valueOf());
  const itemDetail = item.isCustomizable ? item.itemDetail : "";
  return (
    <div className="order-line-container h-container ">
      {item.itemStatus !== ORDER_ITEM_STATUS.DELETED && (
        <div className="order-item-contents" onClick={handleChekBoxClick}>
          {kitchenStatus > ORDER_STATUS.PLACED &&
            kitchenStatus < ORDER_STATUS.SERVED && (
              <SelectMark
                key={item.itemId + "select"}
                type="check"
                disable={false}
                border={false}
                selected={item.itemStatus === ORDER_ITEM_STATUS.SERVED}
              />
            )}
          <OrderMenuItem
            header={`${item.itemQuantity} x ${item.itemName}`}
            detail={itemDetail}
          />
          {/* <span className="order-line-left left">{item.itemQuantity} x&nbsp;{item.itemName}</span> */}
          <ChipItem
            label={OrderUtil.getOrderItemStatusString(item.itemStatus)}
            color={OrderUtil.getOrderItemColorString(item.itemStatus)}
          />
        </div>
      )}

      {item.itemStatus === ORDER_ITEM_STATUS.DELETED && (
        <div className="order-item-contents">
          <span className="order-line-left left text-strike">
            {item.itemQuantity} x&nbsp;{item.itemName}
          </span>
          <ChipItem label="Deleted" color="gray" />
        </div>
      )}
    </div>
  );
};

interface IProp {
  orderDto: OrderDto;
  kitchenId: string;
}

const KitchenOrder: React.FC<IProp> = ({ orderDto, kitchenId }) => {
  const dispatch = useDispatch();

  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const restaurantId = restaurantMeta ? restaurantMeta.restaurantId : "";

  const filteredKitchenItem = orderDto.items.filter(
    (item) => item.kitchenId === kitchenId
  );
  const orderItem = filteredKitchenItem.map((item: OrderItemDto) => {
    return (
      <OrderDetailItemLine
        key={item.itemId}
        item={item}
        parent={orderDto}
      ></OrderDetailItemLine>
    );
  });
  let isAllItemServed: boolean = true;
  isAllItemServed = !orderDto.items.find(
    (item) => item.itemStatus === ORDER_ITEM_STATUS.PREPARING
  );

  const kitchenStatus: ORDER_STATUS = OrderUtil.getKitchenStatusForOrder(
    orderDto,
    kitchenId
  );

  const handleUpdateOrder = async (status: ORDER_STATUS) => {
    try {
      const newOrder = _.cloneDeep(orderDto);
      if (status === ORDER_STATUS.SERVED)
        dispatch(orderServed(newOrder, kitchenId));
      else if (status === ORDER_STATUS.PREPARING)
        dispatch(startOrder(newOrder, kitchenId));
    } catch (err) {
      console.error("Failed to update order", err);
    }
  };

  const handleUndoOrder = async (status: ORDER_STATUS) => {
    try {
      const newOrder = _.cloneDeep(orderDto);
      dispatch(undoOrder(newOrder, kitchenId));
    } catch (err) {
      console.error("Failed to update order", err);
    }
  };

  const waitingTime = Math.ceil(
    (Date.now().valueOf() - orderDto.createdAt) / 60000
  );

  return (
    <div className="KitchenOrder">
      <div className="list-item-container">
        <div className="v-container">
          <div className="order-status-container">
            <div className="order-status-top"></div>
            <div className="order-status">
              {OrderUtil.getOrderStatusText(kitchenStatus.valueOf().toString())}
            </div>
          </div>

          <div className="header-container">
            <div className="order-id-container">
              <span className="header">Order Id:</span>
              <span className="detail">{orderDto.orderId}</span>
            </div>
            <div className="table-no-container">
              <span className="header">Table No:</span>
              <span className="detail">{orderDto.tableId}</span>
            </div>
          </div>

          <div className="order-waiting-container">
            <span className="header">Waiting from:</span>
            <span className="detail">{waitingTime} min</span>
          </div>

          <div className="header">Order Item:</div>
          <div className="menu-item-container">
            <div className="detail">{orderItem}</div>
          </div>
          <div className="order-notes-container">
            <div className="header">Order Notes:</div>
            <div className="detail">{orderDto.orderNotes}</div>
          </div>
          <div className="button-container h-container ">
            {kitchenStatus === ORDER_STATUS.PLACED && (
              <button
                className="btn right"
                onClick={(e) => handleUpdateOrder(ORDER_STATUS.PREPARING)}
              >
                Start Order
              </button>
            )}
            {kitchenStatus === ORDER_STATUS.PREPARING && (
              <button
                className="btn right"
                onClick={(e) => handleUpdateOrder(ORDER_STATUS.SERVED)}
              >
                Order Served
              </button>
            )}
            {kitchenStatus === ORDER_STATUS.SERVED && (
              <button
                className="btn right"
                onClick={(e) => handleUndoOrder(ORDER_STATUS.PREPARING)}
              >
                UNDO
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default KitchenOrder;
