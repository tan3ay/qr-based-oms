import React from "react";
import { OrderDto } from "../../../models/order";
import { PaymentEditOrder } from "./payment-edit-order";
import "./payment-order.scss";
import { PaymentViewOrder } from "./payment-view-order";

interface IProp {
  orderDto: OrderDto;
  isEditOrder: boolean;
  selectMode?: boolean;
}

const PaymentOrder: React.FC<IProp> = ({
  selectMode,
  orderDto,
  isEditOrder,
}) => {
  selectMode = selectMode ? selectMode : false;
  return (
    <div
      className="PaymentOrder
    "
    >
      {isEditOrder && <PaymentEditOrder orderDto={orderDto} />}
      {!isEditOrder && (
        <PaymentViewOrder selectMode={selectMode} orderDto={orderDto} />
      )}
    </div>
  );
};
export default PaymentOrder;
