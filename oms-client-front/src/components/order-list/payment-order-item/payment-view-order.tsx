import _ from "lodash";
import { FaPencilAlt, FaPrint } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { getCostString } from "../../../helper/common-util";
import { OrderUtil } from "../../../helper/OrderUtil";
import {
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../../../models/order";
import { editOrder } from "../../../state/reducers/editOrderReducer";
import { payOrder, undoPaidOrder } from "../../../state/reducers/orderReducer";
import { RootState } from "../../../state/store";
import { ChipItem } from "../../ChipItem/chip-item";
import { OrderMenuItem } from "../../order-menu-item/order-menu-item";
import { OrderDetailLine } from "./payment-order-detail-line";

interface IOrderDetailLineProp {
  item: OrderItemDto;
  parent: OrderDto;
}

interface IProp {
  orderDto: OrderDto;
  selectMode?: boolean;
}

const OrderItemViewLine: React.FC<IOrderDetailLineProp> = ({
  item,
  parent,
}) => {
  const dispatch = useDispatch();
  const itemDetail = item.isCustomizable ? item.itemDetail : "";
  return (
    <div className="order-line-container">
      <div className="order-item-contents">
        {item.itemStatus !== ORDER_ITEM_STATUS.DELETED && (
          <div className="order-line h-container">
            <div className="order-line-left left header">
              <OrderMenuItem
                header={`${item.itemQuantity} x ${item.itemName}`}
                detail={itemDetail}
              />
            </div>
            {/* <OrderMenuItem header={`${item.itemQuantity} x ${item.itemName}`} detail={itemDetail}/> */}
            <ChipItem
              label={OrderUtil.getOrderItemStatusString(item.itemStatus)}
              color={OrderUtil.getOrderItemColorString(item.itemStatus)}
            />
            <div className="order-line-left right detail">
              {item.itemCost * item.itemQuantity}
            </div>
          </div>
        )}
        {item.itemStatus === ORDER_ITEM_STATUS.DELETED && (
          <div className="order-line h-container">
            <div className="order-line-left left header text-strike">
              {item.itemQuantity}x{item.itemName}
            </div>
            <ChipItem label="Deleted" color="gray" />
            <div className="order-line-left right detail text-strike">
              {item.itemCost * item.itemQuantity}
            </div>
          </div>
        )}
      </div>
    </div>
  );
};
export const PaymentViewOrder: React.FC<IProp> = ({ selectMode, orderDto }) => {
  const dispatch = useDispatch();
  const gws = useSelector((state: RootState) => state.webSocket.gsw);
  const restaurantId = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta.restaurantId
  );

  const orderStatus = OrderUtil.getCombineOrderStatus(orderDto); //orderDto.status;
  const history = useHistory();
  const orderItem = orderDto.items.map((item: OrderItemDto) => {
    return (
      <OrderItemViewLine
        key={item.itemId}
        item={item}
        parent={orderDto}
      ></OrderItemViewLine>
    );
  });

  const handleUpdateOrder = async (status: ORDER_STATUS) => {
    try {
      const newOrder = _.cloneDeep(orderDto);
      dispatch(payOrder(newOrder));
    } catch (err) {
      console.error("Failed to update order", err);
    }
  };

  const handleUndoOrder = async (status: ORDER_STATUS) => {
    try {
      const newOrder = _.cloneDeep(orderDto);
      dispatch(undoPaidOrder(newOrder));
    } catch (err) {
      console.error("Failed to update order", err);
    }
  };

  const printOrder = (order: OrderDto) => {
    // window.print();
    history.push("/ordersReceipt/" + order.orderId);
    //PrintUtil.printOrder(order);
    console.log("Print order", order);
  };

  selectMode = selectMode ? selectMode : false;

  return (
    <div className={`list-item-container ${selectMode ? "select-mode" : ""}`}>
      <div className="v-container">
        <div className="header-container">
          <div className="order-status-container">
            <div className="order-status-top"></div>
            <div className="order-status">
              {OrderUtil.getOrderStatusText(orderStatus.toString())}
            </div>
          </div>
          {!(orderStatus === ORDER_STATUS.PAID) && (
            <div className="icon-button-container right">
              <div
                className="edit-order-button icon icon-primary"
                onClick={(e) => dispatch(editOrder(orderDto))}
              >
                <FaPencilAlt />
              </div>
              <div
                className="print-order-button icon icon-primary"
                onClick={(e) => printOrder(orderDto)}
              >
                <FaPrint />
              </div>
            </div>
          )}
        </div>
        <div className="header-container">
          <div className="order-id-container">
            <span className="header">Order Id:</span>
            <span className="detail">{orderDto.orderId}</span>
          </div>
          <div className="table">Table No:{orderDto.tableId}</div>
        </div>

        <div className="order-edit-container">
          <div className="order-detail-container v-container pd-3-t ">
            <div className="order-total-cost-order pd-2-b border-1-b">
              <OrderDetailLine
                left="Total Cost"
                right={getCostString(orderDto.cost.totalCost)}
              ></OrderDetailLine>
            </div>
            <div className="menu-item-container">{orderItem}</div>

            {orderDto.cost.gst !== 0 && (
              <div className="order-detail-cost-container">
                <OrderDetailLine
                  left="Sub Total"
                  right={getCostString(orderDto.cost.subTotal)}
                ></OrderDetailLine>
                <OrderDetailLine
                  left="GST"
                  right={getCostString(orderDto.cost.gst)}
                ></OrderDetailLine>
              </div>
            )}
          </div>

          <div className="button-container h-container ">
            {!(orderStatus === ORDER_STATUS.PAID) && (
              <button
                className="btn right"
                onClick={(e) => handleUpdateOrder(ORDER_STATUS.PAID)}
              >
                Payment Done
              </button>
            )}
            {orderStatus === ORDER_STATUS.PAID && (
              <button
                className="btn right"
                onClick={(e) => handleUndoOrder(ORDER_STATUS.SERVED)}
              >
                UNDO
              </button>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};
