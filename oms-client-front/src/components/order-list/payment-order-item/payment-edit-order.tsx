import { FaPlus } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router";
import { getCostString } from "../../../helper/common-util";
import { OrderUtil } from "../../../helper/OrderUtil";
import {
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
} from "../../../models/order";
import {
  editOrder,
  saveEditOrder,
  editOrderItemUpdated,
} from "../../../state/reducers/editOrderReducer";
import { RootState } from "../../../state/store";
import { ChipItem } from "../../ChipItem/chip-item";
import { OrderMenuItem } from "../../order-menu-item/order-menu-item";
import { FoodQuantitySelector } from "../../quantity-selector/food-quantity-selector";
import { OrderDetailLine } from "./payment-order-detail-line";

interface IOrderDetailLineProp {
  item: OrderItemDto;
}

interface IProp {
  orderDto: OrderDto;
  selectMode?: boolean;
}

const OrderItemEditLine: React.FC<IOrderDetailLineProp> = ({ item }) => {
  const dispatch = useDispatch();
  const handleIncreament = (quantityNew: number) => {
    dispatch(editOrderItemUpdated({ ...item, itemQuantity: quantityNew }));
  };

  const handleDecreament = (quantityNew: number) => {
    dispatch(editOrderItemUpdated({ ...item, itemQuantity: quantityNew }));
  };

  const itemDetail = item.isCustomizable ? item.itemDetail : "";
  //item.isDeleted=true;
  return (
    <div className="order-line-container h-container ">
      <div className="order-item-contents h-container">
        {item.itemStatus !== ORDER_ITEM_STATUS.DELETED && (
          <div className="order-line h-container pd-2-t">
            {/* <div className="order-line-left left">{item.itemName}</div> */}
            <div className="order-line-left header left">
              <OrderMenuItem header={item.itemName} detail={itemDetail} />
            </div>

            <ChipItem
              label={OrderUtil.getOrderItemStatusString(item.itemStatus)}
              color={OrderUtil.getOrderItemColorString(item.itemStatus)}
            />
            <div className="order-line-left right">
              {item.itemStatus < ORDER_ITEM_STATUS.SERVED && (
                <div className="quantity-container ">
                  <FoodQuantitySelector
                    type="filled"
                    quantity={item.itemQuantity}
                    onIncreament={handleIncreament}
                    onDecreament={handleDecreament}
                  />
                </div>
              )}
            </div>
          </div>
        )}
        {item.itemStatus === ORDER_ITEM_STATUS.DELETED && (
          <div className="order-line h-container pd-2-t">
            <div className="order-line-left left text-strike">
              {item.itemQuantity}x{item.itemName}
            </div>
            <ChipItem label="Deleted" color="gray" />
            <div className="order-line-left right"></div>
          </div>
        )}
      </div>
    </div>
  );
};

const printOrderItemStatus = (header: string, order: OrderDto) => {
  console.log("Order Status");
  order.items.forEach((item) => {
    console.log(
      "id:" +
        item.itemId +
        " quantity:" +
        item.itemQuantity +
        " isDelete:" +
        (item.itemStatus === ORDER_ITEM_STATUS.DELETED)
    );
  });
};

export const PaymentEditOrder: React.FC<IProp> = ({ selectMode }) => {
  const dispatch = useDispatch();

  const orderState = useSelector((state: RootState) => state.editOrder);

  const orderDto: OrderDto = orderState.editOrder;

  const history = useHistory();

  const orderItem = orderState.editOrder.items.map((item: OrderItemDto) => {
    return (
      <OrderItemEditLine key={item.itemId} item={item}></OrderItemEditLine>
    );
  });

  const handleCancelEditOrder = async () => {
    dispatch(editOrder({} as OrderDto));
  };

  const handleAddNewItem = async () => {
    history.push("/menu");
  };

  const handleSaveOrder = async () => {
    try {
      dispatch(saveEditOrder(orderDto));
    } catch (err) {
      console.error("ERROR Failed to save order", err);
    }
  };

  selectMode = selectMode ? selectMode : false;
  return (
    <div
      className={`list-item-container edit ${selectMode ? "select-mode" : ""}`}
    >
      <div className="v-container">
        <div className="header-container">
          <div className="order-status-container">
            <div className="order-status-top"></div>
            <div className="order-status">
              {OrderUtil.getOrderStatusText(
                OrderUtil.getCombineOrderStatus(orderDto) + ""
              )}
            </div>
          </div>
        </div>
        <div className="header-container">
          <div className="order-id">Order Id:{orderDto.orderId}</div>
          <div className="table">Table No:{orderDto.tableId}</div>
        </div>

        <div className="order-view-container">
          <div className="order-detail-container v-container pd-3-t ">
            <div className="order-total-cost-order pd-2-b border-1-b">
              <OrderDetailLine
                left="Total Cost"
                right={getCostString(orderDto.cost.totalCost)}
              ></OrderDetailLine>
            </div>
            <div className="order-item-container pd-2-b border-1-b">
              {orderItem}
              <div
                className="order-item-add-buttton-container pd-2-b"
                onClick={(e) => handleAddNewItem()}
              >
                <FaPlus className="icon icon-primary" />
              </div>
            </div>

            {orderDto.cost.gst !== 0 && (
              <div className="order-detail-cost-container">
                <OrderDetailLine
                  left="Sub Total"
                  right={getCostString(orderDto.cost.subTotal)}
                ></OrderDetailLine>
                <OrderDetailLine
                  left="GST"
                  right={getCostString(orderDto.cost.gst)}
                ></OrderDetailLine>
              </div>
            )}
          </div>
        </div>
        <div className="button-container h-container-right">
          <button className="btn " onClick={(e) => handleCancelEditOrder()}>
            Cancel
          </button>
          <button className="btn mg-3-l" onClick={(e) => handleSaveOrder()}>
            Save
          </button>
        </div>
      </div>
    </div>
  );
};
