import { faClosedCaptioning } from '@fortawesome/free-solid-svg-icons';
import { Chip } from '@material-ui/core';
import { useState } from 'react';
import { FaCheck } from 'react-icons/fa';
import './chip-item.scss'

export interface IChipItemProp{
    label:string,
    color:string
}


  
  export const ChipItem:React.FC<IChipItemProp> = ({label,color}) =>{
      
      return (
        <div className="ChipItem">
            <div className={`chip-item-container ${color}`}>
                <Chip label={label} />
            </div>
            
       </div>
      );
  }