import { useState } from "react";
import { FaArrowLeft } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory, useParams } from "react-router-dom";
import { CheckList, OPTION_TYPE } from "../components/checked-list/check-list";
import { PageHeader } from "../components/page-header/page-header";
import { FoodQuantitySelector } from "../components/quantity-selector/food-quantity-selector";
import { convertToNumber } from "../helper/common-util";
import { OrderUtil } from "../helper/OrderUtil";
import { MenuDto } from "../models/menu";
import { cloneMenuDtoToVo } from "../models/menuVo";
import { OrderDto, OrderItemDto } from "../models/order";
import { setMenuOrderItem } from "../state/reducers/menuReducer";

import { RootState } from "../state/store";
import "./food-menu-add-page.scss";

interface RouteParams {
  menuId: string;
}

const FoodMenuAddPage: React.FC = () => {
  let { menuId } = useParams<RouteParams>();
  const menuDtos: MenuDto[] = useSelector(
    (state: RootState) => state.menu.menuItems
  );

  // const complexMenu:MenuDto = menuDtoMock;
  let complexMenu: MenuDto | undefined = menuDtos.find(
    (item) => item.menuId === menuId
  );
  const menuDto: MenuDto = complexMenu ? complexMenu : ({} as MenuDto);

  const [menuVo, setMenuVo] = useState(cloneMenuDtoToVo(menuDto));
  const [quantity, setQuantity] = useState(1);
  const history = useHistory();
  const handleCheckSelect = (
    listId: string,
    optionId: string,
    optionType: OPTION_TYPE
  ) => {
    console.log(listId, optionId, optionType);
    const newMenu = { ...menuVo };
    newMenu.itemsVo?.forEach((item) => {
      let selCount = 0;
      if (item.id === listId) {
        if (optionType === OPTION_TYPE.CHECKBOX) {
          item.options.forEach((option) => {
            if (option.id === optionId) option.selected = !option.selected;
            if (option.selected) selCount++;
          });
          item.currentSelected = selCount;
        } else if (optionType === OPTION_TYPE.RADIO) {
          item.options.forEach((option) => {
            if (option.id === optionId) option.selected = true;
            else option.selected = false;
          });
          item.currentSelected = 1;
        }
      }
    });
    setMenuVo(newMenu);
  };

  const dispatch = useDispatch();

  const handleMenuAdd = () => {
    let detailString = "";
    menuVo.itemsVo?.forEach((item) => {
      detailString += "\n" + item.header;
      item.options.forEach((option) => {
        if (option.selected) {
          detailString += ":" + option.label + ",";
          menuVo.cost = menuVo.cost === null ? 0 : menuVo.cost;
          menuVo.cost += convertToNumber(option.cost);
        }
      });
      detailString += ",";
    });

    const orderItem: OrderItemDto = OrderUtil.getOrderItemVo2(menuVo);
    console.log("OrderItemDto", orderItem);
    orderItem.itemDetail = detailString;
    orderItem.itemQuantity = quantity;
    dispatch(setMenuOrderItem(orderItem));
    history.goBack();
  };
  const listDom = menuVo.itemsVo?.map((item) => {
    if (item.maxSelected > 0)
      return (
        <CheckList key={item.id} data={item} onChange={handleCheckSelect} />
      );
  });

  return (
    <div className="FoodMenuAddPage flex-v">
      <div className="image-container">
        {/* <img className="image" alt="" src={menuVo.imgUrl} ></img> */}
      </div>
      <div className="menu-info-container flex-content-fixed">
        <PageHeader
          header={menuVo.name}
          headerAlign="center"
          left={
            <button
              className="back-button btn icon"
              onClick={(e) => {
                history.goBack();
              }}
            >
              <FaArrowLeft />
              <Link to="/dashboard"></Link>
            </button>
          }
        ></PageHeader>
      </div>
      <div className="menu-item-list-container flex-content-scroll">
        <div className="detail menu-description">{menuVo.desc}</div>
        {listDom}
      </div>

      <div className="menu-action-container flex-content-fixed">
        <div className="menu-quantity-container">
          <FoodQuantitySelector
            type="filled"
            size="m"
            quantity={quantity}
            onIncreament={(quantity) => setQuantity(quantity)}
            onDecreament={(quantity) => setQuantity(quantity)}
          />
        </div>
        <div className="button-container ">
          {quantity > 0 && (
            <button
              className="btn big-button full-width"
              onClick={(e) => handleMenuAdd()}
            >
              Add to basket<Link to="/menu"></Link>
            </button>
          )}
          {quantity === 0 && (
            <button className="btn big-button full-width disable">
              Add to basket<Link to="/menu"></Link>
            </button>
          )}
        </div>
      </div>
    </div>
  );
};

export default FoodMenuAddPage;
