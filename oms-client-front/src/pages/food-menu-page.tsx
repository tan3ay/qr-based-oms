import { Badge, Button } from "@material-ui/core";
import * as _ from "lodash";
import React, { useEffect, useState } from "react";
import { FaArrowLeft, FaSlidersH } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { Link, RouteComponentProps } from "react-router-dom";
import { ICheckListItem } from "../components/checked-list/check-list";
import { Filter } from "../components/filter/filter";
import { Loader } from "../components/Loader/loader";
import FoodMenuListContainer from "../components/menu-list/food-menu-list-container";
import { PageHeader } from "../components/page-header/page-header";
import { SearchBar } from "../components/search/search-bar";
import { BrowserCacheUtil, CASH_KEYS } from "../helper/BrowserCacheUtil";
import { isEmpty } from "../helper/common-util";
import { MenuSearchService } from "../helper/MenuSearchService";
import MenuUtil from "../helper/MenuUtil";
import { OrderUtil } from "../helper/OrderUtil";
import { Filters, MenuDto } from "../models/menu";
import { OrderDto } from "../models/order";
import { editOrder } from "../state/reducers/editOrderReducer";
import {
  addCartMenuToEditOrder,
  fetchMenu,
  setMenuOrder,
} from "../state/reducers/menuReducer";
import { RootState } from "../state/store";
import "./food-menu-page.scss";

interface Props {
  restaurantId: string;
  filters: Filters;
  menu: MenuDto;
}

export interface SearchVo {
  key: string;
  filters: ICheckListItem[];
}

interface IFoodMenuPageProp extends RouteComponentProps {}

const FoodMenuPage: React.FC<IFoodMenuPageProp> = ({ history }) => {
  const dispatch = useDispatch();
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const editOrder: OrderDto = useSelector(
    (state: RootState) => state.editOrder.editOrder
  );

  const menuDtos: MenuDto[] = useSelector(
    (state: RootState) => state.menu.menuItems
  );
  const menuCart: OrderDto = useSelector(
    (state: RootState) => state.menu.menuCart
  );
  const [menuListsVo, setMenuListsVo] = useState(
    MenuUtil.getMenuListEntities(menuDtos)
  );

  const loading = useSelector((state: RootState) => state.menu.loading);

  useEffect(() => {
    setMenuListsVo(MenuUtil.getMenuListEntities(menuDtos));
  }, [menuDtos]);

  const filtersVo = [
    { label: "Chef special", id: "isChefSpecial", selected: false },
    // {label:'Vegan',id:'isVegan',selected:false},
    { label: "Vegetarian", id: "isVeg", selected: false },
    { label: "Drinks", id: "isDrink", selected: false },
  ];

  const [searchVo, setSearchVo] = useState<SearchVo>({
    key: "",
    filters: filtersVo,
  });

  const initializeMenu = async () => {
    if (restaurantMeta) {
      await dispatch(fetchMenu());
    }
  };

  React.useEffect(() => {
    if (menuDtos.length == 0) initializeMenu();
  }, [restaurantMeta]);

  const handleSearch = (value: string) => {
    searchVo.key = value;
    setSearchVo({ ...searchVo, key: value });
    updateSearchResult(searchVo);
  };

  const handleFilterSelect = (items: ICheckListItem[]) => {
    setSearchVo({ ...searchVo, filters: items });
    updateSearchResult(searchVo);
  };

  const updateSearchResult = (searchVo: SearchVo) => {
    if (menuDtos.length > 0) {
      let filteredList = MenuSearchService.fiterBySearchVo(menuDtos, searchVo);
      console.log("handle search filtered list", filteredList);
      setMenuListsVo(filteredList);
    }
  };

  const handleAddToOrder = () => {
    console.log("Add to order");
    try {
      dispatch(addCartMenuToEditOrder(editOrder, menuCart));
      history.goBack();
    } catch (err) {}
  };

  const [filter, setFilter] = useState(false);
  const handleFilterClose = () => {
    console.log("Filter closed");
    setFilter(false);
  };
  const handleFilterOpen = () => {
    console.log("Filter open");
    setFilter(true);
  };
  return (
    <div className="FoodMenuPage flex-v">
      {filter && (
        <Filter
          filters={searchVo.filters}
          onFilterSelect={handleFilterSelect}
          onClose={handleFilterClose}
        />
      )}
      <div className="fixed-header  flex-content-fixed">
        <div className="page-header-container">
          <PageHeader
            header="Add new dishes"
            headerAlign="center"
            left={
              <button
                className="back-button btn icon"
                onClick={(e) => {
                  history.goBack();
                }}
              >
                <FaArrowLeft />
                <Link to="/dashboard"></Link>
              </button>
            }
          ></PageHeader>
        </div>

        <div className="search-container">
          <SearchBar onChange={handleSearch} placeHolder="Search.."></SearchBar>
          <Button
            className="btn filter-button icon-button"
            onClick={(e) => {
              handleFilterOpen();
            }}
          >
            <Badge
              badgeContent={
                searchVo.filters.filter((filter) => filter.selected).length
              }
              color="primary"
            >
              <FaSlidersH />
              {/* <FaShoppingBasket/>   */}
            </Badge>
            <Link to="/Order"></Link>
          </Button>
        </div>
      </div>
      <div className="content flex-content-fixed">
        {!loading && (
          <div className="menu-list-container">
            <FoodMenuListContainer menuLists={menuListsVo} />
          </div>
        )}
        {loading && (
          <div className="loader-container">
            <Loader loadingType="simple" />
          </div>
        )}
      </div>
      <div className="action-button-container footer flex-content-fixed">
        <div className="add-button-container full-width">
          <button
            className="btn big-button full-width"
            onClick={(e) => {
              handleAddToOrder();
            }}
          >
            <Badge badgeContent={menuCart.items.length} color="primary">
              Add to order
            </Badge>
            <Link to="/Order"></Link>
          </button>
        </div>
      </div>
    </div>
  );
};

export default FoodMenuPage;
