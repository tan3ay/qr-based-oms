import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, useParams } from "react-router";
import { OrderService } from "../api/order-service";
import { EVENT_TYPE } from "../models/event";
import { OrderDto, ORDER_ITEM_STATUS } from "../models/order";
import { fetchKitchenOrders } from "../state/reducers/orderReducer";
import {
  setPendingNotification,
  setWebSocketState,
  WEBSOCKET_STATE,
} from "../state/reducers/webSocketReducer";
import { RootState } from "../state/store";
import "./receipt-page.scss";

interface IReceiptDetailLine {
  left: string;
  right: string;
  customClass?: string;
}

export const ReceiptDetailLine: React.FC<IReceiptDetailLine> = ({
  left,
  right,
  customClass,
}) => {
  customClass = customClass ? customClass : "";
  const containerClass = "receipt-line-container h-container " + customClass;
  return (
    <div className={containerClass}>
      <div className="receipt-line-left left ">{left}</div>
      <div className="receipt-line-right right">{right}</div>
    </div>
  );
};

interface IReceiptProp {}

interface RouteParams {
  orderId: string;
}

export const Receipt: React.FC<IReceiptProp> = () => {
  const history = useHistory();
  const gws = useSelector((state: RootState) => state.webSocket.gsw);
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const restaurantId = restaurantMeta ? restaurantMeta.restaurantId : "";
  let { orderId } = useParams<RouteParams>();
  const orders: OrderDto[] = useSelector(
    (state: RootState) => state.orders.orders
  );
  const targetOrder = orders.find((order) => order.orderId === orderId);
  const dispatch = useDispatch();

  const initPaymentOrder = async () => {
    if (restaurantMeta) {
      await dispatch(fetchKitchenOrders(restaurantMeta.restaurantId));
    }
  };
  useEffect(() => {
    if (restaurantMeta && orders.length === 0) initPaymentOrder();
  }, [restaurantMeta]);
  const items = targetOrder?.items.map((item) => {
    if (item.itemStatus !== ORDER_ITEM_STATUS.DELETED) {
      return (
        <div key={item.itemId} className="receipt-item-container">
          <div className="left">{`${item.itemQuantity} x ${item.itemName}`}</div>
          <div className="right">{item.itemQuantity * item.itemCost}</div>
        </div>
      );
    }
  });
  const [buttonVisible, setButtonVisible] = useState(true);

  const buttonRef = useRef<HTMLDivElement>(null);
  const handlePrint = () => {
    // setButtonVisible(false);
    let oldClass: string | null | undefined =
      buttonRef.current?.getAttribute("class");
    buttonRef.current?.setAttribute("class", "hide");
    window.print();
    oldClass = oldClass ? oldClass : "";
    buttonRef.current?.setAttribute("class", oldClass);
    setButtonVisible(true);
  };
  const handlePrint2 = async () => {
    // setButtonVisible(false);
    try {
      if (!targetOrder) return;
      if (!gws || gws.readyState > 1) {
        console.log("food order setting websocket to disconnect" + new Date());
        dispatch(setWebSocketState(WEBSOCKET_STATE.DISCONNECTED));
        console.log("food order setting pending notification" + new Date());
        dispatch(
          setPendingNotification(
            OrderService.getPrintOrderNotification(
              restaurantId,
              targetOrder,
              EVENT_TYPE.ORDER_ITEM_UPDATE
            )
          )
        );
      } else if (gws.readyState == 1) {
        OrderService.postNotification(
          gws,
          OrderService.getPrintOrderNotification(
            restaurantId,
            targetOrder,
            EVENT_TYPE.ORDER_ITEM_UPDATE
          )
        );
      }
      // const res = await PrintUtil.printOrder();
    } catch (e) {
      console.log("Handling error: Printing using browser print ", e);
      handlePrint();
    }
  };
  const handleBack = () => {
    // history.back();
    history.goBack();
  };
  return (
    <div className="Receipt ">
      <div className="receipt-container normal-font">
        <div className="header-container big-font">
          <div className="restaurant-name ">{restaurantMeta?.name}</div>
          <div className="restaurant-address">{restaurantMeta?.address}</div>
        </div>
        <ReceiptDetailLine
          left="Item"
          customClass="header border-bottom"
          right="Cost"
        />
        <div className="receipt-items-container">{items}</div>
        <div className="order-cost-container">
          <ReceiptDetailLine
            left="Total"
            customClass="header border-top"
            right={targetOrder?.cost.totalCost + ""}
          />
          {targetOrder?.cost.gst !== 0 && (
            <div>
              <ReceiptDetailLine
                left="Sub total"
                right={targetOrder?.cost.subTotal + ""}
              />
              <ReceiptDetailLine
                left="GST"
                right={targetOrder?.cost.gst + ""}
              />
            </div>
          )}
        </div>
      </div>
      {buttonVisible && (
        <div ref={buttonRef} className="button-container">
          <button
            className="btn left"
            onClick={(e) => {
              handleBack();
            }}
          >
            Go Back
          </button>
          <button className="btn right" onClick={(e) => handlePrint2()}>
            Print
          </button>
        </div>
      )}
    </div>
  );
};
