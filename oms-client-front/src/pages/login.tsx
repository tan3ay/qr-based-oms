import { AmplifyAuthFields } from "@aws-amplify/ui-react";
import { TextField } from "@material-ui/core"
import {Auth} from 'aws-amplify'
import { useState } from "react";
import { useHistory } from "react-router";
import './login.scss'
interface ILoginProp{

}


export const Login:React.FC<ILoginProp> = () =>{
    const [email,setEmail] = useState('');
    const [password,setPassword] = useState('');
    const history = useHistory();
    const handleSubmit = async (event:React.FormEvent<HTMLButtonElement>)=>{
        console.log(email,password);
        event.preventDefault();
        const res = await Auth.signIn(email,password);
        if(res){
            history.push('/dashboard');
        }
        console.log(res);
    }

    return (
    
        <div className="Login">
            <AmplifyAuthFields/>
            <div className="login-container">
            <TextField className="email" onChange={(e)=>setEmail(e.target.value)}/>
            <TextField  className="password" onChange={(e)=>setPassword(e.target.value)}/>
            <button className="btn submit-button" onClick={(e)=>handleSubmit(e)}>Login</button>
            </div>
        </div>
    );
}

export default Login;