import { AmplifySignOut } from "@aws-amplify/ui-react";
import { Button } from "@material-ui/core";
import { Auth } from "aws-amplify";
import { useEffect } from "react";
import { useState } from "react";
import { useSelector } from "react-redux";
import { Link, useHistory } from "react-router-dom";
import { MenuItem } from "../components/menu-item/menu-item";
import { PageHeader } from "../components/page-header/page-header";
import { KitchenDto } from "../models/restaurantMeta";
import { RootState } from "../state/store";
import "./dashboard-page.scss";
interface IDashboardProp {}

export const Dashboard: React.FC<IDashboardProp> = () => {
  const history = useHistory();
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const kitchenMeta = restaurantMeta !== null ? restaurantMeta.kitchens : [];
  const [kitchens, setKitchens] = useState<KitchenDto[]>(kitchenMeta);
  const kitchenDom = kitchens.map((kitchen) => {
    return (
      <MenuItem
        key={kitchen.kitchenId}
        header={kitchen.name}
        link={"/kitchen/" + kitchen.kitchenId}
        detail=""
      />
    );
  });

  useEffect(() => {
    if (restaurantMeta) setKitchens(restaurantMeta.kitchens);
  }, [restaurantMeta]);

  // const imageUrl = "https://scontent.fsin10-1.fna.fbcdn.net/v/t31.18172-8/17311009_1250607738325839_3420964177342726693_o.jpg?_nc_cat=108&ccb=1-5&_nc_sid=730e14&_nc_ohc=_3CFRcK5AlwAX_4lSKm&_nc_ht=scontent.fsin10-1.fna&oh=e055ce2ca7c4602911a0c1a95bfdbe63&oe=614D7071";
  const imageUrl = "/images/Abc.jpeg";
  const signoutStyle = {
    //'background-image': 'linear-gradient(#00c6b6, #ffdc31)'
    color: "red",
  };

  const handleSignOut = () => {
    Auth.signOut().then(() => {
      history.push("/login");
    });
  };
  return (
    <div className="Dashboard">
      {restaurantMeta && (
        <div className=" flex-v">
          {/* <img className="bg" src={imageUrl}></img> */}
          <div className="header-container flex-content-fixed">
            <PageHeader
              header=""
              headerAlign="center"
              right={
                <AmplifySignOut
                  amplify-primary-color="#00c6b6"
                  style={signoutStyle}
                />
              }
              // right={<Button  className="btn" onClick={(e)=>handleSignOut()}  style={signoutStyle}>Sign Out</Button>}
            />
          </div>
          <div className="content flex-content-scroll">
            <div className="title-container">
              <div className="title">{restaurantMeta.name}</div>
            </div>
            <div className="payement-container">
              <MenuItem
                header="Order Management"
                size="large"
                link={"/orders/0"}
                detail=""
              />
            </div>
            <div className=" kitchen-list-container">
              <div className="kitchen-list-header">Kitchens</div>
              <div className="kitchen-item-container">{kitchenDom}</div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};
