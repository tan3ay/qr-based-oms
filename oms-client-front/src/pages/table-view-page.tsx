import _ from "lodash";
import { useContext, useEffect, useRef, useState } from "react";
import { FaArrowLeft } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { Link, useHistory, useParams } from "react-router-dom";
import { OrderService } from "../api/order-service";
import {
  ChipBar,
  IChipProp,
  SELECT_TYPE,
} from "../components/chip-bar/chip-bar";
import { Loader } from "../components/Loader/loader";
import FoodOrderList, {
  LIST_TYPE,
} from "../components/order-list/food-order-list";
import { PageHeader } from "../components/page-header/page-header";
import { SearchBar } from "../components/search/search-bar";
import ToggleHeaderItem from "../components/toggleHeaderMenu/toggle-header-item";
import { WebSocketContext } from "../components/websocket/WebSocket";
import { BrowserCacheUtil } from "../helper/BrowserCacheUtil";
import { OrderUtil } from "../helper/OrderUtil";
import { OrderDto } from "../models/order";
import { fetchKitchenOrders } from "../state/reducers/orderReducer";
import { RootState } from "../state/store";
import "./table-view-page.scss";

interface ITableProp {}

export interface SearchVo {
  key: string;
  filters: IChipProp[];
}

interface RouteParams {
  tabId: string;
}

export const TableView: React.FC<ITableProp> = () => {
  let { tabId } = useParams<RouteParams>();
  const dispatch = useDispatch();
  const history = useHistory();

  const orders: OrderDto[] = useSelector(
    (state: RootState) => state.orders.orders
  );
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );

  // const tables:string[]=OrderUtil.getTables(orders);

  const [tables, setTables] = useState<string[]>([
    ...OrderUtil.getTables(orders),
  ]);

  // const orderVo:OrderDto[] = OrderUtil.sortLatestUpdate(orders)
  const [orderVo, setOrderVo] = useState<OrderDto[]>(
    _.cloneDeep(OrderUtil.sortLatestUpdate(orders))
  );
  const loading = useSelector((state: RootState) => state.orders.loading);

  const statusFiltersVo = [
    { label: "Pending Orders", value: "3", selected: true },
    { label: "Paid Orders", value: "4", selected: false },
    { label: "All", value: "5", selected: false },
  ];

  const [searchVo, setSearchVo] = useState({
    key: "",
    filters: statusFiltersVo,
  });

  const initPaymentOrder = async () => {
    if (restaurantMeta) {
      await dispatch(fetchKitchenOrders(restaurantMeta.restaurantId));
    }
  };

  const tableList = tables.map((table: string) => {
    const link: string = "/tableOrders/" + table;
    return (
      <div
        className="dinning-table-view"
        key={table}
        onClick={() => handleClick(table)}
      >
        <Link to={"/tableOrders/" + table}></Link>
        {table}
      </div>
    );
  });

  // When restaurant meta change update load orders
  useEffect(() => {
    if (restaurantMeta && orders.length === 0) {
      initPaymentOrder();
    }
    setTables([...OrderUtil.getTables(orders)]);
    setOrderVo(_.cloneDeep(OrderUtil.sortLatestUpdate(orders)));
  }, [restaurantMeta]);

  // When Order state canges update all dependent states
  useEffect(() => {
    setTables([...OrderUtil.getTables(orders)]);
    setOrderVo(_.cloneDeep(OrderUtil.sortLatestUpdate(orders)));
    updateOrders(searchVo);
  }, [orders]);

  const handleTableSearch = (value: string) => {
    let tables = OrderUtil.getTables(orders);
    if (value !== "")
      tables = tables.filter(
        (item) => item.toLowerCase().indexOf(value.toLowerCase()) > -1
      );

    setTables(tables);
  };

  const handleClick = (table: string) => {
    history.push("/tableOrders/" + table);
  };

  const [toggle, setToggle] = useState(tabId === "0");
  const handleToggleClick1 = () => {
    history.replace("/orders/0");
    setToggle(true);
  };
  const handleToggleClick2 = () => {
    history.replace("/orders/1");
    setToggle(false);
  };

  const handleOrderSearch = (value: string) => {
    searchVo.key = value;
    setSearchVo(searchVo);
    updateOrders(searchVo);
  };
  const handleFilterSelect = (items: IChipProp[]) => {
    console.log("Filter Selected" + JSON.stringify(items));

    searchVo.filters = items;

    setSearchVo(searchVo);
    updateOrders(searchVo);
  };

  const updateOrders = (searchVo: SearchVo) => {
    if (orders.length > 0) {
      let filteredList = OrderService.filterOrdersByTableId(orders, searchVo);
      setOrderVo(filteredList);
    }
  };

  const gws = useContext(WebSocketContext);
  gws.onmessage = async (evt: MessageEvent) => {
    console.log("Message Received:" + evt.data);
    console.log("Updating Payment Order");
    if (JSON.parse(evt.data) !== "SENT_SUCCESS_ACK") {
      await initPaymentOrder();
    }
  };

  const scrollParentRef = useRef<HTMLDivElement>(null);

  const scrollPointerRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    console.log("scroll use effect");
    if (scrollParentRef !== null && scrollParentRef.current != null) {
      const offsetTop = BrowserCacheUtil.get("all_order_list_scroll_key");
      scrollParentRef.current.scrollTo({
        top: parseFloat(offsetTop),
      });
    }
  }, [toggle]);

  const handleScroll = _.throttle(() => {
    let scrollOffset = 0;
    if (scrollPointerRef.current) {
      scrollOffset =
        scrollPointerRef.current?.offsetTop +
        Math.abs(scrollPointerRef.current?.getBoundingClientRect().top);
      BrowserCacheUtil.put(scrollOffset + "", "all_order_list_scroll_key", 3);
    }
  }, 600);

  return (
    <div className="TableView flex-v">
      <div className="flex-content-fixed">
        <div className="table-page-header">
          <PageHeader
            header="Order Management"
            headerAlign="center"
            left={
              <button
                className="back-button btn icon"
                onClick={(e) => {
                  history.push("/dashboard");
                }}
              >
                <FaArrowLeft />
                <Link to="/dashboard"></Link>
              </button>
            }
          />
        </div>
        <div className="toggle-header-container">
          <ToggleHeaderItem
            header="Tables"
            selected={toggle}
            onClick={handleToggleClick1}
          />
          <ToggleHeaderItem
            header="All Order"
            selected={!toggle}
            onClick={handleToggleClick2}
          />
        </div>
      </div>

      <div
        ref={scrollParentRef}
        onScroll={handleScroll}
        className="flex-content-scroll content"
      >
        {loading && <Loader loadingType="simple" position="top" />}
        {toggle && (
          <div className="dinning-table-view-container">
            <div className="search-bar-container">
              <SearchBar
                onChange={handleTableSearch}
                placeHolder="Search table no"
              ></SearchBar>
            </div>
            <div className="table-list-container">{tableList}</div>
          </div>
        )}
        {!toggle && (
          <div className="order-view-container">
            <div ref={scrollPointerRef} className="search-bar-container">
              <SearchBar
                onChange={handleOrderSearch}
                placeHolder="Search order by table number"
              ></SearchBar>
            </div>
            <div className="order-status-filter">
              <ChipBar
                chipData={searchVo.filters}
                selectType={SELECT_TYPE.SINGLE}
                onSelectChange={(items) => handleFilterSelect(items)}
              ></ChipBar>
            </div>
            <FoodOrderList
              key="table-order-list"
              orders={orderVo}
              listType={LIST_TYPE.PAYMENT}
            ></FoodOrderList>
          </div>
        )}
      </div>
    </div>
  );
};
