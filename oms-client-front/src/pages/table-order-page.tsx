import React, { useEffect, useState } from "react";
import { FaArrowLeft } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { RouteComponentProps, useParams } from "react-router-dom";
import { ButtonList, IButtonProp } from "../components/button-list/button-list";
import { Loader } from "../components/Loader/loader";
import FoodOrderList, {
  LIST_TYPE,
} from "../components/order-list/food-order-list";
import { PageHeader } from "../components/page-header/page-header";
import ToggleHeaderItem from "../components/toggleHeaderMenu/toggle-header-item";
import { OrderUtil } from "../helper/OrderUtil";
import { OrderDto } from "../models/order";
import {
  fetchKitchenOrders,
  mergeOrders,
} from "../state/reducers/orderReducer";
import { RootState } from "../state/store";
import "./table-order-page.scss";

const orderData: string =
  '[{"restaurantId":"1","orderKey":"order#1628267328686","orderId":"1628267328686","status":3,"tableId":"1","totalCost":250,"items":[{"itemId":"1","itemName":"Paneer Tikka","itemQuantity":2,"itemStatus":1},{"itemId":"2","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628268492048","orderId":"1628268492048","status":1,"tableId":"1","totalCost":250,"items":[{"itemId":"1","itemName":"Paneer Tikka","itemQuantity":2,"itemStatus":1},{"itemId":"2","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628270931294","orderId":"1628270931294","status":1,"tableId":"1","totalCost":57.2,"items":[{"itemId":"1628258959921","itemName":"Paneer Tikka","itemQuantity":4,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628271070874","orderId":"1628271070874","status":1,"tableId":"1","totalCost":48.4,"items":[{"itemId":"1628258959921","itemName":"Paneer Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628258988241","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628259102205","itemName":"Non Veg Thali","itemQuantity":1,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628271090794","orderId":"1628271090794","status":1,"tableId":"1","totalCost":48.4,"items":[{"itemId":"1628258959921","itemName":"Paneer Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628258988241","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628259102205","itemName":"Non Veg Thali","itemQuantity":1,"itemStatus":1}]}]';

interface RouteParams {
  tableNo: string;
}

interface IFoodOrderProp extends RouteComponentProps {}

export enum PAYMENT_ORDER_ACTION {
  SELECT = "SELECT",
  SELECT_ALL = "SELECT_ALL",
  CLEAR_SELECT = "CLEAR_SELECT",
  MERGE = "MERGE",
  DELETE = "DELETE",
}

export const PaymentOrder: React.FC<IFoodOrderProp> = ({ history }) => {
  let { tableNo } = useParams<RouteParams>();
  const dispatch = useDispatch();
  const [toggle, setToggle] = useState(true);

  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );

  const orders: OrderDto[] = useSelector(
    (state: RootState) => state.orders.orders
  );
  const loading = useSelector((state: RootState) => state.orders.loading);

  const initPaymentOrder = async () => {
    if (restaurantMeta) {
      await dispatch(fetchKitchenOrders(restaurantMeta.restaurantId));
    }
  };

  useEffect(() => {
    if (restaurantMeta && orders.length === 0) initPaymentOrder();
  }, [restaurantMeta]);

  const handleToggleClick1 = () => {
    setToggle(true);
  };
  const handleToggleClick2 = () => {
    setToggle(false);
  };

  const gws = useSelector((state: RootState) => state.webSocket.gsw);
  gws.onmessage = async (evt: MessageEvent) => {
    // TODO Update this to only initi order if updated order table number is same as current table number
    // Or need to think better approach

    console.log("Message Received:" + evt.data);

    try {
      if (JSON.parse(evt.data) !== "SENT_SUCCESS_ACK") {
        console.log("Updating Payment Order on table view page");
        await initPaymentOrder();
      }
    } catch (err) {
      console.error(
        "ERROR Failed to update payment order for table",
        tableNo,
        err
      );
    }
  };

  const pendingOrders = OrderUtil.getPendingPaymentOrderForTable(
    orders,
    tableNo
  );

  const [selectedOrders, setSelectedOrders] = useState<string[]>([]);

  const handlePendingOrderSelect = (selectedOrders: string[]) => {
    setSelectedOrders(selectedOrders);
    setActions([
      ...OrderUtil.getActionButtonBasedOnSelectState(
        selectMode,
        selectedOrders.length
      ),
    ]);
    console.log(selectedOrders);
  };

  const [selectMode, setSelectMode] = useState(false);
  const [actions, setActions] = useState<IButtonProp[]>(
    OrderUtil.getActionButtonBasedOnSelectState(
      selectMode,
      selectedOrders.length
    )
  );

  const handleActionClick = async (actionId: string) => {
    console.log("Take action for ", actionId);
    switch (actionId) {
      case PAYMENT_ORDER_ACTION.SELECT:
        setSelectMode(true);
        setActions([
          ...OrderUtil.getActionButtonBasedOnSelectState(
            true,
            selectedOrders.length
          ),
        ]);
        break;
      case PAYMENT_ORDER_ACTION.SELECT_ALL:
        const allOrderIds = pendingOrders.map((order) => order.orderId);
        setSelectedOrders(allOrderIds);
        setSelectMode(true);
        setActions([
          ...OrderUtil.getActionButtonBasedOnSelectState(
            true,
            allOrderIds.length
          ),
        ]);
        break;
      case PAYMENT_ORDER_ACTION.CLEAR_SELECT:
        setSelectedOrders([]);
        setSelectMode(false);
        setActions([...OrderUtil.getActionButtonBasedOnSelectState(true, 0)]);
        break;
      case PAYMENT_ORDER_ACTION.MERGE:
        dispatch(mergeOrders(selectedOrders, pendingOrders));
        setSelectedOrders([]);
        setSelectMode(false);
        setActions([
          ...OrderUtil.getActionButtonBasedOnSelectState(
            false,
            selectedOrders.length
          ),
        ]);
        break;
      case PAYMENT_ORDER_ACTION.DELETE:
        console.log("Delete orders", selectedOrders);
        setSelectedOrders([]);
        setSelectMode(false);
        setActions([
          ...OrderUtil.getActionButtonBasedOnSelectState(
            false,
            selectedOrders.length
          ),
        ]);
        break;
      default:
        console.log("[ERROR]Wrong action");
    }
  };

  return (
    <div className="TableOrder flex-v">
      <div className="flex-content-fixed">
        <div className="payment-page-header">
          <PageHeader
            header={"Table Number:" + tableNo}
            headerAlign="center"
            left={
              <button
                className="btn icon"
                onClick={(e) => {
                  history.goBack();
                }}
              >
                <FaArrowLeft />
              </button>
            }
          />
        </div>

        <div className="toggle-header-container">
          <ToggleHeaderItem
            header="Unpaid"
            badgeCount={pendingOrders.length + ""}
            selected={toggle}
            onClick={handleToggleClick1}
          />
          <ToggleHeaderItem
            header="Paid"
            selected={!toggle}
            onClick={handleToggleClick2}
          />
        </div>
      </div>
      <div className="flex-content-fixed content">
        {toggle && (
          <div className="food-order-list-action-container">
            <ButtonList buttons={actions} onClick={handleActionClick} />
          </div>
        )}
        <div className="food-order-list-container">
          {loading && <Loader loadingType="simple" position="top" />}
          {toggle && (
            <FoodOrderList
              selectMode={selectMode}
              selectedItems={selectedOrders}
              onSelect={handlePendingOrderSelect}
              key="pending-payment-list"
              scrollable={true}
              orders={pendingOrders}
              listType={LIST_TYPE.PAYMENT}
            ></FoodOrderList>
          )}
          {!toggle && (
            <FoodOrderList
              key="finished-payment-list"
              orders={OrderUtil.getFinishedPaymentOrderForTable(
                orders,
                tableNo
              )}
              scrollable={true}
              listType={LIST_TYPE.PAYMENT}
            ></FoodOrderList>
          )}
        </div>
      </div>
      {/* <FoodOrderListContainer orderLists={orderLists}></FoodOrderListContainer> */}
    </div>
  );
};
