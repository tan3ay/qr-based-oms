import React, { useContext, useEffect, useState } from "react";
import { FaArrowLeft } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { Link, RouteComponentProps, useParams } from "react-router-dom";
import { Loader } from "../components/Loader/loader";
import FoodOrderList, {
  LIST_TYPE,
} from "../components/order-list/food-order-list";
import { PageHeader } from "../components/page-header/page-header";
import ToggleHeaderItem from "../components/toggleHeaderMenu/toggle-header-item";
import { WebSocketContext } from "../components/websocket/WebSocket";
import {
  handleKitchenNotification,
  playNotification,
} from "../helper/common-util";
import { OrderUtil } from "../helper/OrderUtil";
import { fetchKitchenOrders } from "../state/reducers/orderReducer";
import { RootState } from "../state/store";
import "./kitchen-order-page.scss";

const orderData: string =
  '[{"restaurantId":"1","orderKey":"order#1628267328686","orderId":"1628267328686","status":1,"tableId":"1","totalCost":250,"items":[{"itemId":"1","itemName":"Paneer Tikka","itemQuantity":2,"itemStatus":1},{"itemId":"2","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628268492048","orderId":"1628268492048","status":1,"tableId":"1","totalCost":250,"items":[{"itemId":"1","itemName":"Paneer Tikka","itemQuantity":2,"itemStatus":1},{"itemId":"2","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628270931294","orderId":"1628270931294","status":1,"tableId":"1","totalCost":57.2,"items":[{"itemId":"1628258959921","itemName":"Paneer Tikka","itemQuantity":4,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628271070874","orderId":"1628271070874","status":1,"tableId":"1","totalCost":48.4,"items":[{"itemId":"1628258959921","itemName":"Paneer Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628258988241","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628259102205","itemName":"Non Veg Thali","itemQuantity":1,"itemStatus":1}]},{"restaurantId":"1","orderKey":"order#1628271090794","orderId":"1628271090794","status":1,"tableId":"1","totalCost":48.4,"items":[{"itemId":"1628258959921","itemName":"Paneer Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628258988241","itemName":"Chicken Tikka","itemQuantity":1,"itemStatus":1},{"itemId":"1628259102205","itemName":"Non Veg Thali","itemQuantity":1,"itemStatus":1}]}]';

interface RouteParams {
  kitchenId: string;
}
interface IFoodOrderProp extends RouteComponentProps {}

export const KitchenOrder: React.FC<IFoodOrderProp> = ({ history }) => {
  let { kitchenId } = useParams<RouteParams>();
  const restaurantMeta = useSelector(
    (state: RootState) => state.restaurant.restaurantMeta
  );
  const kitchen = restaurantMeta.kitchens.find(
    (item) => item.kitchenId === kitchenId
  );
  const kitchenName = kitchen ? kitchen.name : "";

  const orderState = useSelector((state: RootState) => state.orders);
  const orders = orderState.orders;
  const loading = orderState.loading;
  const [toggle, setToggle] = useState(true);

  const dispatch = useDispatch();

  const initializeOrders = async () => {
    if (restaurantMeta) {
      await dispatch(fetchKitchenOrders(restaurantMeta.restaurantId));
    }
  };

  useEffect(() => {
    if (restaurantMeta && orders.length === 0) initializeOrders();
  }, [restaurantMeta]);

  const handleToggleClick1 = () => {
    setToggle(true);
  };
  const handleToggleClick2 = () => {
    setToggle(false);
  };

  const pendingOrders = OrderUtil.getPendingKitchenOrder(orders, kitchenId);

  const gws = useContext(WebSocketContext);
  gws.onmessage = async (evt: MessageEvent) => {
    console.log("Message Received:" + evt.data);
    if (JSON.parse(evt.data) !== "SENT_SUCCESS_ACK") {
      const needToUpdate = handleKitchenNotification(evt, kitchenId);
      if (needToUpdate) {
        await initializeOrders();
        playNotification();
      }
    }
  };

  return (
    <div className="KitchenOrderPage flex-v">
      <div className="flex-content-fixed ">
        <div className="kitchen-page-header ">
          <PageHeader
            header={kitchenName}
            headerAlign="center"
            left={
              <button
                className="back-button btn icon"
                onClick={(e) => {
                  history.push("/dashboard");
                }}
              >
                <FaArrowLeft />
                <Link to="/dashboard"></Link>
              </button>
            }
          />
        </div>
        <div className="toggle-header-container ">
          <ToggleHeaderItem
            header="Pending"
            badgeCount={pendingOrders.length + ""}
            selected={toggle}
            onClick={handleToggleClick1}
          />
          <ToggleHeaderItem
            header="Served"
            selected={!toggle}
            onClick={handleToggleClick2}
          />
        </div>
      </div>

      <div className="flex-content-scroll content">
        {loading && <Loader loadingType="simple" position="top" />}
        {toggle && (
          <FoodOrderList
            orders={pendingOrders}
            listType={LIST_TYPE.KITCHEN}
          ></FoodOrderList>
        )}
        {!toggle && (
          <FoodOrderList
            orders={OrderUtil.getFinishedKitchenOrder(orders, kitchenId)}
            listType={LIST_TYPE.KITCHEN}
          ></FoodOrderList>
        )}
      </div>
    </div>
  );
};

const mockData: string =
  '[{"restaurantId":"1","orderKey":"order#1629206565783","orderId":"1629206565783","status":1,"tableId":"2","cost":{"gst":0.4,"totalCost":4.4,"subTotal":4},"orderNotes":"s","items":[{"itemId":"1629192300860","itemName":"LASSI","itemQuantity":1,"isCustomizable":0,"customizableValue":null,"itemStatus":1,"kitchenId":"1","itemCost":4}],"kitchenStatus":[{"kitchenId":"1","status":2}],"createdAt":1629206565783,"updatedAt":1629209232686},{"restaurantId":"1","orderKey":"order#1629207923748","orderId":"1629207923748","status":1,"tableId":"3","cost":{"gst":1.3,"totalCost":14.3,"subTotal":13},"orderNotes":"dsd","items":[{"itemId":"1629192300860","itemName":"LASSI","itemQuantity":1,"isCustomizable":0,"customizableValue":null,"itemStatus":1,"kitchenId":"1","itemCost":4},{"itemId":"1629192301005","itemName":"LASSI MANGUE","itemQuantity":1,"isCustomizable":0,"customizableValue":null,"itemStatus":1,"kitchenId":"1","itemCost":5},{"itemId":"1629192300989_649b1e59-85c5-5041-9b6f-5aaaa47423ea","itemName":"Veg Platter","itemQuantity":1,"isCustomizable":1,"customizableValue":null,"itemStatus":1,"kitchenId":"0","itemCost":4}],"kitchenStatus":[{"kitchenId":"1","status":0},{"kitchenId":"0","status":0}],"createdAt":1629207923748,"updatedAt":1629207923748}]';
