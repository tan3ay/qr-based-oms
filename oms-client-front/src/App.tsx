import {
  AmplifyAuthenticator,
  AmplifySignIn,
  AmplifySignUp,
} from "@aws-amplify/ui-react";
import { withAuthenticator } from "@aws-amplify/ui-react/lib-esm/withAuthenticator";
import { faPlay } from "@fortawesome/free-solid-svg-icons";
import Amplify, { Hub, Auth } from "aws-amplify";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import useSound from "use-sound";
import { isNull } from "util";
import { OrderService } from "./api/order-service";
import { RestaurantService } from "./api/restaurant-service";
import "./App.css";
import awsmobile from "./aws-exports";
import { Loader } from "./components/Loader/loader";
import { WebSocketContext } from "./components/websocket/WebSocket";
import { isEmpty } from "./helper/common-util";
import { Dashboard } from "./pages/dashboard-page";
import FoodMenuAddPage from "./pages/food-menu-add-page";
import FoodMenuPage from "./pages/food-menu-page";
import { KitchenOrder } from "./pages/kitchen-order-page";
import Login from "./pages/login";
import { Receipt } from "./pages/receipt-page";
import { PaymentOrder } from "./pages/table-order-page";
import { TableView } from "./pages/table-view-page";

import { fetchRestaurantMeta } from "./state/reducers/restaurantMetaReducer";
import {
  sentNotification,
  setPendingNotifications,
  setWebSocket,
  setWebSocketState,
  WEBSOCKET_STATE,
} from "./state/reducers/webSocketReducer";

import { RootState, store } from "./state/store";

export const WS_BASE =
  "wss://8irvmkg0qk.execute-api.us-east-2.amazonaws.com/dev?restaurantId=1629791235991";

Amplify.configure(awsmobile);

const App: React.FC = () => {
  const dispatch = useDispatch();

  const restaurant = useSelector((state: RootState) => state.restaurant);

  if (isEmpty(restaurant.restaurantMeta) && !restaurant.loading) {
    dispatch(fetchRestaurantMeta("1629791235991"));
  }

  const [loading, setLoading] = useState(true);

  useEffect(() => {
    if (!isEmpty(restaurant.restaurantMeta)) {
      setLoading(false);
    }
  }, [restaurant.restaurantMeta]);

  const gwsState = useSelector(
    (state: RootState) => state.webSocket.webSocketState
  );
  const gws = useSelector((state: RootState) => state.webSocket.gsw);
  const pendingNotif = useSelector(
    (state: RootState) => state.webSocket.pendingNotif
  );

  function initWebSocket() {
    if (
      gwsState === WEBSOCKET_STATE.DISCONNECTED &&
      (!gws || gws.readyState > 1)
    ) {
      console.log("Setting web Socket");
      dispatch(setWebSocketState(WEBSOCKET_STATE.CONNECTING));
      setTimeout(() => {
        dispatch(setWebSocket(new WebSocket(WS_BASE)));
      }, 100);
    }
  }
  gws.onopen = function () {
    console.log("onopen: Connected " + new Date());
    dispatch(setWebSocketState(WEBSOCKET_STATE.CONNECTED));
    if (pendingNotif.length > 0) {
      console.log(
        "onopen: Sending pending notification " + new Date(),
        pendingNotif.length
      );
      pendingNotif.forEach((notification) => {
        gws.send(JSON.stringify({ action: "notify", data: notification }));
        //gws.send(JSON.stringify(notification));
      });
      dispatch(setPendingNotifications([]));
      dispatch(sentNotification());
    }
  };
  gws.onclose = function () {
    console.log("connection closed" + new Date());
    dispatch(setWebSocketState(WEBSOCKET_STATE.DISCONNECTED));
  };

  useEffect(() => {
    initWebSocket();
  }, [gwsState]);

  return !loading ? (
    <div className="root">
      <BrowserRouter>
        <WebSocketContext.Provider value={gws}>
          <Route path="/dashboard" exact component={Dashboard}></Route>
          <Route
            path="/kitchen/:kitchenId"
            exact
            component={KitchenOrder}
          ></Route>
          <Route
            path="/tableOrders/:tableNo"
            exact
            component={PaymentOrder}
          ></Route>
          <Route path="/menu/" exact component={FoodMenuPage}></Route>
          <Route
            path="/addMenu/:menuId"
            exact
            component={FoodMenuAddPage}
          ></Route>
          <Route path="/orders/:tabId" exact component={TableView}></Route>
          <Route
            path="/ordersReceipt/:orderId"
            exact
            component={Receipt}
          ></Route>
          <Route
            path="/error"
            exact
            component={() => <div>404 Not Found</div>}
          ></Route>
        </WebSocketContext.Provider>
      </BrowserRouter>
    </div>
  ) : (
    <div>
      <div className="loader-container">
        <Loader loadingType="simple" />
      </div>
    </div>
  );
};

export default withAuthenticator(App);
//  export default App;
