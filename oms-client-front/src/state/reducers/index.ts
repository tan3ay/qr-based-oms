import { combineReducers } from "redux";
import { editOrderReducer } from "./editOrderReducer";
import { menuReducer } from "./menuReducer";
import { orderReducer } from "./orderReducer";
import { restaurantReducer } from "./restaurantMetaReducer";
import { webSocketReducer } from "./webSocketReducer";
export const rootReducer = combineReducers({
  orders: orderReducer,
  editOrder: editOrderReducer,
  menu: menuReducer,
  restaurant: restaurantReducer,
  webSocket: webSocketReducer,
});

// export default rootReducer;
