import _ from "lodash";
import { Dispatch } from "react";
import { MenuService } from "../../api/menu-service";
import { BrowserCacheUtil, CASH_KEYS } from "../../helper/BrowserCacheUtil";
import { OrderUtil } from "../../helper/OrderUtil";

import { MenuDto } from "../../models/menu";
import { OrderDto, OrderItemDto } from "../../models/order";
import { editOrder } from "./editOrderReducer";

// MENU
export enum MENU_ACTIONS {
  SET_MENU = "SET_MENU",
  SET_MENU_ITEM = "SET_MENU_ITEM",
  SET_ORDER = "SET_ORDER",
  SET_MENU_ORDER_ITEM = "SET_MENU_ORDER_ITEM",

  UPDATE_MENU_CART_ITEM = "[Menu] Updating menu cart item",
  MENU_CART_ITEM_UPDATED = "[Menu] Updated menu cart item",

  RESET_MENU_CART = "[Menu] Resetting menu cart",

  REQUEST_SUCCESS = "[Menu] Request Success",
  REQUEST_FAILED = "[Menu] Request Failed",

  FETCH_MENU = "[Menu] Fetch Menu",
  MENU_UPDATED = "[Menu] Menu Updated",

  ADD_MENU_CART_TO_ORDER = "[Menu] Adding menu cart to order",
  MENU_CART_ADDED = "[Menu] Menu cart added to order",

  MENU_ORDER_ITEM_UPDATED = "[Menu] Menu order item updated",
}

interface setMenuItem {
  type: MENU_ACTIONS.SET_MENU_ITEM;
  payload: MenuDto;
}

interface resetMenuCart {
  type: MENU_ACTIONS.RESET_MENU_CART;
}

interface fetchMenu {
  type: MENU_ACTIONS.FETCH_MENU;
}
interface requestSuccess {
  type: MENU_ACTIONS.REQUEST_SUCCESS;
}
interface requestFailed {
  type: MENU_ACTIONS.REQUEST_FAILED;
}

interface menuUpdated {
  type: MENU_ACTIONS.MENU_UPDATED;
  payload: MenuDto[];
}

interface setOrder {
  type: MENU_ACTIONS.SET_ORDER;
  payload: OrderDto;
}

interface setMenuOrderItem {
  type: MENU_ACTIONS.MENU_CART_ITEM_UPDATED;
  payload: OrderItemDto;
}
export type MenuDispatchType =
  | setMenuItem
  | resetMenuCart
  | menuUpdated
  | setOrder
  | requestSuccess
  | requestFailed
  | fetchMenu
  | setMenuOrderItem;

// MENU
export const fetchMenu = () => {
  return async (dispatch: Dispatch<any>, getState: any) => {
    dispatch({ type: MENU_ACTIONS.FETCH_MENU });
    try {
      const menus: MenuDto[] = await (
        await MenuService.getMenus(
          getState().restaurant.restaurantMeta.restaurantId
        )
      ).data;

      dispatch({
        type: MENU_ACTIONS.MENU_UPDATED,
        payload: menus,
      });

      dispatch({
        type: MENU_ACTIONS.REQUEST_SUCCESS,
      });
    } catch {
      dispatch({
        type: MENU_ACTIONS.REQUEST_FAILED,
      });
    }
  };
};

export const addCartMenuToEditOrder = (
  currentEditOrder: OrderDto,
  menuCart: OrderDto
) => {
  return (dispatch: Dispatch<any>) => {
    dispatch({
      type: MENU_ACTIONS.ADD_MENU_CART_TO_ORDER,
    });

    const mergedOrder: OrderDto = OrderUtil.createInsertTranOrder(
      currentEditOrder,
      menuCart
    );

    dispatch(editOrder(mergedOrder));
    dispatch({
      type: MENU_ACTIONS.MENU_CART_ADDED,
    });
    dispatch({
      type: MENU_ACTIONS.RESET_MENU_CART,
    });
  };
};

export const setMenuOrder = (order: OrderDto) => {
  return (dispatch: Dispatch<MenuDispatchType>) => {
    dispatch({
      type: MENU_ACTIONS.SET_ORDER,
      payload: order,
    });
  };
};

export const setMenuOrderItem = (orderItem: OrderItemDto) => {
  return (dispatch: Dispatch<any>) => {
    dispatch({
      type: MENU_ACTIONS.UPDATE_MENU_CART_ITEM,
    });
    dispatch({
      type: MENU_ACTIONS.MENU_CART_ITEM_UPDATED,
      payload: orderItem,
    });
  };
};

const getInitCart = (): OrderDto => {
  const cachedOrder: OrderDto = BrowserCacheUtil.get(CASH_KEYS.MENU_CART);
  return cachedOrder !== null ? cachedOrder : OrderUtil.getMenuInitOrder();
};

const getInitMenu = (): MenuDto[] => {
  const cachedMenu: MenuDto[] = BrowserCacheUtil.get(CASH_KEYS.MENU);
  return cachedMenu !== null ? cachedMenu : ([] as MenuDto[]);
};

export interface MenuStateI {
  menuItems: MenuDto[];
  menuCart: OrderDto;
  loading: boolean;
}

const defaultState: MenuStateI = {
  menuItems: getInitMenu(),
  menuCart: getInitCart(),
  loading: false,
};

export const menuReducer = (
  state: MenuStateI = defaultState,
  action: MenuDispatchType
): MenuStateI => {
  switch (action.type) {
    case MENU_ACTIONS.FETCH_MENU:
      return {
        ...state,
        loading: true,
      };
    case MENU_ACTIONS.REQUEST_SUCCESS:
    case MENU_ACTIONS.REQUEST_FAILED:
      return {
        ...state,
        loading: false,
      };
    case MENU_ACTIONS.MENU_UPDATED:
      BrowserCacheUtil.putMenu(JSON.stringify(action.payload));
      return {
        ...state,
        menuItems: action.payload,
      };
      break;
    case MENU_ACTIONS.SET_MENU_ITEM:
      if (state.menuItems) {
        const targetIdx = state.menuItems.findIndex(
          ({ menuId }) => menuId === action.payload.menuId
        );
        const update = [...state.menuItems];
        update[targetIdx] = action.payload;
        return { ...state, menuItems: update };
      } else return state;

    case MENU_ACTIONS.RESET_MENU_CART:
      return {
        ...state,
        menuCart: OrderUtil.getInitOrder(),
      };

    case MENU_ACTIONS.MENU_CART_ITEM_UPDATED:
      {
        const newOrder = _.cloneDeep(state.menuCart);

        const targetIdx = newOrder.items.findIndex(
          ({ itemId }) => itemId === action.payload.itemId
        );

        if (targetIdx === -1) newOrder.items.push(action.payload);
        else {
          newOrder.items[targetIdx] = action.payload;
        }

        // filter Items
        // Remove items with 0 quantity
        OrderUtil.filterItem(newOrder);

        // Update total cost
        newOrder.cost = OrderUtil.getCost(newOrder);
        // BrowserCacheUtil.putOrder(JSON.stringify(newOrder));
        BrowserCacheUtil.put(JSON.stringify(newOrder), CASH_KEYS.MENU_CART, 10);
        return {
          ...state,
          menuCart: newOrder,
        };
      }
      break;
    default:
      return state;
  }
};
