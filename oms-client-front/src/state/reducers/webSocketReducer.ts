import { Dispatch } from "react";
import { OrderService } from "../../api/order-service";
import { OrderEvent } from "../../models/event";
import { store } from "../store";

// Websocket Meta Information
// 0 closed
// 1 Connecting
// 2 Connected
export enum WEBSOCKET_STATE {
  DISCONNECTED = 0,
  CONNECTING = 1,
  CONNECTED = 2,
}
export enum WEBSOCKET_ACTIONS {
  UPDATE_WEBSOCKET_STATE = "[WebSocket] Update connection state",
  SENDING_MESSAGE = "[WebSocket] Sending Message",
  SENT_MESSAGE = "[WebSocket] Message Sent",
  SET_WEBSOCKET = "[WebSocket] set websocket",
  SET_PENDING_NOTIFICATION = "[WebSocket] Set pending notification",
  SET_PENDING_NOTIFICATIONS = "[WebSocket] Set pending notifications",
}

interface sendingMessage {
  type: WEBSOCKET_ACTIONS.SENDING_MESSAGE;
}

interface messageSent {
  type: WEBSOCKET_ACTIONS.SENT_MESSAGE;
}

interface setWebSocketState {
  type: WEBSOCKET_ACTIONS.UPDATE_WEBSOCKET_STATE;
  payload: number;
}
interface setWebSocket {
  type: WEBSOCKET_ACTIONS.SET_WEBSOCKET;
  payload: WebSocket;
}
interface setPendingNotification {
  type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATION;
  payload: OrderEvent;
}
interface setPendingNotifications {
  type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATIONS;
  payload: OrderEvent[];
}
export type WebSocketDispatchType =
  | setWebSocketState
  | setWebSocket
  | setPendingNotification
  | setPendingNotifications
  | sendingMessage
  | messageSent;

// Websocket

export const sendMessage = (message: OrderEvent) => {
  return (dispatch: Dispatch<any>, getState: any) => {
    let gws = getState().webSocket.gsw;
    dispatch({ type: WEBSOCKET_ACTIONS.SENDING_MESSAGE });
    if (!gws || gws.readyState > 1) {
      console.log("food order setting websocket to disconnect" + new Date());
      dispatch(setWebSocketState(WEBSOCKET_STATE.DISCONNECTED));
      console.log("food order setting pending notification" + new Date());
      dispatch(setPendingNotification(message));
    } else if (gws.readyState == 1) {
      OrderService.postNotification(gws, message);
      dispatch({ type: WEBSOCKET_ACTIONS.SENT_MESSAGE });
    }
  };
};

export const sentNotification = () => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.SENT_MESSAGE,
    });
  };
};

export const setWebSocketState = (state: number) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.UPDATE_WEBSOCKET_STATE,
      payload: state,
    });
  };
};

export const setWebSocket = (ws: WebSocket) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.SET_WEBSOCKET,
      payload: ws,
    });
  };
};

export const setPendingNotification = (notification: OrderEvent) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATION,
      payload: notification,
    });
  };
};

export const setPendingNotifications = (notifications: OrderEvent[]) => {
  return (dispatch: Dispatch<WebSocketDispatchType>) => {
    dispatch({
      type: WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATIONS,
      payload: notifications,
    });
  };
};

export const WS_BASE =
  "wss://8irvmkg0qk.execute-api.us-east-2.amazonaws.com/dev?restaurantId=1629791235991";
export interface WebSocketStateI {
  webSocketState: number;
  gsw: WebSocket;
  pendingNotif: OrderEvent[];
}

const defaultState: WebSocketStateI = {
  webSocketState: WEBSOCKET_STATE.CONNECTING,
  gsw: new WebSocket(WS_BASE),
  pendingNotif: [],
};

export const webSocketReducer = (
  state: WebSocketStateI = defaultState,
  action: WebSocketDispatchType
): WebSocketStateI => {
  switch (action.type) {
    case WEBSOCKET_ACTIONS.UPDATE_WEBSOCKET_STATE:
      return {
        ...state,
        webSocketState: action.payload,
      };
      break;
    case WEBSOCKET_ACTIONS.SET_WEBSOCKET:
      return {
        ...state,
        gsw: action.payload,
      };
      break;
    case WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATION:
      return {
        ...state,
        pendingNotif: [...state.pendingNotif, action.payload],
      };
      break;
    case WEBSOCKET_ACTIONS.SET_PENDING_NOTIFICATIONS:
      const notification = [...action.payload];
      return {
        ...state,
        pendingNotif: notification,
      };
      break;
    default:
      return state;
  }
};
