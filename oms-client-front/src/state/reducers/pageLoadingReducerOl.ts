import { Dispatch } from "react";

export enum PAGE_DEF {
  TABLE_ORDER = "TABLE_ORDER",
  ALL_ORDER = "ALL_ORDER",
  TABLE_VIEW = "TABLE_VIEW",
}

export enum PAGE_LOADING_ACTIONS {
  SET_PAGE_LOADING = "[ UI ] Page Loading",
  SET_PAGE_LOADED = "[ UI ] Page Loaded",
}
interface setPageLoadingState {
  type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADING;
  payload: PAGE_DEF;
}
interface setPageLoadedState {
  type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADED;
  payload: PAGE_DEF;
}

export type PageLoadingDispatchType = setPageLoadingState | setPageLoadedState;

// PageLoading
export const setDataLoading = (loading: boolean) => {
  if (loading)
    return (dispatch: Dispatch<PageLoadingDispatchType>) => {
      dispatch({
        type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADING,
        payload: PAGE_DEF.ALL_ORDER,
      });
    };
  else
    return (dispatch: Dispatch<PageLoadingDispatchType>) => {
      dispatch({
        type: PAGE_LOADING_ACTIONS.SET_PAGE_LOADED,
        payload: PAGE_DEF.ALL_ORDER,
      });
    };
};

export interface PageLoadingStateI {
  tableOrderLoading: boolean;
  allOrderLoading: boolean;
  tablePageLoading: boolean;
}

const defaultState: PageLoadingStateI = {
  tableOrderLoading: false,
  allOrderLoading: false,
  tablePageLoading: false,
};

export const pageLoadingReducer = (
  state: PageLoadingStateI = defaultState,
  action: PageLoadingDispatchType
): PageLoadingStateI => {
  switch (action.type) {
    case PAGE_LOADING_ACTIONS.SET_PAGE_LOADING:
      {
        const newState = { ...state };
        switch (action.payload) {
          case PAGE_DEF.ALL_ORDER:
            newState.allOrderLoading = true;
            break;
          case PAGE_DEF.TABLE_ORDER:
            newState.tableOrderLoading = true;
            break;
          case PAGE_DEF.TABLE_VIEW:
            newState.tablePageLoading = true;
            break;
        }
        return newState;
      }
      break;

    case PAGE_LOADING_ACTIONS.SET_PAGE_LOADED:
      {
        const newState = { ...state };
        switch (action.payload) {
          case PAGE_DEF.ALL_ORDER:
            newState.allOrderLoading = false;
            break;
          case PAGE_DEF.TABLE_ORDER:
            newState.tableOrderLoading = false;
            break;
          case PAGE_DEF.TABLE_VIEW:
            newState.tablePageLoading = false;
            break;
        }
        return newState;
      }
      break;
    default:
      return state;
  }
};
