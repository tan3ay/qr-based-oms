import { Dispatch } from "react";
import { OrderService } from "../../api/order-service";
import { OrderUtil } from "../../helper/OrderUtil";
import { EventData, EVENT_TYPE, OrderEvent } from "../../models/event";
import {
  KitchenStatus,
  OrderDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../../models/order";
import { sendMessage } from "./webSocketReducer";

/*
 TODO MERGE REMOVE UPDATE_ORDER & ORDER_FINISH & change it to ORDER_STATUS_UPDATE
 */
export enum ORDER_ACTIONS {
  // Common order actions
  REQUEST_SUCCESS = "[Order] Request Success",
  REQUEST_FAILED = "[Order] Request Failed",
  ORDER_UPDATED = "[Order] Order Updated",
  ORDERS_UPDATED = "[Order] Orders updated",
  ORDER_ADDED = "[Order] Order Added",
  ORDERS_DELETED = "[Order] Orders Deleted",
  ORDER_MERGED = " [Order] Orders merged",

  FETCH_ORDERS = "[Order] Fetching all orders",
  UPDATE_ORDER = "[Order] Updating order",
  ADD_ORDER = "[Order] Add new order",
  DELETE_ORDERS = "[Order] Delete orders",
  MERGE_ORDERS = "[Order] Merging Orders",

  // Kitchen Order Action
  START_ORDER = "[Kitchen Order] Started",
  KITCHEN_ORDER_SERVED = "[Kitchen Order] Served",
  KITCHEN_ORDER_UNDO_SERVE = "[Kitchen Order] Unserve served order",
  MENU_ITEM_STATUS_UPDATE = "[Kitchen Order] Item Status Updated",
  ORDER_FINISH = "[Kitchen Order] Finished",

  // Payment Order Action
  PAYMENT_ORDER_PAID = "[Payment Order] Paid",
  PAYMENT_ORDER_UNDO_PAID = "[Payment Order] Unpaid paid order",

  EDIT_ORDER = "[Edit Order] Editing order",
  SAVE_ORDER = "[Edit Order] Saving order",
  ORDER_SAVED = "[Edit Order] Order Saved",
}

interface RequestOrders {
  type: ORDER_ACTIONS.FETCH_ORDERS;
}

interface AddOrder {
  type: ORDER_ACTIONS.ADD_ORDER;
}
interface DeleteOrders {
  type: ORDER_ACTIONS.DELETE_ORDERS;
}

interface OrdersLoaded {
  type: ORDER_ACTIONS.ORDERS_UPDATED;
  payload: OrderDto[];
}

interface OrdersAdded {
  type: ORDER_ACTIONS.ORDER_ADDED;
  payload: OrderDto;
}

interface OrdersDeleted {
  type: ORDER_ACTIONS.ORDERS_DELETED;
  payload: OrderDto[];
}

interface OrderLoaded {
  type: ORDER_ACTIONS.ORDER_UPDATED;
  payload: OrderDto;
}

interface OrderLoadFailed {
  type: ORDER_ACTIONS.REQUEST_FAILED;
  payload: OrderDto[];
}

interface OrderRequestSuccess {
  type: ORDER_ACTIONS.REQUEST_SUCCESS;
}

interface OrderServed {
  type: ORDER_ACTIONS.KITCHEN_ORDER_SERVED;
  // payload: { order: OrderDto; kitchenId: string };
}

interface OrderUnServed {
  type: ORDER_ACTIONS.KITCHEN_ORDER_UNDO_SERVE;
  // payload: { order: OrderDto; kitchenId: string };
}

interface OrderUpdated {
  type: ORDER_ACTIONS.ORDER_UPDATED;
  payload: OrderDto;
}

interface UpdateOrder {
  type: ORDER_ACTIONS.UPDATE_ORDER;
  payload: OrderDto;
}

interface StartOrder {
  type: ORDER_ACTIONS.START_ORDER;
  payload: OrderDto;
}

interface MenuItemStatusUpdate {
  type: ORDER_ACTIONS.MENU_ITEM_STATUS_UPDATE;
  payload: { order: OrderDto; itemId: string; status: ORDER_ITEM_STATUS };
}

interface OrderFinish {
  type: ORDER_ACTIONS.ORDER_FINISH;
  payload: OrderDto;
}

export type KitchenOrderDispatchTypes =
  | RequestOrders
  | AddOrder
  | DeleteOrders
  | UpdateOrder
  | OrderLoaded
  | OrdersDeleted
  | OrdersAdded
  | OrderUpdated
  | OrderServed
  | OrderUnServed
  | StartOrder
  | MenuItemStatusUpdate
  | OrderFinish
  | OrderLoadFailed
  | OrderRequestSuccess
  | OrdersLoaded;

export const fetchKitchenOrders = (restaurantId: string) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ORDER_ACTIONS.FETCH_ORDERS });
    try {
      const allOrders: OrderDto[] = await OrderService.getTodaysOrders(
        restaurantId
      );
      dispatch({
        type: ORDER_ACTIONS.ORDERS_UPDATED,
        payload: allOrders,
      });

      dispatch({
        type: ORDER_ACTIONS.REQUEST_SUCCESS,
      });
    } catch {
      dispatch({
        type: ORDER_ACTIONS.REQUEST_FAILED,
      });
    }
  };
};

export const startOrder = (order: OrderDto, kitchenId: string) => {
  return async (dispatch: Dispatch<any>, getState: any) => {
    const newOrder = order;
    updateKitchenStatus(order, kitchenId, ORDER_STATUS.PREPARING);
    updateKitchenItemStatus(order, kitchenId, ORDER_ITEM_STATUS.PREPARING);
    await dispatch(updateOrder(order));
    dispatch({
      type: ORDER_ACTIONS.START_ORDER,
    });

    const restaurantId = getState().restaurant.restaurantMeta?.restaurantId!;
    const updateMessage = createNotification(
      restaurantId,
      order,
      EVENT_TYPE.ORDER_STATUS_UPDATED
    );
    dispatch(sendMessage(updateMessage));
  };
};

export const undoOrder = (order: OrderDto, kitchenId: string) => {
  return async (dispatch: Dispatch<any>, getState: any) => {
    const newOrder = order;
    updateKitchenStatus(order, kitchenId, ORDER_STATUS.PREPARING);
    await dispatch(updateOrder(order));
    dispatch({
      type: ORDER_ACTIONS.KITCHEN_ORDER_UNDO_SERVE,
    });
    const restaurantId = getState().restaurant.restaurantMeta.restaurantId!;
    const updateMessage = createNotification(
      restaurantId,
      order,
      EVENT_TYPE.ORDER_STATUS_UPDATED
    );
    dispatch(sendMessage(updateMessage));
  };
};

export const orderServed = (order: OrderDto, kitchenId: string) => {
  return async (dispatch: Dispatch<any>, getState: any) => {
    const newOrder = order;
    updateKitchenStatus(order, kitchenId, ORDER_STATUS.SERVED);
    updateKitchenItemStatus(order, kitchenId, ORDER_ITEM_STATUS.SERVED);
    await dispatch(updateOrder(order));
    dispatch({
      type: ORDER_ACTIONS.KITCHEN_ORDER_SERVED,
    });
    const restaurantId = getState().restaurant.restaurantMeta?.restaurantId!;
    const updateMessage = createNotification(
      restaurantId,
      order,
      EVENT_TYPE.ORDER_STATUS_UPDATED
    );
    dispatch(sendMessage(updateMessage));
  };
};

export const payOrder = (order: OrderDto) => {
  return async (dispatch: Dispatch<any>, getState: any) => {
    const newOrder = order;

    newOrder.kitchenStatus.forEach((item) => {
      item.status = ORDER_STATUS.PAID;
    });

    newOrder.items.forEach((item) => {
      if (item.itemStatus !== ORDER_ITEM_STATUS.DELETED)
        item.itemStatus = ORDER_ITEM_STATUS.SERVED;
    });

    order.status = ORDER_STATUS.PAID;

    await dispatch(updateOrder(order));
    dispatch({
      type: ORDER_ACTIONS.PAYMENT_ORDER_PAID,
    });
    const restaurantId = getState().restaurant.restaurantMeta.restaurantId!;
    const updateMessage = createNotification(
      restaurantId,
      order,
      EVENT_TYPE.ORDER_STATUS_UPDATED
    );
    dispatch(sendMessage(updateMessage));
  };
};

export const undoPaidOrder = (order: OrderDto) => {
  return async (dispatch: Dispatch<any>, getState: any) => {
    const newOrder = order;

    newOrder.kitchenStatus.forEach((item) => {
      item.status = ORDER_STATUS.SERVED;
    });

    newOrder.items.forEach((item) => {
      if (item.itemStatus !== ORDER_ITEM_STATUS.DELETED)
        item.itemStatus = ORDER_ITEM_STATUS.SERVED;
    });

    order.status = ORDER_STATUS.SERVED;

    await dispatch(updateOrder(order));
    dispatch({
      type: ORDER_ACTIONS.PAYMENT_ORDER_UNDO_PAID,
    });
    const restaurantId = getState().restaurant.restaurantMeta.restaurantId!;
    const updateMessage = createNotification(
      restaurantId,
      order,
      EVENT_TYPE.ORDER_STATUS_UPDATED
    );
    dispatch(sendMessage(updateMessage));
  };
};

export const updateOrder = (order: OrderDto) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ORDER_ACTIONS.UPDATE_ORDER });
    try {
      const res = await OrderService.putOrder(order);

      dispatch({
        type: ORDER_ACTIONS.ORDER_UPDATED,
        payload: order,
      });

      dispatch({
        type: ORDER_ACTIONS.REQUEST_SUCCESS,
      });
    } catch {
      dispatch({
        type: ORDER_ACTIONS.REQUEST_FAILED,
      });
    }
  };
};

export const mergeOrders = (
  selectedOrders: string[],
  pendingOrders: OrderDto[]
) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ORDER_ACTIONS.MERGE_ORDERS });
    try {
      const mergeTargets = OrderUtil.updateOrderStatus(
        pendingOrders,
        selectedOrders,
        ORDER_STATUS.DELETED
      );
      const mergedOrder = OrderUtil.mergeOrdersByOrderId(
        mergeTargets,
        selectedOrders
      );
      try {
        // New Order

        await dispatch(addOrder(mergedOrder));
        // dispatch(addPaymentOrder(JSON.parse(JSON.stringify(data.Item))));

        // Remove Old order
        await dispatch(deleteOrders(mergeTargets));

        // await OrderService.putOrders(mergeTarget);
        // mergeTarget.forEach((order) => dispatch(deletePaymentOrder(order)));
        // dispatch(de(mergedOrder));

        // TODO Send Notification
        // OrderService.postNewOrderNotification(
        //   restaurantMeta.restaurantId,
        //   gws,
        //   mergedOrder
        // );
      } catch (e) {
        console.error("ERROR Problem in updatign merged order", e);
      }
      dispatch({
        type: ORDER_ACTIONS.ORDER_MERGED,
      });
    } catch {
      console.log("Failed to merge Orders");
    }
  };
};

export const addOrder = (order: OrderDto) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ORDER_ACTIONS.ADD_ORDER });
    try {
      const postedOrder = (await OrderService.postOrder(order)).data;

      dispatch({
        type: ORDER_ACTIONS.ORDER_ADDED,
        payload: JSON.parse(JSON.stringify(postedOrder.Item)),
      });

      dispatch({
        type: ORDER_ACTIONS.REQUEST_SUCCESS,
      });
    } catch {
      dispatch({
        type: ORDER_ACTIONS.REQUEST_FAILED,
      });
    }
  };
};

export const deleteOrders = (orders: OrderDto[]) => {
  return async (dispatch: Dispatch<any>) => {
    dispatch({ type: ORDER_ACTIONS.DELETE_ORDERS });
    try {
      orders.forEach((order) => (order.status = ORDER_STATUS.DELETED));
      const res = await OrderService.putOrders(orders);

      dispatch({
        type: ORDER_ACTIONS.ORDERS_DELETED,
        payload: orders,
      });

      dispatch({
        type: ORDER_ACTIONS.REQUEST_SUCCESS,
      });
    } catch {
      dispatch({
        type: ORDER_ACTIONS.REQUEST_FAILED,
      });
    }
  };
};

export const finishOrder = (order: OrderDto) => {
  return (dispatch: Dispatch<KitchenOrderDispatchTypes>) => {
    dispatch({
      type: ORDER_ACTIONS.ORDER_FINISH,
      payload: order,
    });
  };
};

export const itemStatusUpdate = (
  order: OrderDto,
  itemId: string,
  itemStatus: ORDER_ITEM_STATUS
) => {
  return (dispatch: Dispatch<KitchenOrderDispatchTypes>) => {
    dispatch({
      type: ORDER_ACTIONS.MENU_ITEM_STATUS_UPDATE,
      payload: { order: order, itemId: itemId, status: itemStatus },
    });
  };
};

export interface OrderStateI {
  loading: boolean;
  orders: OrderDto[];
}

const defaultState: OrderStateI = {
  loading: false,
  orders: [],
};

export const orderReducer = (
  state: OrderStateI = defaultState,
  action: KitchenOrderDispatchTypes
): OrderStateI => {
  switch (action.type) {
    case ORDER_ACTIONS.FETCH_ORDERS:
    case ORDER_ACTIONS.UPDATE_ORDER:
    case ORDER_ACTIONS.DELETE_ORDERS:
    case ORDER_ACTIONS.ADD_ORDER:
      return { ...state, loading: true };

    case ORDER_ACTIONS.REQUEST_SUCCESS:
    case ORDER_ACTIONS.REQUEST_FAILED:
      console.log("Order Success");
      return { ...state, loading: false };

    case ORDER_ACTIONS.ORDERS_UPDATED:
      return { ...state, orders: action.payload };

    case ORDER_ACTIONS.ORDERS_DELETED: {
      let deleteOrdersIds = new Set();
      action.payload.forEach((order) => deleteOrdersIds.add(order.orderId));
      const newOrders = state.orders
        .filter((order) => !deleteOrdersIds.has(order.orderId))
        .map((order) => order);
      return { ...state, orders: newOrders };
    }

    case ORDER_ACTIONS.ORDER_UPDATED: {
      const targetIndex = state.orders.findIndex(
        (item) => item.orderId === action.payload.orderId
      );
      return {
        ...state,
        orders: state.orders.map((el, index) =>
          index == targetIndex ? action.payload : el
        ),
        loading: false,
      };
    }

    case ORDER_ACTIONS.MENU_ITEM_STATUS_UPDATE: {
      const newOrders = [...state.orders];
      const targetOrder = action.payload.order;
      const targetItem = targetOrder.items.find(
        (item) => item.itemId === action.payload.itemId
      );
      if (targetItem) {
        targetItem.itemStatus = action.payload.status;
        const index = state.orders.findIndex(
          (item) => item.orderId === targetOrder.orderId
        );
        newOrders[index] = targetOrder;
        return { ...state, orders: newOrders };
      } else {
      }
      return { ...state, orders: newOrders };
    }

    case ORDER_ACTIONS.ORDER_ADDED: {
      console.log(JSON.stringify(state.orders));
      console.log("--");
      console.log(JSON.stringify([...state.orders, action.payload]));
      const newOrders = [...state.orders, action.payload];
      return { ...state, orders: newOrders };
    }

    default:
      return state;
  }
};

const updateKitchenStatus = (
  order: OrderDto,
  kitchenId: string,
  staus: ORDER_STATUS
) => {
  order.kitchenStatus.forEach((kitchen: KitchenStatus) => {
    if (kitchen.kitchenId === kitchenId) kitchen.status = staus;
  });
};

const updateKitchenItemStatus = (
  order: OrderDto,
  kitchenId: string,
  status: ORDER_ITEM_STATUS
) => {
  order.items.forEach((item) => {
    if (
      item.kitchenId === kitchenId &&
      item.itemStatus !== ORDER_ITEM_STATUS.SERVED &&
      item.itemStatus !== ORDER_ITEM_STATUS.DELETED
    ) {
      item.itemStatus = status;
    }
  });
};

const createNotification = (
  restaurantId: string,
  order: OrderDto,
  event_type: string
) => {
  let targetKitchens = new Set<string>();
  order.items.forEach((item) => {
    if (
      item.itemStatus !== ORDER_ITEM_STATUS.DELETED &&
      item.itemStatus < ORDER_ITEM_STATUS.SERVED
    )
      targetKitchens.add(item.kitchenId);
  });

  const notifications: EventData[] = Array.from(targetKitchens).map(
    (kitchenId) => {
      return {
        type: "Info",
        target: "kitchen",
        targetId: kitchenId,
      };
    }
  );
  const event: OrderEvent = {
    restaurantId: restaurantId,
    type: event_type,
    data: JSON.stringify(notifications),
  };
  return event;
};
