import { isEmpty } from "lodash";
import { Dispatch } from "react";
import { RestaurantService } from "../../api/restaurant-service";
import { BrowserCacheUtil } from "../../helper/BrowserCacheUtil";
import { RestaurantDto } from "../../models/restaurantMeta";

// Restaurant Meta Information
export enum RESTAURANT_ACTIONS {
  REQUEST_SUCCESS = "[Restaurant Meta] Request Success",
  REQUEST_FAILED = "[Restaurant Meta] Request Failed",
  FETCH_RESTAURANT_META = "[Restaurant Meta] Fetching restaurant meta if not present",
  RESTAURANT_META_UPDATED = "[Restaurant Meta] Updated.",
  CLEAR_RESTAURANT_META = "[Restaurant Meta] Clear restaurant meta",
}

interface updatingRestaurantMeta {
  type: RESTAURANT_ACTIONS.FETCH_RESTAURANT_META;
}
interface restaruntMetaUpdated {
  type: RESTAURANT_ACTIONS.RESTAURANT_META_UPDATED;
  payload: RestaurantDto;
}
interface restaurantRequestFailed {
  type: RESTAURANT_ACTIONS.REQUEST_FAILED;
}
interface restaurantRequestSucceed {
  type: RESTAURANT_ACTIONS.REQUEST_SUCCESS;
}

export type RestaurantDispatchType =
  | updatingRestaurantMeta
  | restaruntMetaUpdated
  | restaurantRequestFailed
  | restaurantRequestSucceed;

export const fetchRestaurantMeta = (restaurantId: string) => {
  return async (dispatch: Dispatch<RestaurantDispatchType>, getState: any) => {
    if (isEmpty(getState().restaurant.restaurantMeta)) {
      try {
        console.log(getState().restaurant.restaurantMeta, "rr");

        dispatch({ type: RESTAURANT_ACTIONS.FETCH_RESTAURANT_META });
        const res = await RestaurantService.getRestaurantData(restaurantId);

        BrowserCacheUtil.put(res.data, "KEY_RESTAURANT", 15);
        dispatch({
          type: RESTAURANT_ACTIONS.RESTAURANT_META_UPDATED,
          payload: res.data,
        });

        dispatch({
          type: RESTAURANT_ACTIONS.REQUEST_SUCCESS,
        });
      } catch {
        dispatch({
          type: RESTAURANT_ACTIONS.REQUEST_FAILED,
        });
      }
    }
  };
};

export interface RestaurantStateI {
  restaurantMeta: RestaurantDto;
  loading: boolean;
}

const defaultState: RestaurantStateI = {
  restaurantMeta: {} as RestaurantDto,
  loading: false,
};

export const restaurantReducer = (
  state: RestaurantStateI = defaultState,
  action: RestaurantDispatchType
): RestaurantStateI => {
  switch (action.type) {
    case RESTAURANT_ACTIONS.FETCH_RESTAURANT_META:
      return { ...state, loading: true };
    case RESTAURANT_ACTIONS.REQUEST_SUCCESS:
    case RESTAURANT_ACTIONS.REQUEST_FAILED:
      return { ...state, loading: false };
      break;
    case RESTAURANT_ACTIONS.RESTAURANT_META_UPDATED:
      return {
        ...state,
        restaurantMeta: action.payload,
      };
      break;
    default:
      return state;
  }
};
