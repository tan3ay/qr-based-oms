import _ from "lodash";
import { Dispatch } from "react";
import { BrowserCacheUtil, CASH_KEYS } from "../../helper/BrowserCacheUtil";
import { OrderUtil } from "../../helper/OrderUtil";
import { EventData, EVENT_TYPE, OrderEvent } from "../../models/event";
import {
  OrderCostDto,
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../../models/order";
import { updateOrder } from "./orderReducer";
import { sendMessage } from "./webSocketReducer";

export enum EDIT_ORDER_ACTIONS {
  SAVE_ORDER = "[Edit Order] Saving Order",
  ORDER_SAVED = "[Edit Order] Order saved.",

  ORDER_ITEM_UPDATED = "[Edit Order] Order Item Updated",
  EDIT_ORDER = "[Edit Order] Editing Order", // start editing order
  FINISH_EDITING = "[Edit Order] Finish editing Order", // start editing order
}

interface editOrder {
  type: EDIT_ORDER_ACTIONS.EDIT_ORDER;
  payload: OrderDto;
}

interface finishEditingOrder {
  type: EDIT_ORDER_ACTIONS.FINISH_EDITING;
}

// used
interface editOrderItemUpdated {
  type: EDIT_ORDER_ACTIONS.ORDER_ITEM_UPDATED;
  payload: OrderItemDto;
}

interface saveOrder {
  type: EDIT_ORDER_ACTIONS.SAVE_ORDER;
}

interface orderSaved {
  type: EDIT_ORDER_ACTIONS.ORDER_SAVED;
}

export type EditOrderDispatchTypes =
  | editOrder
  | finishEditingOrder
  | editOrderItemUpdated
  | saveOrder
  | orderSaved;

// PAYMENT ORDER ACTION CREATORS
const createNotification = (
  restaurantId: string,
  order: OrderDto,
  oldOrder: OrderDto,
  event_type: string
) => {
  let targetKitchens = new Set<string>();
  let oldItems = new Set<string>();
  let updatedItems = new Set<string>();
  oldOrder.items.forEach((item) => oldItems.add(item.itemId));
  // Item with new item Ids are updated items
  order.items.forEach((item) => {
    //!item.isDeleted && item.itemStatus<ORDER_ITEM_STATUS.SERVED||
    if (!oldItems.has(item.itemId)) {
      targetKitchens.add(item.kitchenId);
      updatedItems.add(item.itemId);
    }
  });

  const notifications: EventData[] = Array.from(targetKitchens).map(
    (kitchenId) => {
      return {
        type: "Info",
        target: "kitchen",
        targetId: kitchenId,
      };
    }
  );
  const event: OrderEvent = {
    restaurantId: restaurantId,
    type: event_type,
    data: JSON.stringify(notifications),
  };
  return event;
};

export const saveEditOrder = (targetOrder: OrderDto) => {
  return async (dispatch: Dispatch<any>, getState: any) => {
    dispatch({
      type: EDIT_ORDER_ACTIONS.SAVE_ORDER,
    });

    const existingOrder = getState().orders.orders.find(
      (order: OrderDto) => order.orderId === targetOrder.orderId
    );
    const originalOrder = _.cloneDeep(existingOrder);
    const updatedOrder = _.cloneDeep(targetOrder);
    const upsertTranOrder = createUpsertTranOrder(originalOrder, updatedOrder);

    await dispatch(updateOrder(upsertTranOrder));

    dispatch({
      type: EDIT_ORDER_ACTIONS.ORDER_SAVED,
    });

    const restaurantId = getState().restaurant.restaurantMeta.restaurantId!;
    const updateMessage = createNotification(
      restaurantId,
      upsertTranOrder,
      originalOrder,
      EVENT_TYPE.ORDER_ITEM_UPDATE
    );
    dispatch(sendMessage(updateMessage));

    dispatch(finishEditingOrder());
  };
};

export const editOrderItemUpdated = (orderItem: OrderItemDto) => {
  return (dispatch: Dispatch<EditOrderDispatchTypes>) => {
    dispatch({
      type: EDIT_ORDER_ACTIONS.ORDER_ITEM_UPDATED,
      payload: orderItem,
    });
  };
};

export const editOrder = (order: OrderDto) => {
  return (dispatch: Dispatch<EditOrderDispatchTypes>) => {
    dispatch({
      type: EDIT_ORDER_ACTIONS.EDIT_ORDER,
      payload: order,
    });
  };
};

const finishEditingOrder = () => {
  return (dispatch: Dispatch<EditOrderDispatchTypes>) => {
    dispatch({
      type: EDIT_ORDER_ACTIONS.FINISH_EDITING,
    });
  };
};

export interface OrderStateI {
  editOrder: OrderDto;
}

const getEditOrder = () => {
  const cachedPaymentEditOrder: OrderDto = BrowserCacheUtil.get(
    CASH_KEYS.PAYMENT_EDIT_ORDER
  );
  return cachedPaymentEditOrder === null
    ? <OrderDto>{}
    : cachedPaymentEditOrder;
};

const defaultState: OrderStateI = {
  editOrder: getEditOrder(),
};

export const editOrderReducer = (
  state: OrderStateI = defaultState,
  action: EditOrderDispatchTypes
): OrderStateI => {
  // debugger
  switch (action.type) {
    case EDIT_ORDER_ACTIONS.EDIT_ORDER: {
      BrowserCacheUtil.put(
        JSON.stringify(action.payload),
        CASH_KEYS.PAYMENT_EDIT_ORDER,
        15
      );
      return { ...state, editOrder: action.payload };
    }
    case EDIT_ORDER_ACTIONS.FINISH_EDITING: {
      return { ...state, editOrder: <OrderDto>{} };
    }
    case EDIT_ORDER_ACTIONS.ORDER_ITEM_UPDATED: {
      const newItems: OrderItemDto[] = state.editOrder.items.map((el) =>
        el.itemId === action.payload.itemId ? action.payload : el
      );
      const newOrder: OrderDto = { ...state.editOrder, items: newItems };
      // filter Items
      // Remove items with 0 quantity
      OrderUtil.filterItem(newOrder);
      // Update total cost
      newOrder.cost = OrderUtil.getCost(newOrder);

      return { ...state, editOrder: newOrder };
    }
    default:
      return state;
  }
};

const printOrderItemStatus = (header: string, order: OrderDto) => {
  console.log(header + "Order Status");
  order.items.forEach((item) => {
    console.log(
      "id:" +
        item.itemId +
        " quantity:" +
        item.itemQuantity +
        " isDelete:" +
        (item.itemStatus === ORDER_ITEM_STATUS.DELETED)
    );
  });
};

const createUpsertTranOrder = (
  originalOrder: OrderDto,
  updatedOrder: OrderDto
): OrderDto => {
  let upsertOrderDto: OrderDto = _.cloneDeep(originalOrder);
  let updatedItems: Array<OrderItemDto> = [];
  let deletedItems: Array<OrderItemDto> = [];
  let unchangedItems: Array<OrderItemDto> = [];
  let insertedItem: Array<OrderItemDto> = [];
  // console.log('createUpsertTranOrder');
  // console.log('originalOrder',originalOrder.items);
  // console.log('updatedOrder',updatedOrder.items);
  originalOrder.items.forEach((originalItem) => {
    if (
      originalItem.itemStatus === ORDER_ITEM_STATUS.SERVED ||
      originalItem.itemStatus === ORDER_ITEM_STATUS.DELETED
    ) {
      unchangedItems.push(originalItem);
    } else {
      console.log("Checking for upsert");
      let updatedItem: OrderItemDto | undefined = getItemWithSameId(
        updatedOrder,
        originalItem.itemId
      );
      if (updatedItem) {
        if (updatedItem.itemQuantity === originalItem.itemQuantity) {
          console.log(
            "Item quantity in both order is same so should be unchanged"
          );
          unchangedItems.push(updatedItem);
        } else {
          console.log("Item quantity changed");
          deletedItems.push(originalItem);
          updatedItems.push(updatedItem);
        }
      } else {
        console.log("Item not found in new order list so should be deleted");
        deletedItems.push(originalItem);
      }
    }
  });

  updatedOrder.items.forEach((newItem) => {
    if (
      newItem.itemStatus !== ORDER_ITEM_STATUS.DELETED &&
      newItem.itemStatus !== ORDER_ITEM_STATUS.SERVED
    ) {
      const similarOldItem = getPendingSimilarItemInOrder(
        upsertOrderDto,
        newItem.itemId
      );
      if (similarOldItem) {
        console.log(
          "This is updated item do nothing since handled in previous loop"
        );
      } else {
        insertedItem.push(newItem);
      }
    }
  });

  let upsertItemList: Array<OrderItemDto> = [];

  updatedItems.forEach((item) => {
    item.itemId =
      item.itemId.replace("U", "") + "U" + new Date().getUTCSeconds();
    upsertItemList.push(item);
  });

  unchangedItems.forEach((item) => {
    upsertItemList.push(item);
  });

  let newKitchensIds = new Set<string>();
  insertedItem.forEach((item) => {
    item.itemId = item.itemId + "I" + new Date().getUTCSeconds();
    upsertItemList.push(item);
    newKitchensIds.add(item.kitchenId);
  });

  deletedItems.forEach((item) => {
    item.itemId =
      item.itemId.replace("D", "") + "D" + new Date().getUTCSeconds();
    //item.isDeleted=true;
    item.itemStatus = ORDER_ITEM_STATUS.DELETED;
    upsertItemList.push(item);
  });

  upsertOrderDto.items = sortItemListByStatus(upsertItemList);
  //const unservedItem = upsertOrderDto.items.find(item=>item.itemStatus=ORDER_ITEM_STATUS.PREPARING);

  // Add kitche for new Items if not present
  newKitchensIds.forEach((id) => {
    let isPresent: boolean = false;
    upsertOrderDto.kitchenStatus.forEach((kitchen) => {
      if (kitchen.kitchenId === id) isPresent = true;
    });
    if (!isPresent) {
      upsertOrderDto.kitchenStatus.push({
        kitchenId: id,
        status: ORDER_STATUS.PLACED,
      });
    }
  });

  // update kitchen status
  upsertOrderDto.items.forEach((item) => {
    if (item.itemStatus <= ORDER_ITEM_STATUS.PREPARING) {
      upsertOrderDto.kitchenStatus.forEach((kitchen) => {
        if (item.kitchenId === kitchen.kitchenId)
          kitchen.status = ORDER_STATUS.PLACED;
      });
    }
  });
  upsertOrderDto.status = getCombineOrderStatus(upsertOrderDto);
  upsertOrderDto.cost = getCost(upsertOrderDto);

  return upsertOrderDto;
};

const getCombineOrderStatus = (order: OrderDto): ORDER_STATUS => {
  // If all kitchen status x Order status x
  // If Any kitchen status < served Order status is Cooking
  let isAllKitchenItemStatusSame = true;
  let tempStatus: ORDER_STATUS = ORDER_STATUS.INVALID;
  order.kitchenStatus.forEach((item) => {
    if (tempStatus === ORDER_STATUS.INVALID) {
      tempStatus = item.status;
    } else if (tempStatus !== item.status) {
      isAllKitchenItemStatusSame = false;
    }
  });

  if (!isAllKitchenItemStatusSame) {
    return ORDER_STATUS.PREPARING;
  }
  return tempStatus;
};

const getCost = (order: OrderDto): OrderCostDto => {
  const TAX = 0;
  let subTotal: number = 0;
  order.items.forEach((item) => {
    if (item.itemStatus !== ORDER_ITEM_STATUS.DELETED)
      subTotal += item.itemQuantity * item.itemCost;
  });
  const gst = (subTotal * TAX) / 100;
  const totalCost = subTotal + gst;
  return {
    totalCost: totalCost,
    gst: gst,
    subTotal: subTotal,
  };
};

const getItemWithSameId = (
  order: OrderDto,
  itemId: string
): OrderItemDto | undefined => {
  return order.items.find((item) => {
    if (item.itemId === itemId) return true;
  });
};

const sortItemListByStatus = (list: OrderItemDto[]): OrderItemDto[] => {
  // list
  const newList: OrderItemDto[] = [];
  list.forEach((item) => {
    if (item.itemStatus !== ORDER_ITEM_STATUS.DELETED) newList.push(item);
  });
  newList.sort((item1, item2) => {
    return item1.itemStatus - item2.itemStatus;
  });
  list.forEach((item) => {
    if (item.itemStatus === ORDER_ITEM_STATUS.DELETED) newList.push(item);
  });

  return newList;
};

const getPendingSimilarItemInOrder = (
  order: OrderDto,
  itemId: string
): OrderItemDto | undefined => {
  return order.items.find((item) => {
    if (
      item.itemId.startsWith(itemId) &&
      item.itemStatus !== ORDER_ITEM_STATUS.SERVED &&
      item.itemStatus !== ORDER_ITEM_STATUS.DELETED
    )
      return true;
  });
};
