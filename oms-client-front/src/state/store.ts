import { createStore, applyMiddleware, compose } from "redux";

import thunk from "redux-thunk";
import { rootReducer } from "./reducers";
import { composeWithDevTools } from "redux-devtools-extension";

/*

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(
        reducers,
        {},
        composeEnhancers(applyMiddleware(thunk))
    );
*/

// For redux-inspect tool seeting
// const composeEnhancers =
//   process.env.NODE_ENV !== "production" &&
//   (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__
//     ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
//         name: "MyApp",
//         actionsBlacklist: ["REDUX_STORAGE_SAVE"],
//       })
//     : compose;
// const enhancer = composeEnhancers(
//   applyMiddleware(thunk)
//   // other store enhancers if any
// );
// export const store = createStore(rootReducer, enhancer);

export type RootState = ReturnType<typeof rootReducer>;

export const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(thunk)
    // other store enhancers if any
  )
);
