export interface KitchenDto{
    kitchenId:string,
    name:string,
    details:string
}

export interface TableDto{
    tableNo:string,
    noOfSeats:number
}
export interface RestaurantDto{
    restaurantId:string,
    name:string
    address:string, // currently not used
    city:string,
    country:string,
    language:string,
    currency:string,
    tables:TableDto[],
    kitchens:KitchenDto[]
    createdAt:number,
    updatedAt:number
}
