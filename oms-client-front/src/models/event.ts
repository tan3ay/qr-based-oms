import { OrderDto } from "./order";

export interface EventData {
  type: string;
  target: string;
  targetId: string;
  order?: OrderDto;
}

export enum EVENT_TYPE {
  ORDER_CREATED = "ORDER_CREATED",
  ORDER_STATUS_UPDATED = "ORDER_STATUS_UPDATED",
  ORDER_ITEM_UPDATE = "ORDER_ITEM_UPDATE",
  ORDER_PRINT = "ORDER_PRINT",
}

export interface OrderEvent {
  restaurantId: string;
  type: string;
  data: string;
}
