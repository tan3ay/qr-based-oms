// import { IChipProp } from "../components/chip-bar/chip-bar";


export enum SELECT_TYPE{
    SINGLE_SELECT="SINGLE_SELECT",
    MULTI_SELECT="MULTI_SELECT"
}

export interface MenuItemOption{
    optionId:string,
    optionName:string,
    optionCost:number
}


export interface MenuItemDto{
    itemId:string,
    itemName:string,
    itemType:number,
    maxSelectOptions:number,
    itemOptions:MenuItemOption[],
}

export interface MenuDto{
    restId:string, // partition
    menuKey:string
    menuId:string, // sort  
    name:string,
    desc:string,
    menuOrder:string,
    imgUrl:string,
    category:string,
    cost:number,
    quantity:number, // Can be removed
    kitchenId:string,
    characteristics:{[key: string]: number},
    isCustomizable:number
    customizeSettingType:number, // for enhancing customization feature
    customizeSettingData:{},
    items:MenuItemDto[]
}

export interface MenuLists{
    listName:string,
    menuList:MenuDto[]
  }

export interface Filters{
    categories:string[];
}

// export const menuFilters:IChipProp[] =[
//     {label:'Chef special',value:'isChefSpecial',selected:false},
//     {label:'Vegan',value:'isVegan',selected:false},
//     {label:'Vegetarian',value:'isVeg',selected:false},
//     {label:'Drinks',value:'isDrink',selected:false}
// ]

