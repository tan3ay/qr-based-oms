import { IChipProp } from "../components/chip-bar/chip-bar";
import { MenuDto, MenuItemDto, MenuItemOption } from "./menu";
import * as _ from "lodash";
import { ICheckList, ICheckListItem, ICheckListItemProp, ICheckListProp } from "../components/checked-list/check-list";
import { last } from "lodash";

export enum SELECT_TYPE{
    SINGLE_SELECT="SINGLE_SELECT",
    MULTI_SELECT="MULTI_SELECT"
}


export interface MenuDtoVo extends MenuDto{
    itemsVo?:ICheckList[]
}

export const cloneMenuDtoToVo=(menuDto:MenuDto|undefined):MenuDtoVo=>{
    if(!menuDto)
    return <MenuDtoVo>{};
    const complexMenuVo:MenuDtoVo=_.cloneDeep(menuDto);
    let listVo:ICheckList[]=[];
    complexMenuVo.items.forEach(item=>{
        let options:ICheckListItem[]=[];
        item.itemOptions.forEach(option=>{
            options.push({
                id:option.optionId,
                label:option.optionName,
                selected:false,
                cost:option.optionCost
            })
        });
        listVo.push({
            id:item.itemId,
            header:item.itemName,
            currentSelected:0,
            maxSelected:item.maxSelectOptions,
            options:options
        })
    })
    
    return {...complexMenuVo,itemsVo:listVo};
}



