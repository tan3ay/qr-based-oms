

export enum ORDER_STATUS{
    INVALID=-1,
    PLACED=1,
    PREPARING=2,
    SERVED=3,
    PAID=4,
    DELETED=5
  }
  
 export enum ORDER_ITEM_STATUS{
    PLACED= 1 ,
    PREPARING= 2,
    SERVED=3,
    DELETED=4
  }

  export interface KitchenStatus{
    kitchenId:string,
    status:number
  }

  // export interface CustomizationValue{
  //   header:string,
  //   detail:string
  // }

 export interface OrderItemDto{
    itemId:string,
    itemName:string,
    itemDetail:string,
    itemQuantity:number,
    itemCost:number,
    itemStatus:number,
    isCustomizable:number,
    kitchenId:string,
    //isDeleted:boolean,
    customizableValue?:{},// TODO need to think about this change
  }

  export interface OrderCostDto{
    totalCost:number,
    gst:number,
    subTotal:number
  }

 export interface OrderDto{
    restaurantId:string,
    orderKey:string,
    orderId:string
    status:number,
    tableId:string,
    cost:OrderCostDto,
    orderNotes:string,
    items:OrderItemDto[],
    kitchenStatus:KitchenStatus[],
    createdAt:number,
    updatedAt:number
  }


  export interface OrderLists{
    orderList:OrderDto[]
  }
