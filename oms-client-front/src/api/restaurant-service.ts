import { API } from "./base-api";

const RESTAURANT_ENDPOINT = "/restaurant";

export const RestaurantService = {
  async getRestaurantData(restaurantId: string) {
    const param = {
      restaurantId: restaurantId,
    };
    const res = {
      data: JSON.parse(
        '{"restaurantId":"1629791235991","name":"Bharati Restuarant","address":"Parc Borely","city":"Marseille","country":"France","language":"French","currency":"Euro","tables":[{"tableNo":"1","noOfSeats":-1},{"tableNo":"2","noOfSeats":-1},{"tableNo":"3","noOfSeats":-1},{"tableNo":"4","noOfSeats":-1},{"tableNo":"5","noOfSeats":-1},{"tableNo":"6","noOfSeats":-1},{"tableNo":"11","noOfSeats":-1},{"tableNo":"12","noOfSeats":-1},{"tableNo":"13","noOfSeats":-1},{"tableNo":"14","noOfSeats":-1},{"tableNo":"21","noOfSeats":-1},{"tableNo":"22","noOfSeats":-1},{"tableNo":"23","noOfSeats":-1},{"tableNo":"24","noOfSeats":-1},{"tableNo":"31","noOfSeats":-1},{"tableNo":"32","noOfSeats":-1},{"tableNo":"33","noOfSeats":-1},{"tableNo":"34","noOfSeats":-1},{"tableNo":"41","noOfSeats":-1},{"tableNo":"42","noOfSeats":-1},{"tableNo":"43","noOfSeats":-1},{"tableNo":"44","noOfSeats":-1},{"tableNo":"45","noOfSeats":-1},{"tableNo":"46","noOfSeats":-1},{"tableNo":"501","noOfSeats":-1},{"tableNo":"502","noOfSeats":-1},{"tableNo":"503","noOfSeats":-1},{"tableNo":"504","noOfSeats":-1},{"tableNo":"505","noOfSeats":-1},{"tableNo":"601","noOfSeats":-1},{"tableNo":"602","noOfSeats":-1},{"tableNo":"603","noOfSeats":-1},{"tableNo":"604","noOfSeats":-1},{"tableNo":"701","noOfSeats":-1},{"tableNo":"702","noOfSeats":-1},{"tableNo":"703","noOfSeats":-1},{"tableNo":"704","noOfSeats":-1},{"tableNo":"705","noOfSeats":-1},{"tableNo":"706","noOfSeats":-1}],"kitchens":[{"name":"Bar","kitchenId":"1","details":""},{"name":"Food Kitchen","kitchenId":"2","details":""}],"createdAt":1629791235992,"updatedAt":1629791235992}'
      ),
    };
    return res;
    //return API.makeGetRequest(RESTAURANT_ENDPOINT, param);
  },
};
