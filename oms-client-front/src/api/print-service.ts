import { API } from "./base-api";

const PRINT_ENDPOINT = "https://127.0.0.1:8433/printer/print";

export const PrintService = {
  async print() {
    return await API.postToPrinter(PRINT_ENDPOINT);
  },
};
