import axios from "axios";

export const API = {
  baseURL: "https://8z6p9yyd55.execute-api.us-east-2.amazonaws.com/dev",
  //baseURL:'https://8z6p9dsdasdayyd55.execute-api.us-east-2.amazonaws.com/dev',

  getURL(URL: string): string {
    return this.baseURL + URL;
  },

  async makeGetRequest(URL: string, param: any) {
    return await axios.get(this.getURL(URL), {
      params: param,
    });
  },

  async makePostRequest(URL: string, requestBody: any, headers?: any) {
    return await axios.post(this.getURL(URL), requestBody);
  },

  async makePutRequest(URL: string, requestBody: any, headers?: any) {
    return await axios.put(this.getURL(URL), requestBody);
  },

  async postToPrinter(url: string) {
    return await axios.get(url);
  },
};
