import { WS_BASE } from "../App";
import { OrderEvent, EventData, EVENT_TYPE } from "../models/event";
import {
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../models/order";
import { SearchVo } from "../pages/table-view-page";
import { API } from "./base-api";

const ORDERS_ENDPOINT = "/orders";
const ORDER_ENDPOINT = "/order";
export const OrderService = {
  async getTodaysOrders(restaurantId: string) {
    var startD = new Date();
    startD.setHours(0, 0, 0, 0);

    var endD = new Date();
    endD.setHours(23, 59, 59, 999);

    const start = startD.valueOf();
    const end = endD.valueOf();

    const param = {
      restaurantId: restaurantId,
      start: start,
      end: end,
    };

    const allOrders: OrderDto[] = (
      await API.makeGetRequest(ORDER_ENDPOINT, param)
    ).data;
    return allOrders.filter((order) => order.status !== ORDER_STATUS.DELETED);
  },
  postNewOrderNotification(
    restaurantId: string,
    ws: WebSocket,
    order: OrderDto
  ) {
    const notifications: EventData[] = order.kitchenStatus.map((kitchen) => {
      return {
        type: "Info",
        target: "kitchen",
        targetId: kitchen.kitchenId,
      };
    });
    const event: OrderEvent = {
      restaurantId: restaurantId,
      type: EVENT_TYPE.ORDER_CREATED,
      data: JSON.stringify(notifications),
    };
    if (!ws || (ws && ws.CLOSED == ws.readyState)) {
      ws = new WebSocket(WS_BASE);
      ws.onopen = () => {
        ws.send(JSON.stringify(event));
      };
    } else {
      ws.send(JSON.stringify(event));
    }
  },

  getPrintOrderNotification(
    restaurantId: string,
    order: OrderDto,
    event_type: string
  ) {
    const event: OrderEvent = {
      restaurantId: restaurantId,
      type: EVENT_TYPE.ORDER_CREATED,
      data: JSON.stringify(order),
    };
    return event;
  },

  getNotification(
    restaurantId: string,
    order: OrderDto,
    event_type: string,
    oldOrder?: OrderDto
  ): OrderEvent {
    let targetKitchens = new Set<string>();
    let oldItems = new Set<string>();
    let updatedItems = new Set<string>();
    console.log("old order", oldOrder);
    console.log("new order", order);
    // Find only updated items
    if (oldOrder && event_type === EVENT_TYPE.ORDER_ITEM_UPDATE) {
      oldOrder.items.forEach((item) => oldItems.add(item.itemId));
      // Item with new item Ids are updated items
      order.items.forEach((item) => {
        //!item.isDeleted && item.itemStatus<ORDER_ITEM_STATUS.SERVED||
        if (!oldItems.has(item.itemId)) {
          targetKitchens.add(item.kitchenId);
          updatedItems.add(item.itemId);
        }
      });
    } else if (oldOrder && event_type === EVENT_TYPE.ORDER_STATUS_UPDATED) {
      // This is only for use case where order is marked PAID from Order management while unserved
      // If item status marked changed to PAID check for the kitchens with unserved order and notify them
      oldOrder.items.forEach((item) => {
        if (
          item.itemStatus !== ORDER_ITEM_STATUS.DELETED &&
          item.itemStatus < ORDER_ITEM_STATUS.SERVED
        )
          targetKitchens.add(item.kitchenId);
      });
    } else {
      // TODO Remove this chunk
      order.items.forEach((item) => {
        if (
          item.itemStatus !== ORDER_ITEM_STATUS.DELETED &&
          item.itemStatus < ORDER_ITEM_STATUS.SERVED
        )
          targetKitchens.add(item.kitchenId);
      });
    }

    const notifications: EventData[] = Array.from(targetKitchens).map(
      (kitchenId) => {
        return {
          type: "Info",
          target: "kitchen",
          targetId: kitchenId,
        };
      }
    );
    const event: OrderEvent = {
      restaurantId: restaurantId,
      type: event_type,
      data: JSON.stringify(notifications),
    };
    return event;
  },

  postNotification(ws: WebSocket, event: OrderEvent) {
    try {
      ws.send(JSON.stringify({ action: "notify", data: event }));
    } catch (err) {
      console.error("ERROR Proble while sending update notification", err);
    }
  },

  async getOrders(restaurantId: string, orderId: string) {
    const param = {
      restaurantId: restaurantId,
      orderId: orderId,
    };
    return API.makeGetRequest(ORDERS_ENDPOINT, param);
  },

  async putOrder(order: OrderDto) {
    return await API.makePutRequest(ORDER_ENDPOINT, JSON.stringify(order));
  },

  async putOrders(orders: OrderDto[]) {
    orders.forEach(async (order) => {
      await API.makePutRequest(ORDER_ENDPOINT, JSON.stringify(order));
    });
    return "Success";
  },

  async updateOrderList(restaurantId: string) {
    const param = {
      restaurantId: restaurantId,
      ordersTill: ORDER_STATUS.PAID,
    };
    const allPendingOrders: OrderDto[] = await OrderService.getTodaysOrders(
      restaurantId
    );
    const kitchenOrder = allPendingOrders.filter(
      (order) => order.status < ORDER_STATUS.SERVED
    );
    return {
      kitchenOrder: kitchenOrder,
      paymentOrder: allPendingOrders,
    };
  },

  async getPendingKitchenOrders(restaurantId: string) {
    const param = {
      restaurantId: restaurantId,
      ordersTill: ORDER_STATUS.SERVED,
    };
    return API.makeGetRequest(ORDERS_ENDPOINT, param);
  },

  async getAllPendingPaymentOrders(restaurantId: string) {
    const param = {
      restaurantId: restaurantId,
      ordersTill: ORDER_STATUS.PAID,
    };
    return API.makeGetRequest(ORDERS_ENDPOINT, param);
  },

  async getPendingPaymentOrdersForTable(restaurantId: string, tableNo: string) {
    const todaysOrder: OrderDto[] = await OrderService.getTodaysOrders(
      restaurantId
    );
    return todaysOrder.filter((order) => order.tableId === tableNo);
  },

  async postOrder(order: OrderDto) {
    return await API.makePostRequest(ORDER_ENDPOINT, JSON.stringify(order));
  },

  async getOrder(restaurantId: string, orderId: string) {
    const param = {
      restaurantId: restaurantId,
      orderId: orderId,
    };
    return await API.makeGetRequest(ORDER_ENDPOINT, param);
  },
  filterById(orders: OrderDto[], keyword: string): OrderDto[] {
    if (keyword === "") {
      return orders;
    } else {
      let filteredMenuList = orders.filter((order: OrderDto) => {
        console.log(order.orderId.toLowerCase().indexOf(keyword.toLowerCase()));
        return order.orderId.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
      });
      console.log(orders);
      console.log(filteredMenuList);
      return filteredMenuList;
    }
  },

  filterOrdersByTableId(orders: OrderDto[], searchVo: SearchVo): OrderDto[] {
    const keyword = searchVo.key;
    const trueFilters = new Set<string>();

    searchVo.filters.forEach((item) => {
      if (item.selected) trueFilters.add(item.value);
    });

    // for filter status unpaid
    if (trueFilters.has("3")) {
      trueFilters.add("1");
      trueFilters.add("2");
    }
    if (trueFilters.has("5")) {
      trueFilters.add("1");
      trueFilters.add("2");
      trueFilters.add("3");
      trueFilters.add("4");
    }
    console.log(trueFilters);

    if (keyword !== "" || trueFilters.size > 0) {
      return orders.filter((item) => {
        let keyMatch = false,
          filterMatch = false;
        if (keyword == "") {
          keyMatch = true;
        } else if (item.tableId.toLowerCase() === keyword.toLowerCase())
          keyMatch = true;
        if (trueFilters.size === 0) {
          filterMatch = true;
        } else if (trueFilters.has(item.status + "")) {
          filterMatch = true;
        }
        return keyMatch && filterMatch;
      });
    }

    return orders;
  },
  filterOrdersByOrderId(orders: OrderDto[], searchVo: SearchVo): OrderDto[] {
    const keyword = searchVo.key;
    const trueFilters = new Set<string>();

    searchVo.filters.forEach((item) => {
      if (item.selected) trueFilters.add(item.value);
    });

    // for filter status unpaid
    if (trueFilters.has("3")) {
      trueFilters.add("2");
      trueFilters.add("1");
    }

    if (keyword !== "" || trueFilters.size > 0) {
      return orders.filter((item) => {
        let keyMatch = false,
          filterMatch = false;
        if (keyword == "") {
          keyMatch = true;
        } else if (
          item.orderId.toLowerCase().indexOf(keyword.toLowerCase()) > -1
        )
          keyMatch = true;
        if (trueFilters.size === 0) {
          filterMatch = true;
        } else if (trueFilters.has(item.status + "")) {
          filterMatch = true;
        }
        return keyMatch && filterMatch;
      });
    }

    return orders;
  },
};
