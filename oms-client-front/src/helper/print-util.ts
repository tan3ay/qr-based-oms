import { PrintService } from "../api/print-service";
import { OrderDto } from "../models/order";

export const PrintUtil={

    async printOrder(){
       const result = await PrintService.print();
       return result;
    }


}