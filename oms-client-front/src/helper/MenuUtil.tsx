import { MenuDto, MenuLists } from "../models/menu";

const MenuListService = {
    
    getMenuItems:():MenuDto[]=>{
       return [
            // {
            // restId:'1',
            // menuId:'1',
            // name:"Chicken Tikka",
            // desc:"Tender chicken cooked in thick onion gravy",
            // imgUrl:"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg",
            // quantity:0,
            // cost:13,
            // category:"Starters"
            // },{
            // restId:'1',
            // menuId:'2',
            // name:"Panner Tikka",
            // desc:"Tender panner cooked in thick onion gravy",
            // imgUrl:"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg",
            // quantity:0,
            // cost:10,
            // category:"Starters"
            // },{
            // restId:'1',
            // menuId:'3',
            // name:"Chicken Tikka",
            // desc:"Tender chicken cooked in thick onion gravy",
            // imgUrl:"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg",
            // quantity:0,
            // cost:13,
            // category:"Starters"
            // },{
            // restId:'1',
            // menuId:'4',
            // name:"Panner Tikka",
            // desc:"Tender panner cooked in thick onion gravy",
            // imgUrl:"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg",
            // quantity:0,
            // cost:10,
            // category:"Starters"
            // }
        ]
    },

    getMenus:():MenuDto[]=>{
      return JSON.parse('[{"restId":"1","menuKey":"menu#1628258959921","menuId":"1628258959921","name":"Paneer Tikka","desc":"Tender paneer cooked in think onion gravy","imgUrl":"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg","category":"Starters","cost":13,"characteristics":{},"quantity":0,"isCustomizable":0,"customizationSetting":null},{"restId":"1","menuKey":"menu#1628258988241","menuId":"1628258988241","name":"Chicken Tikka","desc":"Tender chicken cooked in think onion gravy","imgUrl":"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg","category":"Starters","cost":13,"characteristics":{},"quantity":0,"isCustomizable":0,"customizationSetting":null},{"restId":"1","menuKey":"menu#1628259102205","menuId":"1628259102205","name":"Non Veg Thali","desc":"Rice+Daal+Meat","imgUrl":"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg","category":"Starters","cost":18,"characteristics":{},"quantity":0,"isCustomizable":1,"customizationSetting":{"0":"Chicken","1":"Mutton"}}]');
    },
    

    getMenuListEntities : (originalMenulists:MenuDto[]):MenuLists[] => {
        const categoryToMenus =new Map();

        originalMenulists.forEach(
          (item:any)=>{
            const key = item.category;
            const collection= categoryToMenus.get(key);
            if(!collection){
              categoryToMenus.set(key,[item]);
            }else{
              collection.push(item);
            }
          }
        );

        // Sort Items in each category 
        const compareMenuOrder=(menuDto1: MenuDto, menuDto2: MenuDto): number=> {
          let order1=parseInt(menuDto1.menuOrder.split('#')[1]);
          let order2=parseInt(menuDto2.menuOrder.split('#')[1]);
          return order1-order2;
        }
        categoryToMenus.forEach((list:MenuDto[],catogory)=>{
          list.sort(
            (item1,item2)=> compareMenuOrder(item1,item2))
        });
        
        const menuListsEntities: MenuLists[]= []
        categoryToMenus.forEach(
          (val,key)=>{
              menuListsEntities.push({
                listName:key,
                menuList:val
              })
          }
        );

        const compareCategoryOrder=(menuList1: MenuDto[], menuList2: MenuDto[]): number=> {
          let order1=parseInt(menuList1[0].menuOrder.split('#')[0]);
          let order2=parseInt(menuList2[0].menuOrder.split('#')[0]);
          return order1-order2;
        }
        // Sort all category list
        menuListsEntities.sort(
          (item1:MenuLists,item2:MenuLists)=> compareCategoryOrder(item1.menuList,item2.menuList)
        );
        return menuListsEntities;
    },

}

export default MenuListService;



