import fs from "fs";
import { MenuDto } from "../models/menu";
import { OrderDto } from "../models/order";

interface cachItem {
  expiry: number;
  value: string;
}

export enum CASH_KEYS {
  MENU_CART = "MENU_CART",
  MENU = "MENU",
  PAYMENT_EDIT_ORDER = "PAYMENT_EDIT_ORDER",
}

export const BrowserCacheUtil = {
  // ttl in minute
  put: (content: string, cashKey: string, ttlMin: number): void => {
    const ttl = ttlMin * 1000 * 60;
    const later = new Date().getTime() + ttl;
    const key: string = cashKey;
    const item: cachItem = {
      value: content,
      expiry: later,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },

  get: (cashKey: string): any => {
    const key: string = cashKey;
    const str = localStorage.getItem(key);
    if (!str) {
      return null;
    }
    const item: cachItem = JSON.parse(str);
    if (new Date().getTime() > item.expiry) {
      localStorage.removeItem(key);
      return null;
    }
    return JSON.parse(item.value);
  },

  putOrder: (content: string): void => {
    const ttl = 1000 * 15 * 60;
    const later = new Date().getTime() + ttl;
    const key: string = "MENU_EDIT_ORDER";
    const item: cachItem = {
      value: content,
      expiry: later,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },
  putMenu: (content: string): void => {
    const ttl = 1000 * 15 * 60;
    const later = new Date().getTime() + ttl;
    const key: string = "MENU";
    const item: cachItem = {
      value: content,
      expiry: later,
    };
    localStorage.setItem(key, JSON.stringify(item));
  },
  getMenu: (): MenuDto[] | null => {
    const key: string = "MENU";
    const menuStr = localStorage.getItem(key);
    if (!menuStr) {
      return null;
    }
    const item: cachItem = JSON.parse(menuStr);
    if (new Date().getTime() > item.expiry) {
      localStorage.removeItem(key);
      return null;
    }
    return JSON.parse(item.value);
  },

  getOrder: (): OrderDto | null => {
    const key: string = "MENU_EDIT_ORDER";
    const orderStr = localStorage.getItem(key);
    if (!orderStr) {
      return null;
    }
    const item: cachItem = JSON.parse(orderStr);
    if (new Date().getTime() > item.expiry) {
      localStorage.removeItem(key);
      return null;
    }
    return JSON.parse(item.value);
  },
};
