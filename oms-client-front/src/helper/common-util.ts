import { EventData, OrderEvent } from "../models/event";

export function isEmpty(obj:any){
    for(var i in obj) return false;
    return true;
}


export function getCostString(cost:number){
    if(cost===0||cost===null){
        return ''
    }
    return Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(cost);;
}

export function convertToNumber(cost:any):number{
    if(cost as string){
        return parseFloat(cost);
    }
    if(cost as number){
        return cost;
    }else{
        return 0;
    }
     
}

export function handleKitchenNotification(evt:any,kitchenId:string):boolean{

    console.log('Message Received:'+evt.data);
    // Update only when order created for this kitchen
    let needToUpdate:boolean=false;
    const event:OrderEvent=JSON.parse(evt.data);
    if(event){
      const eventData:EventData[]=JSON.parse(event.data);
      eventData.forEach(item=>{
        if(item.targetId===kitchenId)
          needToUpdate=true;
      })
    }   
    return needToUpdate;
}

export function playNotification():void{
    const notification = new Audio('/asset/Notification.wav');
    notification.muted=false;
    notification.play();
}