import {
  KitchenStatus,
  OrderCostDto,
  OrderDto,
  OrderItemDto,
  ORDER_ITEM_STATUS,
  ORDER_STATUS,
} from "../models/order";
import * as _ from "lodash";
import { MenuDto } from "../models/menu";
import { BrowserCacheUtil } from "./BrowserCacheUtil";
import { MenuDtoVo } from "../models/menuVo";
import { PAYMENT_ORDER_ACTION } from "../pages/table-order-page";
import { IButtonProp } from "../components/button-list/button-list";

export const OrderUtil = {
  getActionButtonBasedOnSelectState(
    selectMode: boolean,
    selectedCount: number
  ) {
    let actions: IButtonProp[] = [
      { id: PAYMENT_ORDER_ACTION.SELECT, label: "Select", disable: false },
      {
        id: PAYMENT_ORDER_ACTION.SELECT_ALL,
        label: "Select all",
        disable: false,
      },
      {
        id: PAYMENT_ORDER_ACTION.CLEAR_SELECT,
        label: "Clear selection",
        disable: true,
      },
      { id: PAYMENT_ORDER_ACTION.MERGE, label: "Merge", disable: true },
      { id: PAYMENT_ORDER_ACTION.DELETE, label: "Delete", disable: true },
    ];
    if (selectMode) {
      actions.forEach((action) => {
        if (selectedCount > 0 && action.id === PAYMENT_ORDER_ACTION.DELETE) {
          // Currently delete is always disabled
          // action.disable=false;
        }
        if (selectedCount > 1 && action.id === PAYMENT_ORDER_ACTION.MERGE) {
          action.disable = false;
        }
        if (action.id === PAYMENT_ORDER_ACTION.CLEAR_SELECT) {
          action.disable = false;
        }
      });
    }
    return actions;
  },
  getPendingSimilarItemInOrder(
    order: OrderDto,
    itemId: string
  ): OrderItemDto | undefined {
    return order.items.find((item) => {
      if (
        item.itemId.startsWith(itemId) &&
        item.itemStatus !== ORDER_ITEM_STATUS.SERVED &&
        item.itemStatus !== ORDER_ITEM_STATUS.DELETED
      )
        return true;
    });
  },

  createInsertTranOrder(
    originalOrder: OrderDto,
    updatedOrder: OrderDto
  ): OrderDto {
    let upsertOrderDto: OrderDto = _.cloneDeep(originalOrder);
    updatedOrder.items.forEach((newItem) => {
      let originalMatchingItem: OrderItemDto | undefined =
        this.getPendingSimilarItemInOrder(upsertOrderDto, newItem.itemId);
      if (
        originalMatchingItem &&
        originalMatchingItem.itemStatus !== ORDER_ITEM_STATUS.SERVED &&
        originalMatchingItem.itemStatus !== ORDER_ITEM_STATUS.DELETED
      ) {
        console.log(
          "Similar item present so updated quantity of original item"
        );
        originalMatchingItem.itemQuantity =
          newItem.itemQuantity + originalMatchingItem.itemQuantity;
      } else {
        console.log("Totally new item so push to original list");
        newItem.itemId = newItem.itemId + "I" + new Date().getUTCSeconds();
        upsertOrderDto.items.push(newItem);
      }
    });
    return upsertOrderDto;
  },

  getInitMenu(): MenuDto[] {
    const cachedMenu = BrowserCacheUtil.getMenu();
    if (cachedMenu != null) return cachedMenu;
    return [];
  },

  getMenuInitOrder(): OrderDto {
    const cachedMenu = BrowserCacheUtil.getOrder();
    if (cachedMenu != null) return cachedMenu;
    return OrderUtil.getEmptyOrder();
  },

  getItemWithSameId(order: OrderDto, itemId: string): OrderItemDto | undefined {
    return order.items.find((item) => {
      if (item.itemId === itemId) return true;
    });
  },

  createUpsertTranOrder(
    originalOrder: OrderDto,
    updatedOrder: OrderDto
  ): OrderDto {
    let upsertOrderDto: OrderDto = _.cloneDeep(originalOrder);
    let updatedItems: Array<OrderItemDto> = [];
    let deletedItems: Array<OrderItemDto> = [];
    let unchangedItems: Array<OrderItemDto> = [];
    let insertedItem: Array<OrderItemDto> = [];
    // console.log('createUpsertTranOrder');
    // console.log('originalOrder',originalOrder.items);
    // console.log('updatedOrder',updatedOrder.items);
    originalOrder.items.forEach((originalItem) => {
      if (
        originalItem.itemStatus === ORDER_ITEM_STATUS.SERVED ||
        originalItem.itemStatus === ORDER_ITEM_STATUS.DELETED
      ) {
        unchangedItems.push(originalItem);
      } else {
        console.log("Checking for upsert");
        let updatedItem: OrderItemDto | undefined = this.getItemWithSameId(
          updatedOrder,
          originalItem.itemId
        );
        if (updatedItem) {
          if (updatedItem.itemQuantity === originalItem.itemQuantity) {
            console.log(
              "Item quantity in both order is same so should be unchanged"
            );
            unchangedItems.push(updatedItem);
          } else {
            console.log("Item quantity changed");
            deletedItems.push(originalItem);
            updatedItems.push(updatedItem);
          }
        } else {
          console.log("Item not found in new order list so should be deleted");
          deletedItems.push(originalItem);
        }
      }
    });

    updatedOrder.items.forEach((newItem) => {
      if (
        newItem.itemStatus !== ORDER_ITEM_STATUS.DELETED &&
        newItem.itemStatus !== ORDER_ITEM_STATUS.SERVED
      ) {
        const similarOldItem = this.getPendingSimilarItemInOrder(
          upsertOrderDto,
          newItem.itemId
        );
        if (similarOldItem) {
          console.log(
            "This is updated item do nothing since handled in previous loop"
          );
        } else {
          insertedItem.push(newItem);
        }
      }
    });

    let upsertItemList: Array<OrderItemDto> = [];

    updatedItems.forEach((item) => {
      item.itemId =
        item.itemId.replace("U", "") + "U" + new Date().getUTCSeconds();
      upsertItemList.push(item);
    });

    unchangedItems.forEach((item) => {
      upsertItemList.push(item);
    });

    let newKitchensIds = new Set<string>();
    insertedItem.forEach((item) => {
      item.itemId = item.itemId + "I" + new Date().getUTCSeconds();
      upsertItemList.push(item);
      newKitchensIds.add(item.kitchenId);
    });

    deletedItems.forEach((item) => {
      item.itemId =
        item.itemId.replace("D", "") + "D" + new Date().getUTCSeconds();
      //item.isDeleted=true;
      item.itemStatus = ORDER_ITEM_STATUS.DELETED;
      upsertItemList.push(item);
    });

    upsertOrderDto.items = this.sortItemListByStatus(upsertItemList);
    //const unservedItem = upsertOrderDto.items.find(item=>item.itemStatus=ORDER_ITEM_STATUS.PREPARING);

    // Add kitche for new Items if not present
    newKitchensIds.forEach((id) => {
      let isPresent: boolean = false;
      upsertOrderDto.kitchenStatus.forEach((kitchen) => {
        if (kitchen.kitchenId === id) isPresent = true;
      });
      if (!isPresent) {
        upsertOrderDto.kitchenStatus.push({
          kitchenId: id,
          status: ORDER_STATUS.PLACED,
        });
      }
    });

    // update kitchen status
    upsertOrderDto.items.forEach((item) => {
      if (item.itemStatus <= ORDER_ITEM_STATUS.PREPARING) {
        upsertOrderDto.kitchenStatus.forEach((kitchen) => {
          if (item.kitchenId === kitchen.kitchenId)
            kitchen.status = ORDER_STATUS.PLACED;
        });
      }
    });
    upsertOrderDto.status = this.getCombineOrderStatus(upsertOrderDto);
    upsertOrderDto.cost = this.getCost(upsertOrderDto);

    return upsertOrderDto;
  },

  sortItemListByStatus(list: OrderItemDto[]): OrderItemDto[] {
    // list
    const newList: OrderItemDto[] = [];
    list.forEach((item) => {
      if (item.itemStatus !== ORDER_ITEM_STATUS.DELETED) newList.push(item);
    });
    newList.sort((item1, item2) => {
      return item1.itemStatus - item2.itemStatus;
    });
    list.forEach((item) => {
      if (item.itemStatus === ORDER_ITEM_STATUS.DELETED) newList.push(item);
    });

    return newList;
  },

  isOrderBelongsToKitchen(order: OrderDto, kitchenId: string): boolean {
    let item = order.items.find((item) => item.kitchenId === kitchenId);
    return item != undefined;
  },
  getKitchenStatusForOrder(order: OrderDto, kitchenId: string): ORDER_STATUS {
    let kitchenStatus: KitchenStatus | undefined = order.kitchenStatus.find(
      (kitchen) => kitchen.kitchenId === kitchenId
    );
    return kitchenStatus ? kitchenStatus.status : 1;
  },
  getPendingKitchenOrder(orders: OrderDto[], kitchenId: string): OrderDto[] {
    return orders
      .filter((order) => {
        return (
          this.getKitchenStatusForOrder(order, kitchenId) <
            ORDER_STATUS.SERVED &&
          this.isOrderBelongsToKitchen(order, kitchenId)
        );
      })
      .sort(function (order1, order2) {
        return order1.createdAt - order2.createdAt;
      });
  },
  getFinishedKitchenOrder(orders: OrderDto[], kitchenId: string): OrderDto[] {
    const servedOrder: OrderDto[] = orders
      .filter((order) => {
        return (
          this.getKitchenStatusForOrder(order, kitchenId) ===
            ORDER_STATUS.SERVED &&
          this.isOrderBelongsToKitchen(order, kitchenId)
        );
      })
      .sort(function (order1, order2) {
        return order2.updatedAt - order1.updatedAt;
      });

    return servedOrder;
  },
  getPendingPaymentOrder(orders: OrderDto[]): OrderDto[] {
    return orders
      .filter((order) => {
        return order.status < ORDER_STATUS.PAID;
      })
      .sort(function (order1, order2) {
        return order2.createdAt - order1.createdAt;
      });
  },
  getOrderItemStatusString(status: ORDER_ITEM_STATUS): string {
    if (status === ORDER_ITEM_STATUS.PREPARING) return "Preparing";
    else if (status === ORDER_ITEM_STATUS.SERVED) return "Served";
    else if (status === ORDER_ITEM_STATUS.PLACED) return "Placed";
    else return "Wrong Order Status";
  },

  getOrderItemColorString(status: ORDER_ITEM_STATUS): string {
    if (status === ORDER_ITEM_STATUS.PREPARING) return "yellow";
    else if (status === ORDER_ITEM_STATUS.SERVED) return "green";
    else if (status === ORDER_ITEM_STATUS.PLACED) return "white";
    else return "gray";
  },

  getCombineOrderStatus(order: OrderDto): ORDER_STATUS {
    // If all kitchen status x Order status x
    // If Any kitchen status < served Order status is Cooking
    let isAllKitchenItemStatusSame = true;
    let tempStatus: ORDER_STATUS = ORDER_STATUS.INVALID;
    order.kitchenStatus.forEach((item) => {
      if (tempStatus === ORDER_STATUS.INVALID) {
        tempStatus = item.status;
      } else if (tempStatus !== item.status) {
        isAllKitchenItemStatusSame = false;
      }
    });

    if (!isAllKitchenItemStatusSame) {
      return ORDER_STATUS.PREPARING;
    }
    return tempStatus;
  },
  getFinishedPaymentOrder(orders: OrderDto[]): OrderDto[] {
    const servedOrder: OrderDto[] = orders
      .filter((order) => {
        return (
          this.getCombineOrderStatus(order) === ORDER_STATUS.PAID.valueOf()
        );
      })
      .sort(function (order1, order2) {
        return order2.updatedAt - order1.updatedAt;
      });
    return servedOrder;
  },
  getPendingPaymentOrderForTable(
    orders: OrderDto[],
    tableNo: string
  ): OrderDto[] {
    return orders
      .filter((order) => {
        return order.status < ORDER_STATUS.PAID && order.tableId === tableNo;
      })
      .sort(function (order1, order2) {
        return order2.createdAt - order1.createdAt;
      });
  },
  getFinishedPaymentOrderForTable(
    orders: OrderDto[],
    tableNo: string
  ): OrderDto[] {
    const servedOrder: OrderDto[] = orders
      .filter((order) => {
        return (
          order.status === ORDER_STATUS.PAID.valueOf() &&
          order.tableId === tableNo
        );
      })
      .sort(function (order1, order2) {
        return order2.updatedAt - order1.updatedAt;
      });
    return servedOrder;
  },
  getCost(order: OrderDto): OrderCostDto {
    const TAX = 0;
    let subTotal: number = 0;
    order.items.forEach((item) => {
      if (item.itemStatus !== ORDER_ITEM_STATUS.DELETED)
        subTotal += item.itemQuantity * item.itemCost;
    });
    const gst = (subTotal * TAX) / 100;
    const totalCost = subTotal + gst;
    return {
      totalCost: totalCost,
      gst: gst,
      subTotal: subTotal,
    };
  },

  updateOrderStatus(
    orders: OrderDto[],
    targetIds: string[],
    status: ORDER_STATUS
  ): OrderDto[] {
    let targetOrders: OrderDto[] = [];
    orders.forEach((order) => {
      if (targetIds.indexOf(order.orderId) > -1) {
        order.status = status;
        targetOrders.push(order);
      }
    });
    return targetOrders;
  },

  mergeOrdersByOrderId(
    mergedTargets: OrderDto[],
    mergeTargetsIds: string[]
  ): OrderDto {
    let mergedOrder: OrderDto = this.getEmptyOrder();

    // let mergedTargetIdsSet = new Set(mergeTargetsIds);
    // orders.forEach(order=>{
    //     if(mergedTargetIdsSet.has(order.orderId)){
    //         mergedTargets.push(order);
    //     }
    //     order.status=ORDER_STATUS.DELETED;
    // })

    this.updateMergedItemAndKitchen(mergedTargets, mergedOrder);

    mergedOrder.cost = this.getCost(mergedOrder);
    mergedOrder.tableId = mergedTargets[0].tableId;
    mergedOrder.status = this.getCombineOrderStatus(mergedOrder);
    mergedOrder.restaurantId = mergedTargets[0].restaurantId;
    console.log("Orders to be merged", mergedTargets);
    console.log("mergedOrder", mergedOrder);
    return mergedOrder;
  },
  getOriginalItemId(itemId: string): string {
    return itemId.split("#")[0];
  },
  updateMergedItemAndKitchen(orders: OrderDto[], mergedOrder: OrderDto) {
    let newItems: OrderItemDto[] = [];
    let kitchenStatusMap = new Map<string, number>();
    let itemIdToItemMap = new Map<string, OrderItemDto>();
    orders.forEach((order) => {
      order.items.forEach((item) => {
        const newItemId =
          this.getOriginalItemId(item.itemId) + "#" + item.itemStatus;
        if (itemIdToItemMap.has(newItemId)) {
          // Add quantity
          const existingItem = itemIdToItemMap.get(newItemId);
          if (existingItem) {
            existingItem.itemQuantity += item.itemQuantity;
            itemIdToItemMap.set(newItemId, existingItem);
          }
        } else {
          itemIdToItemMap.set(newItemId, { ...item, itemId: newItemId });
        }
      });

      order.kitchenStatus.forEach((kitchen) => {
        const preStatus = kitchenStatusMap.get(kitchen.kitchenId);
        if (preStatus && preStatus !== null && preStatus > kitchen.status) {
          kitchenStatusMap.set(kitchen.kitchenId, kitchen.status);
        } else if (!preStatus || preStatus === null) {
          kitchenStatusMap.set(kitchen.kitchenId, kitchen.status);
        }
      });
    });

    itemIdToItemMap.forEach((val, key) => {
      newItems.push(val);
    });
    mergedOrder.items = newItems;

    let kitchenStatus: KitchenStatus[] = [];
    kitchenStatusMap.forEach((val, key) => {
      kitchenStatus.push({
        kitchenId: key,
        status: val,
      });
    });
    mergedOrder.kitchenStatus = kitchenStatus;
  },

  filterItem(order: OrderDto): OrderDto {
    order.items = order.items.filter((item) => item.itemQuantity > 0);
    return order;
  },

  getTables(orders: OrderDto[]): string[] {
    let tableSet: Set<string> = new Set();

    orders.forEach((order) => {
      if (order.status !== ORDER_STATUS.PAID.valueOf())
        tableSet.add(order.tableId);
    });
    //console.log('getTables',tableSet)
    return Array.from(tableSet);
  },
  sortLatestUpdate(orders: OrderDto[]): OrderDto[] {
    const sortedOrders: OrderDto[] = orders.sort(function (order1, order2) {
      return order2.updatedAt - order1.updatedAt;
    });
    return sortedOrders;
  },
  getOrderStatusText(status: string): string {
    switch (status) {
      case "1":
        return "New";
      case "2":
        return "Cooking";
      case "3":
        return "Served";
      case "4":
        return "Completed";
      case "5":
        return "Deleted";
      default:
        return "";
    }
  },

  // UTILITIES FOR MENU PAGE
  getQuantity(menuItem: MenuDto, order: OrderDto): number {
    if (menuItem.isCustomizable !== 1) {
      const orderItem = order.items.find(
        (item) => item.itemId == menuItem.menuId
      );
      if (orderItem != null) return orderItem.itemQuantity;
      return 0;
    }
    if (menuItem.isCustomizable === 1) {
      console.log("custome quantity", menuItem.menuId);
      let quantity = 0;
      order.items.forEach((item) => {
        if (item.itemId.startsWith(menuItem.menuId))
          quantity += item.itemQuantity;
      });
      return quantity;
    }
    return 0;
  },
  findOrderItem(menuItem: MenuDto, order: OrderDto): OrderItemDto | undefined {
    return order.items.find((item) => item.itemId == menuItem.menuId);
  },
  getOrderItemVo(menu: MenuDto): OrderItemDto {
    return {
      itemId: menu.menuId,
      itemName: menu.name,
      itemDetail: menu.desc,
      itemQuantity: 0,
      itemCost: menu.cost,
      itemStatus: ORDER_ITEM_STATUS.PLACED.valueOf(),
      isCustomizable: menu.isCustomizable,
      customizableValue: {},
      kitchenId: menu.kitchenId,
      //isDeleted:false
    };
  },
  getOrderItemVo2(menu: MenuDtoVo): OrderItemDto {
    return {
      itemId: menu.menuId,
      itemName: menu.name,
      itemDetail: menu.desc,
      itemQuantity: 0,
      itemCost: menu.cost,
      itemStatus: ORDER_ITEM_STATUS.PREPARING.valueOf(),
      isCustomizable: menu.isCustomizable,
      customizableValue: {},
      kitchenId: menu.kitchenId,
      //isDeleted:false
    };
  },
  getInitOrder(): OrderDto {
    // const  cachedOrder = BrowserCacheUtil.getOrder();

    // if(cachedOrder!=null)
    // return cachedOrder;
    return this.getEmptyOrder();
  },

  getEmptyOrder(): OrderDto {
    return {
      restaurantId: "1",
      orderId: "",
      orderKey: "",
      status: ORDER_STATUS.PLACED,
      tableId: "",
      cost: {
        totalCost: 0,
        gst: 0,
        subTotal: 0,
      },
      orderNotes: "",
      items: [],
      createdAt: 0,
      updatedAt: 0,
      kitchenStatus: [],
    };
  },
};
