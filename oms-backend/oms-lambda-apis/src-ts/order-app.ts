'use strict'
import { DynamoDB } from 'aws-sdk';
// var moment = require('moment');

const dynamoClient = new DynamoDB.DocumentClient();

enum path{
  ORDERS='/orders',
  ORDER='/order',
  HEALTH='/health'
}

const TABLE_NAME = 'restaurant';
const KEY='restaurantId';
const SORT_KEY='dataIdentifier';

const STATUS_TABLE_INDEX='order_status_table_index'

interface ResponseBody{
  statusCode:number,
  headers:{
    "Content-Type":string,
    "Access-Control-Allow-Headers" : string,
    "Access-Control-Allow-Origin": string,
    "Access-Control-Allow-Methods": string
  },
  body: string
}

enum ORDER_STATUS{
  PLACED=1,
  PREPARING=2,
  SERVED=3,
  PAID=3
}

enum ORDER_ITEM_STATUS{
  PREPARING= 1 ,
  SERVED= 2
}

interface OrderItemDto{
  itemId:string,
  itemName:string,
  itemDetail:string,
  itemQuantity:number,
  itemCost:number,
  itemStatus:number,
  isCustomizable:number,
  kitchenId:string,
  isDeleted:boolean,
  customizableValue?:{}
}

interface KitchenStatus{
  kitchenId:string,
  status:number
} 

interface OrderCostDto{
  totalCost:number,
  gst:number,
  subTotal:number
}

interface OrderDto{
  restaurantId:string,
  orderKey:string,
  orderId:string
  status:number, // currently not used
  tableId:string,
  cost:OrderCostDto,
  orderNotes:string,
  items:OrderItemDto[],
  kitchenStatus:KitchenStatus[]
  createdAt:number,
  updatedAt:number
}

export interface OrderCostEntity{
  totalCost:number,
  gst:number,
  subTotal:number
}

export interface OrderItemEntity{
  itemId:string,
  itemName:string,
  itemDetails:string,
  quantity:number,
  itemStatus:number,
  cost:number,
  kitchenId:string,
  isDeleted:boolean,
  isCustomizable:number,
  customizableValue?:{}
}


export interface OrderEntity{
  restaurantId:string,
  dataIdentifier:string,
  orderId:string,
  status_table_Identifier:string,// Need to improve this logic
  cost:OrderCostEntity,
  orderNotes:string,
  orderItems:OrderItemEntity[],
  kitchenStatus:KitchenStatus[],
  createdAt:number,
  updatedAt:number
}

export interface OrderIdentifiers{
  orderKey: string;
  orderId: string;
}

exports.handler = async function(event:any){
  console.log('Request events:',event);
    let response:ResponseBody|void;
    switch (true){
        case event.httpMethod =='GET' && event.path === path.HEALTH:
            response=buildResponse(200);
            break;
        case event.httpMethod == 'GET' && event.path === path.ORDER:
            if('orderId' in event.queryStringParameters)
              response= await getOrder(event.queryStringParameters.restaurantId,event.queryStringParameters.orderId);
            else
              response= await getTodaysOrder(event.queryStringParameters.restaurantId);
            break;
        case event.httpMethod == 'GET' && event.path === path.ORDERS:
            if('orderStatus' in event.queryStringParameters){
              if('tableNo' in event.queryStringParameters)
              response = await getOrdersForStatus(event.queryStringParameters.restaurantId,event.queryStringParameters.orderStatus,event.queryStringParameters.tableNo);
              else
              response = await getOrdersForStatus(event.queryStringParameters.restaurantId,event.queryStringParameters.orderStatus);
                
            }else if('ordersTill' in event.queryStringParameters){
              if('tableNo' in event.queryStringParameters)
              response = await getOrdersTillStatus(event.queryStringParameters.restaurantId,event.queryStringParameters.ordersTill,event.queryStringParameters.tableNo);
              else
              response = await getOrdersTillStatus(event.queryStringParameters.restaurantId,event.queryStringParameters.ordersTill);
            }
            else{
              response= await getOrders(event.queryStringParameters.restaurantId);
            }
            
            break;
        case event.httpMethod == 'POST' && event.path === path.ORDER:
            response= await saveOrder(JSON.parse(event.body));    
            break;
        case event.httpMethod == 'PUT' && event.path === path.ORDER:
          response= await putOrder(JSON.parse(event.body));    
          break;
        // case event.httpMethod == 'PATCH' && event.path === menuPath:
        //     const requestBody = JSON.parse(event.body);
        //     response= await modifyMenu(requestBody.restaurantId,requestBody.menuId,requestBody.updateKey,requestBody.updateValue);      
        //     break;
        // case event.httpMethod == 'DELETE' && event.path === menuPath:
        //     const requestBody = JSON.parse(event.body);
        //     response= await deleteMenu(requestBody.restaurantId,requestBody.menuId);
        //     break;
        default:
            response=buildResponse(404,'404 Not Found');

    }
    return response;
}



function buildResponse(statusCode:number,body?:string):ResponseBody{
  return {
      statusCode:statusCode,
      headers:{
        "Content-Type":"application/json",
        "Access-Control-Allow-Headers" : "Content-Type",
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
      },
      body: body
  }
}


async function getOrder(restaurantId:string,orderId:string):Promise<void | ResponseBody>{
  const params = {
      
      TableName:TABLE_NAME,
      Key:{
        'restaurantId':restaurantId,
        'dataIdentifier':'order#'+orderId
      }
  }
  return await dynamoClient.get(params).promise().then((response)=>{
      return buildResponse(200,JSON.stringify(entityToDtoMapper(response.Item as OrderEntity)));
  },(error)=>{
      console.error('Order Not Found:'+error);
  });
}

async function getTodaysOrder(restaurantId:string){
  try{
    var startD = new Date();
    startD.setHours(0,0,0,0);

    var endD = new Date();
    endD.setHours(23,59,59,999);

    const start = startD.valueOf(); 
    const end = endD.valueOf();
    
    const params = {
        TableName:TABLE_NAME,
        KeyConditionExpression: 'restaurantId = :restaurantId AND dataIdentifier between  :start and :end',
        ExpressionAttributeValues: {
        ':restaurantId': restaurantId,
        ':start': "order#"+start,
        ':end': "order#"+end,
        }
    };
    console.log("getOrders params:"+JSON.stringify(params));
    const response = await dynamoClient.query(params).promise();
    console.log("getOrders result:"+JSON.stringify(response));
  
    const dtos:OrderDto[]=response.Items.map((item:OrderEntity)=>{
      return entityToDtoMapper(item);
    });
  
    return buildResponse(200,JSON.stringify(dtos));
    }catch(error){
        console.log("Error:"+error);
    } 
}

async function getOrdersTillStatus(restaurantId:string,orderStatus:string,tableNo?:string){
  try{

  const start=tableNo?'1#'+tableNo:'1#';
  const end=tableNo?orderStatus+'#'+tableNo:orderStatus+'#';
  
  const params = {
      TableName:TABLE_NAME,
      IndexName:STATUS_TABLE_INDEX,
      KeyConditionExpression: 'restaurantId = :restaurantId AND status_table_Identifier between  :start and :end',
      ExpressionAttributeValues: {
      ':restaurantId': restaurantId,
      ':start': start,
      ':end': end,
      }
  };
  console.log("getOrders params:"+JSON.stringify(params));
  const response = await dynamoClient.query(params).promise();
  console.log("getOrders result:"+JSON.stringify(response));

  const dtos:OrderDto[]=response.Items.map((item:OrderEntity)=>{
    return entityToDtoMapper(item);
  });

  return buildResponse(200,JSON.stringify(dtos));
  }catch(error){
      console.log("Error:"+error);
  }
}


async function getOrdersForStatus(restaurantId:string,orderStatus:string,tableNo?:string){
  try{

  const statusTableIdentifier = tableNo?orderStatus+'#'+tableNo:orderStatus+'#';

  const params = {
      TableName:TABLE_NAME,
      IndexName:STATUS_TABLE_INDEX,
      KeyConditionExpression: 'restaurantId = :restaurantId AND begins_with ( status_table_Identifier , :status_table_Identifier )',
      ExpressionAttributeValues: {
      ':restaurantId': restaurantId,
      ':status_table_Identifier': statusTableIdentifier
      }
  };
  console.log("getOrders params:"+JSON.stringify(params));
  const response = await dynamoClient.query(params).promise();
  console.log("getOrders result:"+JSON.stringify(response));

  const dtos:OrderDto[]=response.Items.map((item:OrderEntity)=>{
    return entityToDtoMapper(item);
  });

  return buildResponse(200,JSON.stringify(dtos));
  }catch(error){
      console.log("Error:"+error);
  }
}

async function getOrders(restaurantId:string){
  try{
  const params = {
      
      KeyConditionExpression: 'restaurantId = :restaurantId AND begins_with ( dataIdentifier , :dataIdentifier )',
      ExpressionAttributeValues: {
      ':restaurantId': restaurantId,
      ':dataIdentifier': 'order'
      },
      TableName:TABLE_NAME
  };
  console.log("getOrders params:"+params);
  const response = await dynamoClient.query(params).promise();
  console.log("getOrders result:"+JSON.stringify(response));

  const dtos:OrderDto[]=response.Items.map((item:OrderEntity)=>{
    return entityToDtoMapper(item);
  });

  return buildResponse(200,JSON.stringify(dtos));
  }catch(error){
      console.log("Error:"+error);
  }
}


async function saveOrder(requestBody:OrderDto){
  const entity:OrderEntity=createEntity(requestBody);
  const params = {
      TableName:TABLE_NAME,
      Item:entity
  }
  return await dynamoClient.put(params).promise().then(()=>{
      const body={
          Operation:'SAVE',
          Message:'SUCCESS',
          Item:entityToDtoMapper(entity)
      }
      return buildResponse(200,JSON.stringify(body));
  },(error)=>{
      console.error('Menu Not Found');
  });
}

async function putOrder(requestBody:OrderDto){
  const entity:OrderEntity=dtoToEntityMapper(requestBody);
  const params = {
      TableName:TABLE_NAME,
      Item:entity
  }
  return await dynamoClient.put(params).promise().then(()=>{
      const body={
          Operation:'SAVE',
          Message:'SUCCESS',
          Item:entity
      }
      return buildResponse(200,JSON.stringify(body));
  },(error)=>{
      console.error('Can not insert Order');
  });
}


// DTO TO ENTITY MAPPER
function dtoToEntityMapper(order:OrderDto):OrderEntity{
  let foodOrder:OrderEntity=<OrderEntity>{};

  foodOrder.restaurantId=order.restaurantId;
  foodOrder.dataIdentifier=order.orderKey;
  foodOrder.orderId=order.orderId;
  foodOrder.status_table_Identifier=getOrderStatusTableKey(order.status,order.tableId);
  foodOrder.cost=order.cost;
  foodOrder.orderNotes=order.orderNotes;
  foodOrder.orderItems=order.items.map((item:OrderItemDto)=>{
    return mapOrderItemEntity(item)
  });
  foodOrder.kitchenStatus=order.kitchenStatus;
  foodOrder.createdAt=order.createdAt;
  foodOrder.updatedAt=Date.now();
  return foodOrder;
}


function createEntity(order:OrderDto):OrderEntity{
  let foodOrder:OrderEntity=<OrderEntity>{};
  const orderIds= getOrderIdentifiers();

  foodOrder.restaurantId=order.restaurantId;
  foodOrder.dataIdentifier=orderIds.orderKey;
  foodOrder.orderId=orderIds.orderId;
  foodOrder.status_table_Identifier=getOrderStatusTableKey(order.status,order.tableId);
  foodOrder.cost=order.cost;
  foodOrder.orderNotes=order.orderNotes;
  foodOrder.orderItems=order.items.map((item)=>{
    return mapOrderItemEntity(item)
  });
  foodOrder.kitchenStatus=order.kitchenStatus;
  foodOrder.createdAt=Date.now();
  foodOrder.updatedAt=Date.now();
  return foodOrder;
}

function mapOrderItemEntity(orderItem:OrderItemDto):OrderItemEntity{
  let orderItemEntity:OrderItemEntity=<OrderItemEntity>{};
  orderItemEntity.itemId=orderItem.itemId;
  orderItemEntity.itemName=orderItem.itemName;
  orderItemEntity.itemDetails=orderItem.itemDetail;
  orderItemEntity.quantity=orderItem.itemQuantity;
  orderItemEntity.itemStatus=orderItem.itemStatus;
  orderItemEntity.isCustomizable=orderItem.isCustomizable;
  orderItemEntity.customizableValue={};
  orderItemEntity.cost=orderItem.itemCost;
  orderItemEntity.isDeleted=orderItem.isDeleted;
  orderItemEntity.kitchenId=orderItem.kitchenId;
  
  return orderItemEntity;
}

function getOrderIdentifiers():OrderIdentifiers{
  // Get date in millis from epoc time
  let date = Date.now().toString();

  // let date = moment.utc();
  let orderIdentifiers:OrderIdentifiers={
    // orderKey:'order#'+'20211216#'+milliSeconds,
    orderKey:'order#'+date,
    orderId:date
  }

  return orderIdentifiers;
}

function getOrderStatusTableKey(status:ORDER_STATUS,table:string):string{
  return status.toString()+'#'+table;
}
// DTO TO ENTITY MAPPER END

// ENTITY TO DTO MAPPER
function entityToDtoMapper(order:OrderEntity): OrderDto{
  let foodOrder:OrderDto=<OrderDto>{};
  foodOrder.restaurantId=order.restaurantId;
  foodOrder.orderKey=order.dataIdentifier;
  foodOrder.orderId=order.orderId;
  foodOrder.status=getOrderStatus(order.status_table_Identifier);
  foodOrder.tableId=getOrderTable(order.status_table_Identifier);
  foodOrder.cost=order.cost;
  foodOrder.orderNotes=order.orderNotes;
  foodOrder.items=order.orderItems.map((item)=>{
    return itemEntityToDtoMapper(item);
  });
  foodOrder.kitchenStatus=order.kitchenStatus;
  foodOrder.createdAt=order.createdAt;
  foodOrder.updatedAt=order.updatedAt;
  return foodOrder;
}

function itemEntityToDtoMapper(itemEntity:OrderItemEntity):OrderItemDto{
  let item:OrderItemDto=<OrderItemDto>{};
  item.itemId=itemEntity.itemId;
  item.itemName=itemEntity.itemName;
  item.itemDetail=itemEntity.itemDetails;
  item.itemQuantity=itemEntity.quantity;
  item.isCustomizable=itemEntity.isCustomizable;
  item.customizableValue=itemEntity.customizableValue;
  item.isDeleted=itemEntity.isDeleted,
  // TODO Update based on order status
  item.itemStatus=itemEntity.itemStatus;
  item.kitchenId=itemEntity.kitchenId;
  item.itemCost=itemEntity.cost;
  return item;
}

function getOrderStatus(value:string):ORDER_STATUS{
    let status:ORDER_STATUS = parseInt(value.split('#')[0]);
    return status;
}
function getOrderTable(value:string):string{
  return value.split('#')[1]
}


let orderItems:OrderItemDto[]=[
  {
    itemId:'1',
    itemName:'Paneer Tikka',
    itemDetail:'Paneer Tikka cooked in think gravy',
    itemQuantity:2,
    itemStatus:ORDER_ITEM_STATUS.PREPARING,
    itemCost:12,
    isCustomizable:0,
    customizableValue:null,
    isDeleted:false,
    kitchenId:'1'
},
{
  itemId:'2',
  itemName:'Chicken Tikka',
  itemDetail:'Chicken Tikka cooked in think gravy',
  itemQuantity:1,
  itemStatus:ORDER_ITEM_STATUS.PREPARING,
  itemCost:15,
  isCustomizable:0,
  isDeleted:false,
  customizableValue:null,
  kitchenId:'0'
}];
let orderDto:OrderDto={
  restaurantId:'1',
  orderKey:'',
  orderId:'1',
  status:ORDER_STATUS.PLACED,
  tableId:'1',
  cost:{
    totalCost:12,
    subTotal:10,
    gst:2
  },
  kitchenStatus:[{
    kitchenId:'1',
    status:1},
    {
      kitchenId:'0',
      status:1}],
  createdAt:Date.now(),
  updatedAt:Date.now(),
  orderNotes:'Please cook spicy',
  items:orderItems,
}



// ENTITY TO DTO MAPPER END

// console.log("DTO Before");
// console.log(JSON.stringify(orderDto));

// let orderEntity:OrderEntity=createEntity(orderDto);
// console.log(orderEntity);

// console.log("DTO after");
// let orderDto1:OrderDto=entityToDtoMapper(orderEntity);
// console.log(JSON.stringify(orderDto1));

// getOrderStatusTableKey(1,'1');

// var startD = new Date();
//     startD.setHours(0,0,0,0);

//     var endD = new Date();
//     endD.setHours(23,59,59,999);

//     const start = startD.toString(); 
//     const end = endD.toString();

//     console.log(Date.now());
//     console.log(startD.getMilliseconds());
//     console.log(end)
