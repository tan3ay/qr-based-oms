'use strict'
import { ConfigService, DynamoDB } from 'aws-sdk';

const dynamoClient = new DynamoDB.DocumentClient();
interface KitchenDto{
    kitchenId:string,
    name:string,
    details:string
}

interface TableDto{
    tableNo:string,
    noOfSeats:number
}
interface RestaurantDto{
    restaurantId:string,
    name:string
    address:string, // currently not used
    city:string,
    country:string,
    language:string,
    currency:string,
    tables:TableDto[],
    kitchens:KitchenDto[],
    createdAt:number,
    updatedAt:number
}


interface KitchenDto{
    kitchenId:string,
    name:string,
    details:string
}

interface TableEntity{
    tableNo:string,
    noOfSeats:number
}
interface RestaurantEntity{
    restaurantId:string,
    dataIdentifier:string,
    name:string
    address:string, // currently not used
    city:string,
    country:string,
    language:string,
    currency:string,
    tables:TableEntity[],
    kitchens:KitchenDto[],
    createdAt:number,
    updatedAt:number
}

  enum path{
    RESTAURANT='/restaurant',
    HEALTH='/health'
  }

  interface ResponseBody{
    statusCode:number,
    headers:{
      "Content-Type":string,
      "Access-Control-Allow-Headers" : string,
      "Access-Control-Allow-Origin": string,
      "Access-Control-Allow-Methods": string
    },
    body: string
  }

  function buildResponse(statusCode:number,body?:string):ResponseBody{
    return {
        statusCode:statusCode,
        headers:{
            "Content-Type":"application/json",
            "Access-Control-Allow-Headers" : "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
        },
        body: body
    }
  }
  
  const TABLE_NAME:string = 'restaurant';
  const KEY:string='restaurantId';
  const SORT_KEY:string='dataIdentifier';

  exports.handler = async function(event:any){
    console.log('Request events:',event);
      let response:ResponseBody|void;
      switch (true){
          case event.httpMethod =='GET' && event.path === path.HEALTH:
              response=buildResponse(200);
              break;
          case event.httpMethod == 'GET' && event.path === path.RESTAURANT:
              response= await getRestaurant(event.queryStringParameters.restaurantId);
              break;
          case event.httpMethod == 'POST' && event.path === path.RESTAURANT:
              response= await saveRestaurant(JSON.parse(event.body));
              break;
          default:
              response=buildResponse(404,'404 Not Found');
      }
      return response;
  }

    async function getRestaurant(restaurantId:string):Promise<void | ResponseBody>{
        const params = {
            
            TableName:TABLE_NAME,
            Key:{
            'restaurantId':restaurantId,
            'dataIdentifier':'restaurant'
            }
        }

        console.log("In getMenu");
        console.log(restaurantId+" ");
        return await dynamoClient.get(params).promise().then((response)=>{
            return buildResponse(200,JSON.stringify(entityToDto(response.Item as RestaurantEntity)));
        },(error)=>{
            console.error('Menu Not Found:'+error);
        });
    }

    async function saveRestaurant(requestBody:RestaurantDto):Promise<void | ResponseBody>{
        const entity:RestaurantEntity = createEntity(requestBody)
        const params = {
            TableName:TABLE_NAME,
            Item:entity
        }
        return await dynamoClient.put(params).promise().then(()=>{
            const body={
                Operation:'SAVE',
                Message:'SUCCESS',
                Item:entity
            }
            return buildResponse(200,JSON.stringify(body));
        },(error)=>{
            console.error('Menu Not Found');
        });
    }


    function createEntity(restaurantDto:RestaurantDto):RestaurantEntity{
        const restaurantIds= Date.now().toString();
        let restaurantEntity:RestaurantEntity=
        {
            restaurantId:restaurantIds,
            dataIdentifier:'restaurant',
            name:restaurantDto.name,
            address:restaurantDto.address,
            city:restaurantDto.city,
            country:restaurantDto.country,
            language:restaurantDto.language,
            currency:restaurantDto.currency,
            tables:restaurantDto.tables,
            kitchens:restaurantDto.kitchens,
            createdAt:Date.now(),
            updatedAt:Date.now(),
        }
        return restaurantEntity;
      }


      function entityToDto(restaurantEntity:RestaurantEntity):RestaurantDto{
        let restaurantDto:RestaurantDto=
        {
            restaurantId:restaurantEntity.restaurantId,
            name:restaurantEntity.name,
            address:restaurantEntity.address,
            city:restaurantEntity.city,
            country:restaurantEntity.country,
            language:restaurantEntity.language,
            currency:restaurantEntity.currency,
            tables:restaurantEntity.tables,
            kitchens:restaurantEntity.kitchens,
            createdAt:restaurantEntity.createdAt,
            updatedAt:restaurantEntity.updatedAt,
        }
        return restaurantDto;
      
      }



const tableDtos:TableDto[]=[
    {tableNo:'1',noOfSeats:-1},
    {tableNo:'2',noOfSeats:-1},
    {tableNo:'3',noOfSeats:-1},
    {tableNo:'4',noOfSeats:-1},
    {tableNo:'5',noOfSeats:-1},
    {tableNo:'6',noOfSeats:-1},
    {tableNo:'11',noOfSeats:-1},
    {tableNo:'12',noOfSeats:-1},
    {tableNo:'13',noOfSeats:-1},
    {tableNo:'14',noOfSeats:-1},
    {tableNo:'21',noOfSeats:-1},
    {tableNo:'22',noOfSeats:-1},
    {tableNo:'23',noOfSeats:-1},
    {tableNo:'24',noOfSeats:-1},
    {tableNo:'31',noOfSeats:-1},
    {tableNo:'32',noOfSeats:-1},
    {tableNo:'33',noOfSeats:-1},
    {tableNo:'34',noOfSeats:-1},
    {tableNo:'41',noOfSeats:-1},
    {tableNo:'42',noOfSeats:-1},
    {tableNo:'43',noOfSeats:-1},
    {tableNo:'44',noOfSeats:-1},
    {tableNo:'45',noOfSeats:-1},
    {tableNo:'46',noOfSeats:-1},
    {tableNo:'501',noOfSeats:-1},
    {tableNo:'502',noOfSeats:-1},
    {tableNo:'503',noOfSeats:-1},
    {tableNo:'504',noOfSeats:-1},
    {tableNo:'505',noOfSeats:-1},
    {tableNo:'601',noOfSeats:-1},
    {tableNo:'602',noOfSeats:-1},
    {tableNo:'603',noOfSeats:-1},
    {tableNo:'604',noOfSeats:-1},
    {tableNo:'701',noOfSeats:-1},
    {tableNo:'702',noOfSeats:-1},
    {tableNo:'703',noOfSeats:-1},
    {tableNo:'704',noOfSeats:-1},
    {tableNo:'705',noOfSeats:-1},
    {tableNo:'706',noOfSeats:-1},
]
const kitchendtos:KitchenDto[]=[
    {
    kitchenId:'1',
    name:'Bar',
    details:''
    },
    {
    kitchenId:'2',
    name:'Food Kitchen',
    details:''
    },
]

const dto:RestaurantDto={
    restaurantId:'1',
    name:'Bharati Restuarant',
    address:'Parc Borely',
    city:'Marseille',
    country:'France',
    language:'French',
    currency:'Euro',
    tables:tableDtos,
    kitchens:kitchendtos,
    createdAt:0,
    updatedAt:0,
}

// console.log('Before',JSON.stringify(dto))

// const entity=createEntity(dto);
// console.log(JSON.stringify(entity))

// const dto2=entityToDto(entity);
// console.log('After',JSON.stringify(dto2))