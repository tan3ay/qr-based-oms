'use strict'
import { ConfigService, DynamoDB } from 'aws-sdk';

const dynamoClient = new DynamoDB.DocumentClient();



interface ResponseBody{
  statusCode:number,
  headers:{
    "Content-Type":string,
    "Access-Control-Allow-Headers" : string,
    "Access-Control-Allow-Origin": string,
    "Access-Control-Allow-Methods": string
  },
  body: string
}

enum path{
  MENU='/menu',
  MENUS='/menus',
  HEALTH='/health'
}

const TABLE_NAME:string = 'restaurant';
const KEY:string='restaurantId';
const SORT_KEY:string='dataIdentifier';

interface MenuItemOption{
  optionId:string,
  optionName:string,
  optionCost:number
}

interface MenuItemDto{
  itemId:string,
  itemName:string,
  itemType:number,
  maxSelectOptions:number,
  itemOptions:MenuItemOption[]
}

interface MenuDto{
  restId:string, // partition
  menuKey:string
  menuId:string, // sort  
  name:string,
  desc:string,
  menuOrder:string,
  imgUrl:string,
  category:string,
  cost:number,
  quantity:number, // Can be removed
  kitchenId:string,
  characteristics:{[key: string]: number}
  isCustomizable:number
  customizeSettingType:number, // for enhancing customization feature
  customizeSettingData:{},
  items:MenuItemDto[]
}

interface MenuItemOptionEntity{
  optionId:string,
  optionName:string,
  optionCost:number
}

interface MenuItemEntity{
  itemId:string,
  itemName:string,
  itemType:number
  maxSelectOptions:number,
  itemOptions:MenuItemOption[]
}

export interface MenuEntity{
  restaurantId:string,
  dataIdentifier:string,
  menuId:string,
  menuName:string,
  meunDetails:string,
  menuOrder:string,
  menuImgUrl:string,
  menuCost:number,
  menuCategory:string,
  menuQuantity:number,
  menuCharacteristics:{[key: string]: number},
  kitchenId:string,
  isCustomizable:number,
  customizeSettingType:number,// for enhancing customization feature
  customizeSettingData:{},
  items:MenuItemEntity[]
  }

  export interface OrderIdentifiers{
    menuKey: string;
    menuId: string;
  }

exports.handler = async function(event:any){
  console.log('Request events:',event);
    let response:ResponseBody|void;
    switch (true){
        case event.httpMethod =='GET' && event.path === path.HEALTH:
            response=buildResponse(200);
            break;
        case event.httpMethod == 'GET' && event.path === path.MENU:
            response= await getMenu(event.queryStringParameters.restaurantId,event.queryStringParameters.menuId);
            break;
        case event.httpMethod == 'GET' && event.path === path.MENUS:
            response= await getMenus(event.queryStringParameters.restaurantId);
            break;
        case event.httpMethod == 'POST' && event.path === path.MENU:
            response= await saveMenu(JSON.parse(event.body));    
            break;
        // case event.httpMethod == 'PATCH' && event.path === menuPath:
        //     const requestBody = JSON.parse(event.body);
        //     response= await modifyMenu(requestBody.restId,requestBody.menuId,requestBody.updateKey,requestBody.updateValue);      
        //     break;
        // case event.httpMethod == 'DELETE' && event.path === menuPath:
        //     const requestBody = JSON.parse(event.body);
        //     response= await deleteMenu(requestBody.restId,requestBody.menuId);
        //     break;
        default:
            response=buildResponse(404,'404 Not Found');

    }
    return response;
}

function buildResponse(statusCode:number,body?:string):ResponseBody{
  return {
      statusCode:statusCode,
      headers:{
          "Content-Type":"application/json",
          "Access-Control-Allow-Headers" : "Content-Type",
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods": "OPTIONS,POST,GET"
      },
      body: body
  }
}


async function getMenu(restaurantId:string,menuId:string):Promise<void | ResponseBody>{
  const params = {
      
      TableName:TABLE_NAME,
      Key:{
        'restaurantId':restaurantId,
        'dataIdentifier':menuId
      }
  }
  
  console.log("In getMenu");
  console.log(restaurantId+" "+menuId);
  return await dynamoClient.get(params).promise().then((response)=>{
      return buildResponse(200,JSON.stringify(entityToDto(response.Item as MenuEntity)));
  },(error)=>{
      console.error('Menu Not Found:'+error);
  });
}

async function getMenus(restaurantId:string){
  try{
  const params = {
      
      KeyConditionExpression: 'restaurantId = :restaurantId AND begins_with ( dataIdentifier , :dataIdentifier )',
      ExpressionAttributeValues: {
      ':restaurantId': restaurantId,
      ':dataIdentifier': 'menu'
      },
      TableName:TABLE_NAME
  };
  console.log("getMenus params:"+params);
  const response = await dynamoClient.query(params).promise();
  console.log("getMenus result:"+JSON.stringify(response));
  
  const dtos:MenuDto[]=response.Items.map((item:MenuEntity)=>{
    return entityToDto(item);
  });
  return buildResponse(200,JSON.stringify(dtos));
  }catch(error){
      console.log("Error:"+error);
  }
}

async function saveMenu(requestBody:MenuDto){
  const entity:MenuEntity=createEntity(requestBody);
  const params = {
      TableName:TABLE_NAME,
      Item:entity
  }
  return await dynamoClient.put(params).promise().then(()=>{
      const body={
          Operation:'SAVE',
          Message:'SUCCESS',
          Item:entity
      }
      return buildResponse(200,JSON.stringify(body));
  },(error)=>{
      console.error('Menu Not Found');
  });
}

function itemDtoToEntity(menuItemDtos:MenuItemDto[]):MenuItemEntity[]{
  let menuItemEntitys:MenuItemEntity[]=[];
  menuItemDtos.forEach((item)=>{
    let options:MenuItemOptionEntity[]=[];
    item.itemOptions.forEach(option=>{
        options.push({
            optionId:option.optionId,
            optionName:option.optionName,
            optionCost:option.optionCost
        })
    });
    menuItemEntitys.push({
      itemId:item.itemId,
      itemName:item.itemName,
      itemOptions:options,
      itemType:item.itemType,
      maxSelectOptions:item.maxSelectOptions
    })
  });
  return menuItemEntitys;
}

function createEntity(menuDto:MenuDto):MenuEntity{
  const menuIds= getMenuIdentifiers(menuDto.menuId);
  const items=itemDtoToEntity(menuDto.items);
  let menuEntity:MenuEntity=
  {
    restaurantId:menuDto.restId,
    dataIdentifier:menuIds.menuKey,
    menuId:menuIds.menuId,
    menuName:menuDto.name,
    meunDetails:menuDto.desc,
    menuOrder:menuDto.menuOrder,
    menuCost:menuDto.cost,
    menuImgUrl:menuDto.imgUrl,
    menuCategory:menuDto.category,
    menuQuantity:menuDto.quantity,
    kitchenId:menuDto.kitchenId,
    menuCharacteristics:menuDto.characteristics,
    isCustomizable:menuDto.isCustomizable,
    customizeSettingType:menuDto.customizeSettingType,
    customizeSettingData:menuDto.customizeSettingData,
    items:items
  }
  return menuEntity;
}

function dtoToEntity(menuDto:MenuDto):MenuEntity{
  const items=itemDtoToEntity(menuDto.items);
  let menuEntity:MenuEntity=
  {
    restaurantId:menuDto.restId,
    dataIdentifier:menuDto.menuKey,
    menuId:menuDto.menuId,
    menuName:menuDto.name,
    meunDetails:menuDto.desc,
    menuOrder:menuDto.menuOrder,
    menuCost:menuDto.cost,
    menuImgUrl:menuDto.imgUrl,
    menuCategory:menuDto.category,
    menuQuantity:menuDto.quantity,
    kitchenId:menuDto.kitchenId,
    menuCharacteristics:menuDto.characteristics,
    isCustomizable:menuDto.isCustomizable,
    customizeSettingType:menuDto.customizeSettingType,
    customizeSettingData:menuDto.customizeSettingData,
    items:items
  }
  return menuEntity;
}

function itemEntityToDto(menuItemEntity:MenuItemEntity[]):MenuItemDto []{
  let menuItemDtos:MenuItemDto[]=[];
  menuItemEntity.forEach((item)=>{
    let options:MenuItemOptionEntity[]=[];
    item.itemOptions.forEach(option=>{
        options.push({
            optionId:option.optionId,
            optionName:option.optionName,
            optionCost:option.optionCost
        })
    });
    menuItemDtos.push({
      itemId:item.itemId,
      itemName:item.itemName,
      itemOptions:options,
      itemType:item.itemType,
      maxSelectOptions:item.maxSelectOptions
    })
  });
  return menuItemDtos;
}

function entityToDto(menuEntity:MenuEntity):MenuDto{
  const items=itemEntityToDto(menuEntity.items);
  let menuDto:MenuDto=
  {
    restId:menuEntity.restaurantId, // partition
    menuKey:menuEntity.dataIdentifier,
    menuId:menuEntity.menuId, // sort  
    name:menuEntity.menuName,
    desc:menuEntity.meunDetails,
    imgUrl:menuEntity.menuImgUrl,
    category:menuEntity.menuCategory,
    menuOrder:menuEntity.menuOrder,
    cost:menuEntity.menuCost,
    characteristics:menuEntity.menuCharacteristics,
    quantity:0, // Can be removed
    kitchenId:menuEntity.kitchenId,
    isCustomizable:menuEntity.isCustomizable,
    customizeSettingType:menuEntity.customizeSettingType,
    customizeSettingData:menuEntity.customizeSettingData,
    items:items
  }
  return menuDto;

}

function getMenuIdentifiers(menuId:string){
    // Get date in millis from epoc time
    let date = Date.now().toString();

    let menuIdentifiers:OrderIdentifiers={
      // orderKey:'order#'+'20211216#'+milliSeconds,
      menuKey:'menu#'+menuId,
      menuId:menuId
    }
  
    return menuIdentifiers;
}

const dto:MenuDto={
  restId:'1', // partition
  menuId:'1', // sort  
  menuKey:'',
  name:'Paneer Tikka',
  desc:'Tender paneer cooked in think onion gravy',
  imgUrl:'https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg',
  menuOrder:'1#1',
  category:'Starters',
  cost:13,
  quantity:0, // Can be removed
  isCustomizable:0,
  kitchenId:'1',
  customizeSettingType:0,
  customizeSettingData:{},
  characteristics:{"isVegan":1,"isChefSpecial":1,"isDrink":1,"isVeg":1},
  items:[]
}

const riceOption:MenuItemOption[]=[
  {
      optionId:"1",
      optionName:"Biryani",
      optionCost:0
  },
  {
      optionId:"2",
      optionName:"Plain rice",
      optionCost:0
  },
  {
      optionId:"3",
      optionName:"Curd Rice",
      optionCost:0
  }
];
const curryOptions:MenuItemOption[]=[
  {
      optionId:"1",
      optionName:"Chicken curry",
      optionCost:0
  },
  {
      optionId:"2",
      optionName:"Daal",
      optionCost:0
  },
  {
      optionId:"3",
      optionName:"Veg Curry",
      optionCost:0
  }
]

const meatOptions:MenuItemOption[]=[
  {
      optionId:"1",
      optionName:"Chicken",
      optionCost:0
  },
  {
      optionId:"2",
      optionName:"Mutton",
      optionCost:0
  }
]
const sweetOptions:MenuItemOption[]=[
  {
      optionId:"1",
      optionName:"Ladoo",
      optionCost:0
  },
  {
      optionId:"2",
      optionName:"Chocolate Lava",
      optionCost:0
  }
]

let riceItem:MenuItemDto={
  itemId:"1",
  itemName:"Rice",
  itemOptions:riceOption,
  itemType:0,
  maxSelectOptions:2
}

let curryItem:MenuItemDto={
  itemId:"2",
  itemName:"Curry",
  itemOptions:curryOptions,
  itemType:0,
  maxSelectOptions:2
}

let meatItem:MenuItemDto={
  itemId:"3",
  itemName:"Meat",
  itemOptions:curryOptions,
  itemType:0,
  maxSelectOptions:1
}

let sweetItem:MenuItemDto={
  itemId:"4",
  itemName:"Sweets",
  itemOptions:curryOptions,
  itemType:0,
  maxSelectOptions:1
}
const menuDtoMock:MenuDto= {
  "restId":"1",
  "menuId":"1",
  "menuKey":"1",
  "menuOrder":"2#1",
  "name":"Veg Platter",
  "desc":"Veg platter comes with 1 rice 2 rotis and your choice of drinks",
  "imgUrl":"https://spicecravings.com/wp-content/uploads/2020/01/Chicken-Tikka-1.jpg",
  "category":"Specialite Maison",
  "cost":4,
  "quantity":0,
  "kitchenId":'1',
  "isCustomizable":0,
  "customizeSettingType":0,
  "customizeSettingData":{},
  "characteristics":{"isVegan":1,"isChefSpecial":1,"isDrink":1,"isVeg":1},
  "items":[riceItem,curryItem,meatItem,sweetItem]
}
// console.log('==========================================')
// console.log(JSON.stringify(menuDtoMock));
// const entity=createEntity(menuDtoMock);
// console.log('==========================================')
// console.log(entity);
// const dto2=entityToDto(entity);
// console.log('==========================================')
// console.log(JSON.stringify(dto2));
